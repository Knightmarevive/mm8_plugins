#pragma once
#include "pch.h"
#include "main.h"
#include "GameMisc.h"
#include "Arcomage.h"
#include "MergeSpecific.h"
#include "Lod.h"
#include "Bin.h"
#include "GameDataStructs.h"
#include "Common.h"
#include "TxtFileItems.h"
#include "GameMap.h"
#include "Monster.h"
#include "GameParty.h"
#include "Player.h"
#include "MapModel.h"
#include "MapElements.h"
#include "MapMisc.h"

#pragma pack(push, 1)

namespace mm8
{
	struct GameStructure // size: 0x6007A8CC
	{
		static inline mm8::Arcomage* const arcomage = 0;
		static inline mm8::ArmorPicsCoords* const armorPicsCoords = 0;
		static inline mm8::CharacterVoices* const characterVoices = 0;
		static inline mm8::GameClassKinds* const classKinds = 0;
		static inline mm8::GameClasses* const classes = 0;
		static inline mm8::DialogLogic* const dialogLogic = 0;
		static inline mm8::HouseRules* const houseRules = 0;
		static inline mm8::GameMap* const map = 0;
		static inline mm8::GameMouse* const mouse = 0;
		static inline mm8::GameParty* const party = 0;
		static inline mm8::Weather* const weather = 0;

		static int32_t* autonoteCategory; // original offset 0x1A91AC24 (445754404 decimal)element size: 0x4 (4 decimal)
		static uint32_t autonoteCategory_size; // Offset/size: 0x2BA (698 decimal)
		static char** autonoteTxt; // original offset 0x1A91AC20 (445754400 decimal)element size: 0x4 (4 decimal)
		static uint32_t autonoteTxt_size; // Offset/size: 0x2BA (698 decimal)
		static int32_t* awardsSort; // original offset 0x76164C (7738956 decimal)element size: 0x4 (4 decimal)
		static uint32_t awardsSort_size; // Offset/size: 0x69 (105 decimal)
		static mm8::CharacterDollType* characterDollTypes; // original offset 0x462BE90 (73580176 decimal)element size: 0x2F (47 decimal)
		static uint32_t characterDollTypes_size; // Offset/size: 0x6 (6 decimal)
		static mm8::CharacterPortrait* characterPortraits; // original offset 0x45E5E60 (73293408 decimal)element size: 0x44 (68 decimal)
		static uint32_t characterPortraits_size; // Offset/size: 0x4D (77 decimal)
		static char** classNames; // original offset 0xBB2FD0 (12267472 decimal)element size: 0x4 (4 decimal)
		static uint32_t classNames_size; // Offset/size: 0x7A (122 decimal)
		static mm8::CustomLods* customLods; // original offset 0x349DE00 (55172608 decimal)
		static mm8::EventLine* globalEvtLines; // original offset 0x1CA2BC39 (480427065 decimal)element size: 0xC (12 decimal)
		static uint32_t* globalEvtLines_sizePtr; // Offset/size: 0x1CA10048 (480313416 decimal)
		static std::array<std::array<mm8::Item, 12>, 12>* guildItems; // original offset 0x1CA90686 (480839302 decimal)element size: 0x1440 (5184 decimal)
		static uint32_t guildItems_size; // Offset/size: 0x7 (7 decimal)
		static int64_t* guildNextRefill2; // original offset 0x1CA7410E (480723214 decimal)element size: 0x8 (8 decimal)
		static uint32_t guildNextRefill2_size; // Offset/size: 0x7 (7 decimal)
		static std::array<uint8_t, 218>* hostileTxt; // original offset 0x11DC0000 (299630592 decimal)element size: 0xDA (218 decimal)
		static uint32_t hostileTxt_size; // Offset/size: 0xDA (218 decimal)
		static mm8::HouseMovie* houseMovies; // original offset 0x11041BE0 (285481952 decimal)element size: 0x10 (16 decimal)
		static uint32_t houseMovies_size; // Offset/size: 0x1E8 (488 decimal)
		static mm8::Events2DItem* houses; // original offset 0x10F38D70 (284396912 decimal)element size: 0x34 (52 decimal)
		static uint32_t houses_size; // Offset/size: 0x648 (1608 decimal)
		static mm8::HousesExtra* housesExtra; // original offset 0x45863C8 (72901576 decimal)element size: 0x4 (4 decimal)
		static uint32_t housesExtra_size; // Offset/size: 0x647 (1607 decimal)
		static mm8::ItemsTxtItem* itemsTxt; // original offset 0x462BFDC (73580508 decimal)element size: 0x30 (48 decimal)
		static uint32_t* itemsTxt_sizePtr; // Offset/size: 0x5EFBC8 (6224840 decimal)
		static int16_t* mapDoorSound; // original offset 0x10FEEF70 (285142896 decimal)element size: 0x2 (2 decimal)
		static uint32_t mapDoorSound_size; // Offset/size: 0xD0 (208 decimal)
		static mm8::FogChances* mapFogChances; // original offset 0x9120AD0 (152177360 decimal)element size: 0x4 (4 decimal)
		static uint32_t mapFogChances_size; // Offset/size: 0xD0 (208 decimal)
		static mm8::MapStatsItem* mapStats; // original offset 0x11D74E70 (299322992 decimal)element size: 0x44 (68 decimal)
		static uint32_t* mapStats_sizePtr; // Offset/size: 0x11D785B0 (299337136 decimal)
		static mm8::MissileSetup* missileSetup; // original offset 0x45A28A8 (73017512 decimal)element size: 0x1 (1 decimal)
		static uint32_t* missileSetup_sizePtr; // Offset/size: 0x45A28AC (73017516 decimal)
		static std::array<uint16_t, 70>* mixPotions; // original offset 0x4646DF8 (73690616 decimal)element size: 0x8C (140 decimal)
		static uint32_t mixPotions_size; // Offset/size: 0x46 (70 decimal)
		static mm8::MonsterKind* monsterKinds; // original offset 0x458C5C8 (72926664 decimal)element size: 0x8 (8 decimal)
		static uint32_t* monsterKinds_sizePtr; // Offset/size: 0x458C5CC (72926668 decimal)
		static mm8::MonstersTxtItem* monstersTxt; // original offset 0x11DB0000 (299565056 decimal)element size: 0x60 (96 decimal)
		static uint32_t* monstersTxt_sizePtr; // Offset/size: 0x5EFBBC (6224828 decimal)
		static mm8::NPC* NPC; // original offset 0x4662AC4 (73804484 decimal)element size: 0x4C (76 decimal)
		static uint32_t NPC_size; // Offset/size: 0x4CB (1227 decimal)
		static mm8::NPC* NPCDataTxt; // original offset 0x464BE80 (73711232 decimal)element size: 0x4C (76 decimal)
		static uint32_t NPCDataTxt_size; // Offset/size: 0x4CB (1227 decimal)
		static std::array<char*, 2>* NPCGreet; // original offset 0x467B958 (73906520 decimal)element size: 0x8 (8 decimal)
		static uint32_t NPCGreet_size; // Offset/size: 0x14E (334 decimal)
		static int16_t* NPCGroup; // original offset 0x467C4BE (73909438 decimal)element size: 0x2 (2 decimal)
		static uint32_t NPCGroup_size; // Offset/size: 0x66 (102 decimal)
		static char** NPCNews; // original offset 0x46797B0 (73897904 decimal)element size: 0x4 (4 decimal)
		static uint32_t NPCNews_size; // Offset/size: 0x399 (921 decimal)
		static char** NPCText; // original offset 0x458005C (72876124 decimal)element size: 0x4 (4 decimal)
		static uint32_t NPCText_size; // Offset/size: 0xAED (2797 decimal)
		static char** NPCTopic; // original offset 0x4580058 (72876120 decimal)element size: 0x4 (4 decimal)
		static uint32_t NPCTopic_size; // Offset/size: 0x704 (1796 decimal)
		static mm8::PatchOptions* patchOptions; // original offset 0x330D364 (53531492 decimal)
		static char** placeMonTxt; // original offset 0x926CB00 (153537280 decimal)element size: 0x4 (4 decimal)
		static uint32_t* placeMonTxt_sizePtr; // Offset/size: 0x5EFBC0 (6224832 decimal)
		static char** questsTxt; // original offset 0x1A918FF0 (445747184 decimal)element size: 0x4 (4 decimal)
		static uint32_t questsTxt_size; // Offset/size: 0x6C7 (1735 decimal)
		static mm8::ReagentSettings* reagentSettings; // original offset 0x467EEF8 (73920248 decimal)element size: 0x4 (4 decimal)
		static uint32_t reagentSettings_size; // Offset/size: 0x2B (43 decimal)
		static std::array<mm8::Item, 12>* shopItems; // original offset 0x1CA745A6 (480724390 decimal)element size: 0x1B0 (432 decimal)
		static uint32_t shopItems_size; // Offset/size: 0x85 (133 decimal)
		static int64_t* shopNextRefill; // original offset 0x1CA73A7E (480721534 decimal)element size: 0x8 (8 decimal)
		static uint32_t shopNextRefill_size; // Offset/size: 0x85 (133 decimal)
		static std::array<mm8::Item, 12>* shopSpecialItems; // original offset 0x1CA82616 (480781846 decimal)element size: 0x1B0 (432 decimal)
		static uint32_t shopSpecialItems_size; // Offset/size: 0x85 (133 decimal)
		static int64_t* shopTheftExpireTime; // original offset 0x1CA74146 (480723270 decimal)element size: 0x8 (8 decimal)
		static uint32_t shopTheftExpireTime_size; // Offset/size: 0x460 (1120 decimal)
		static uint8_t* titleTrackOffset; // original offset 0x458C530 (72926512 decimal)element size: 0x1 (1 decimal)
		static uint32_t titleTrackOffset_size; // Offset/size: 0x4 (4 decimal)
		static char** transTxt; // original offset 0x45857E8 (72898536 decimal)element size: 0x4 (4 decimal)
		static uint32_t transTxt_size; // Offset/size: 0x10F (271 decimal)
		static std::array<int8_t, 4>* transportIndex; // original offset 0x1CA733F4 (480719860 decimal)element size: 0x4 (4 decimal)
		static uint32_t transportIndex_size; // Offset/size: 0x38 (56 decimal)
		static mm8::TravelInfo* transportLocations; // original offset 0x500DF8 (5246456 decimal)element size: 0x20 (32 decimal)
		static uint32_t transportLocations_size; // Offset/size: 0x66 (102 decimal)

		// uint8_t* randSeed; // real size is 0 | Unknown type | 0x0 (0 decimal)
		SKIP(4381266);
		std::array<uint8_t, 1> minMeleeRecoveryTime; // Unknown type | 0x42DA52 (4381266 decimal)
		SKIP(14553);
		char* winMapIndex; // EditConstPChar - unprotect before/protect after edit | 0x43132C (4395820 decimal)
		SKIP(114349);
		char* summonElementalC; // EditConstPChar - unprotect before/protect after edit | 0x44D1DD (4510173 decimal)
		SKIP(19);
		char* summonElementalB; // EditConstPChar - unprotect before/protect after edit | 0x44D1F4 (4510196 decimal)
		SKIP(3);
		char* summonElementalA; // EditConstPChar - unprotect before/protect after edit | 0x44D1FB (4510203 decimal)
		SKIP(68796);
		// uint8_t* baseYear; // real size is 0 | Unknown type | 0x45DEBB (4579003 decimal)
		SKIP(26481);
		std::array<uint8_t, 4> newGameMap; // Unknown type | 0x46462C (4605484 decimal)
		SKIP(179895);
		std::array<uint8_t, 4> maxBirthYear; // Unknown type | 0x4904E7 (4785383 decimal)
		SKIP(141777);
		std::array<uint8_t, 1> dialogTopicsLimit; // Unknown type | 0x4B2EBC (4927164 decimal)
		SKIP(42192);
		std::array<uint8_t, 1> titleTrack; // Unknown type | 0x4BD38D (4969357 decimal)
		SKIP(222282);
		int32_t currentScreen; // 0x4F37D8 (5191640 decimal)
		SKIP(676);
		std::array<int8_t, 66> monsterClassInfoY; // 0x4F3A80 (5192320 decimal)
		SKIP(2950);
		std::array<std::array<uint8_t, 4>, 132> spellObjId; // MMExt: 1..132, here 0..131 | Unknown type | 0x4F4648 (5195336 decimal)
		SKIP(24);
		std::array<mm8::SpellInfo, 133> spells; // 0x4F4870 (5195888 decimal)
		SKIP(284);
		std::array<int32_t, 6> specialEnterX; // MMExt: 1..6, here 0..5 | 0x4F53F0 (5198832 decimal)
		std::array<int32_t, 6> specialEnterY; // MMExt: 1..6, here 0..5 | 0x4F5408 (5198856 decimal)
		std::array<int32_t, 6> specialEnterZ; // MMExt: 1..6, here 0..5 | 0x4F5420 (5198880 decimal)
		std::array<int32_t, 6> specialEnterDirection; // MMExt: 1..6, here 0..5 | 0x4F5438 (5198904 decimal)
		SKIP(11004);
		std::array<uint8_t, 20> equipStat2ItemSlot; // 0x4F7F4C (5209932 decimal)
		SKIP(19240);
		std::array<mm8::TownPortalTownInfo, 6> townPortalInfo; // 0x4FCA88 (5229192 decimal)
		SKIP(120);
		std::array<char*, 30> playerFaces; // EditConstPChar - unprotect before/protect after edit | 0x4FCB78 (5229432 decimal)
		SKIP(1200);
		std::array<std::array<uint8_t, 30>, 49> standardPlayerSoundsCount; // MMExt: 1..49, here 0..48 | 0x4FD0A0 (5230752 decimal)
		SKIP(2);
		std::array<mm8::FaceAnimationInfo, 110> standardFaceAnimations; // 0x4FD660 (5232224 decimal)
		SKIP(1464);
		std::array<int16_t, 13> skillRecoveryTimes; // 0x4FDF88 (5234568 decimal)
		SKIP(392);
		std::array<int16_t, 132> spellSounds; // MMExt: 1..132, here 0..131 | 0x4FE12A (5234986 decimal)
		SKIP(9662);
		std::array<mm8::ShopItemKind, 14> shopWeaponKinds; // MMExt: 1..14, here 0..13 | 0x5007F0 (5244912 decimal)
		SKIP(4);
		std::array<std::array<mm8::ShopItemKind, 2>, 14> shopArmorKinds; // MMExt: 15..28, here 0..13 | MMExt: 1..2, here 0..1 | 0x500880 (5245056 decimal)
		union
		{
			std::array<int16_t, 13> shopMagicLevels; // MMExt: 29..41, here 0..12 | 0x500998 (5245336 decimal)
			struct
			{
				SKIP(6);
				std::array<int16_t, 12> shopAlchemistLevels; // MMExt: 42..53, here 0..11 | 0x50099E (5245342 decimal)
			};
		};
		SKIP(122);
		std::array<mm8::ShopItemKind, 14> shopWeaponKindsSpecial; // MMExt: 1..14, here 0..13 | 0x500A30 (5245488 decimal)
		SKIP(4);
		std::array<std::array<mm8::ShopItemKind, 2>, 14> shopArmorKindsSpecial; // MMExt: 15..28, here 0..13 | MMExt: 1..2, here 0..1 | 0x500AC0 (5245632 decimal)
		std::array<int16_t, 13> shopMagicLevelsSpecial; // MMExt: 29..41, here 0..12 | 0x500BD8 (5245912 decimal)
		SKIP(2);
		std::array<int16_t, 12> shopAlchemistLevelsSpecial; // MMExt: 42..53, here 0..11 | 0x500BF4 (5245940 decimal)
		SKIP(292);
		std::array<int32_t, 11> guildJoinCost; // 0x500D30 (5246256 decimal)
		std::array<int16_t, 13> trainingLevels; // MMExt: 89..101, here 0..12 | 0x500D5C (5246300 decimal)
		SKIP(2);
		std::array<int32_t, 32> guildAwards; // MMExt: 139..170, here 0..31 | 0x500D78 (5246328 decimal)
		union
		{
			std::array<mm8::TravelInfo, 102> transportLocations; // 0x500DF8 (5246456 decimal)
			struct
			{
				SKIP(1088);
				std::array<int16_t, 34> guildSpellLevels; // MMExt: 139..172, here 0..33 | 0x501238 (5247544 decimal)
			};
		};
		SKIP(272);
		std::array<int16_t, 6> townPortalX; // 0x501BC8 (5249992 decimal)
		std::array<int16_t, 6> townPortalY; // 0x501BD4 (5250004 decimal)
		SKIP(32956);
		int32_t turnBasedPhase; // 0x509C9C (5282972 decimal)
		SKIP(59656);
		int32_t currentCharScreen; // 0x5185A8 (5342632 decimal)
		SKIP(3392);
		std::array<char*, 7> statsNames; // EditPChar | 0x5192EC (5346028 decimal)
		SKIP(4);
		uint32_t ctrlPressed; // 4-byte boolean | 0x51930C (5346060 decimal)
		SKIP(20);
		mm8::Dlg* currentNPCDialog; // 0x519324 (5346084 decimal)
		SKIP(40);
		std::array<uint8_t, 4> currentPlayer; // Unknown type | 0x519350 (5346128 decimal)
		bool rightButtonPressed; // 0x519354 (5346132 decimal)
		SKIP(16319);
		uint32_t paused2; // 4-byte boolean | 0x51D314 (5362452 decimal)
		SKIP(36);
		uint32_t paused; // 4-byte boolean | 0x51D33C (5362492 decimal)
		SKIP(20);
		int32_t timeDelta; // 0x51D354 (5362516 decimal)
		SKIP(3568);
		uint32_t actionsNext_size; // 0x51E148 (5366088 decimal)
		std::array<mm8::ActionItem, 40> actionsNext; // size field offset is 0x51E148 | 0x51E14C (5366092 decimal)
		SKIP(4);
		uint32_t actions_size; // 0x51E330 (5366576 decimal)
		std::array<mm8::ActionItem, 40> actions; // size field offset is 0x51E330 | 0x51E334 (5366580 decimal)
		SKIP(431044);
		mm8::ProgressBar progressBar; // 0x5878D8 (5798104 decimal)
		SKIP(44);
		uint32_t loadingScreen; // 4-byte boolean | 0x587914 (5798164 decimal)
		SKIP(452);
		uint32_t needRedraw; // 4-byte boolean | 0x587ADC (5798620 decimal)
		SKIP(120984);
		int32_t NPCCommand; // 0x5A5378 (5919608 decimal)
		int32_t dialogNPC; // 0x5A537C (5919612 decimal)
		SKIP(4);
		int32_t houseOwnerPic; // 0x5A5384 (5919620 decimal)
		SKIP(832);
		std::array<int32_t, 6> houseNPCs; // MMExt: 1..6, here 0..5 | 0x5A56C8 (5920456 decimal)
		int32_t houseNPCSlot; // 0x5A56E0 (5920480 decimal)
		SKIP(48);
		int32_t houseNPCSlotsCount; // 0x5A5714 (5920532 decimal)
		SKIP(135472);
		std::array<char, 2000> streetMessage; // fixed size string, requires null terminator | 0x5C6848 (6056008 decimal)
		SKIP(8);
		uint32_t mapEvtLines_size; // 0x5C7020 (6058016 decimal)
		SKIP(23700);
		mm8::MoveToMap moveToMap; // 0x5CCCB8 (6081720 decimal)
		SKIP(16);
		bool inQuestionDialog; // 0x5CCCE4 (6081764 decimal)
		SKIP(3);
		std::array<mm8::EventLine, 5000> mapEvtLines; // size field offset is 0x5C7020 | 0x5CCCE8 (6081768 decimal)
		SKIP(16);
		std::array<char, 200> statusMessage; // fixed size string, requires null terminator | 0x5DB758 (6141784 decimal)
		std::array<char, 200> mouseOverStatusMessage; // fixed size string, requires null terminator | 0x5DB820 (6141984 decimal)
		int32_t statusDisappearTime; // 0x5DB8E8 (6142184 decimal)
		SKIP(16);
		int32_t houseExitMap; // 0x5DB8FC (6142204 decimal)
		SKIP(16);
		int32_t autonote_fnt; // 0x5DB910 (6142224 decimal)
		int32_t spell_fnt; // 0x5DB914 (6142228 decimal)
		int32_t arrus_fnt; // 0x5DB918 (6142232 decimal)
		int32_t lucida_fnt; // 0x5DB91C (6142236 decimal)
		int32_t book2_fnt; // 0x5DB920 (6142240 decimal)
		int32_t book_fnt; // 0x5DB924 (6142244 decimal)
		SKIP(4);
		int32_t create_fnt; // 0x5DB92C (6142252 decimal)
		SKIP(4);
		int32_t comic_fnt; // 0x5DB934 (6142260 decimal)
		int32_t smallnum_fnt; // 0x5DB938 (6142264 decimal)
		SKIP(14244);
		std::array<char, 2000> textBuffer; // fixed size string, requires null terminator | 0x5DF0E0 (6156512 decimal)
		SKIP(6000);
		std::array<char, 2000> textBuffer2; // fixed size string, requires null terminator | 0x5E1020 (6164512 decimal)
		SKIP(6104);
		uint32_t TFTBin_size; // 0x5E2FC8 (6172616 decimal)
		mm8::TFTItem* TFTBin; // size field offset is 0x5E2FC8 | 0x5E2FCC (6172620 decimal)
		mm8::SFT SFTBin; // 0x5E2FD0 (6172624 decimal)
		SKIP(6408);
		std::array<char*, 36> classDescriptions; // EditPChar | 0x5E48F0 (6179056 decimal)
		SKIP(16);
		std::array<char*, 7> statsDescriptions; // EditPChar | 0x5E4990 (6179216 decimal)
		SKIP(772);
		std::array<char*, 39> skillDescriptions; // EditPChar | 0x5E4CB0 (6180016 decimal)
		SKIP(92);
		std::array<mm8::HistoryTxtItem, 29> historyTxt; // MMExt: 1..29, here 0..28 | 0x5E4DA8 (6180264 decimal)
		SKIP(13172);
		std::array<mm8::SpellsTxtItem, 133> spellsTxt; // 0x5E8278 (6193784 decimal)
		SKIP(26256);
		uint32_t monstersTxt_size; // 0x5EFBBC (6224828 decimal)
		uint32_t placeMonTxt_size; // 0x5EFBC0 (6224832 decimal)
		SKIP(4);
		uint32_t itemsTxt_size; // 0x5EFBC8 (6224840 decimal)
		SKIP(61508);
		std::array<std::array<int16_t, 50>, 50> potionTxt; // MMExt: 222..271, here 0..49 | MMExt: 222..271, here 0..49 | 0x5FEC10 (6286352 decimal)
		SKIP(5296);
		std::array<char*, 750> globalTxt; // EditPChar | 0x601448 (6296648 decimal)
		SKIP(1328);
		uint32_t overlayBin_size; // 0x602530 (6300976 decimal)
		mm8::OverlayItem* overlayBin; // size field offset is 0x602530 | 0x602534 (6300980 decimal)
		SKIP(106484);
		uint32_t chestBin_size; // 0x61C52C (6407468 decimal)
		mm8::DChestItem* chestBin; // size field offset is 0x61C52C | 0x61C530 (6407472 decimal)
		uint32_t monListBin_size; // 0x61C534 (6407476 decimal)
		mm8::MonListItem* monListBin; // MMExt: 1..0, here 0..-1 | size field offset is 0x61C534 | 0x61C538 (6407480 decimal)
		SKIP(598012);
		uint32_t objListBin_size; // 0x6AE538 (7005496 decimal)
		mm8::ObjListItem* objListBin; // size field offset is 0x6AE538 | 0x6AE53C (7005500 decimal)
		SKIP(108060);
		uint32_t decListBin_size; // 0x6C8B5C (7113564 decimal)
		mm8::DecListItem* decListBin; // size field offset is 0x6C8B5C | 0x6C8B60 (7113568 decimal)
		SKIP(96);
		std::array<int32_t, 30> keyCodes; // 0x6C8BC4 (7113668 decimal)
		std::array<int32_t, 30> keyTypes; // 0x6C8C3C (7113788 decimal)
		SKIP(22852);
		mm8::Lod saveGameLod; // 0x6CE5F8 (7136760 decimal)
		SKIP(4);
		mm8::Lod gamesLod; // 0x6CE838 (7137336 decimal)
		SKIP(176);
		int32_t mainMenuCode; // 0x6CEB24 (7138084 decimal)
		int32_t exitMapAction; // 0x6CEB28 (7138088 decimal)
		SKIP(148624);
		uint32_t tile3Bin_size; // 0x6F2FBC (7286716 decimal)
		mm8::TileItem* tile3Bin; // size field offset is 0x6F2FBC | 0x6F2FC0 (7286720 decimal)
		uint32_t tile2Bin_size; // 0x6F2FC4 (7286724 decimal)
		mm8::TileItem* tile2Bin; // size field offset is 0x6F2FC4 | 0x6F2FC8 (7286728 decimal)
		uint32_t tileBin_size; // 0x6F2FCC (7286732 decimal)
		mm8::TileItem* tileBin; // size field offset is 0x6F2FCC | 0x6F2FD0 (7286736 decimal)
		mm8::CurrentTileBin* currentTileBin; // 0x6F2FD4 (7286740 decimal)
		int32_t exitLevelCode; // 0x6F2FD8 (7286744 decimal)
		SKIP(48);
		int32_t outdoorViewMul; // 0x6F300C (7286796 decimal)
		SKIP(1908);
		std::array<float, 4> speedModifiers; // 0x6F3784 (7288708 decimal)
		SKIP(416);
		uint32_t windowHandle; // 0x6F3934 (7289140 decimal)
		SKIP(119);
		int32_t soundVolume; // 0x6F39AF (7289263 decimal)
		SKIP(105013);
		mm8::BitmapsLod iconsLod; // 0x70D3E8 (7394280 decimal)
		SKIP(8);
		mm8::SpritesLod spritesLod; // 0x71EFA8 (7466920 decimal)
		mm8::BitmapsLod bitmapsLod; // 0x72DC60 (7527520 decimal)
		SKIP(138800);
		union
		{
			struct __awardsTxt
			{
				char* value;
				SKIP(4);
			};
			std::array<__awardsTxt, 105> awardsTxt; // EditPChar | 0x761648 (7738952 decimal) | 0x761648 (7738952 decimal)
			struct
			{
				SKIP(4);
				struct __awardsSort
				{
					int32_t value;
					SKIP(4);
				};
				std::array<__awardsSort, 105> awardsSort; // 0x76164C (7738956 decimal) | 0x76164C (7738956 decimal)
			};
		};
		SKIP(88076);
		std::array<mm8::NPC, 100> streetNPC; // size field offset is 0x779FF4 | 0x7771A0 (7827872 decimal)
		SKIP(4260);
		uint32_t streetNPC_size; // 0x779FF4 (7839732 decimal)
		SKIP(488);
		std::array<std::array<char*, 7>, 4> merchantTxt; // EditPChar | 0x77A1E0 (7840224 decimal)
		SKIP(3378608);
		struct // size: 0xC, MMExt union
		{
			int64_t delay; // 0xAB3000 (11218944 decimal)
			int16_t animation; // 0xAB3008 (11218952 decimal)
			int16_t playerIndex; // 0xAB300A (11218954 decimal)
		} delayedFaceAnimation;
		static_assert(sizeof(delayedFaceAnimation) == 0xC, "Invalid \"delayedFaceAnimation\" structure size");
		uint32_t IFTBin_size; // 0xAB300C (11218956 decimal)
		mm8::IFTItem* IFTBin; // size field offset is 0xAB300C | 0xAB3010 (11218960 decimal)
		uint32_t PFTBin_size; // 0xAB3014 (11218964 decimal)
		mm8::PFTItem* PFTBin; // size field offset is 0xAB3014 | 0xAB3018 (11218968 decimal)
		SKIP(450208);
		int64_t time; // 0xB20EBC (11669180 decimal)
		SKIP(512);
		std::array<int64_t, 34> guildNextRefill; // MMExt: 139..172, here 0..33 | 0xB210C4 (11669700 decimal)
		SKIP(984);
		int32_t year; // 0xB215AC (11670956 decimal)
		int32_t month; // 0xB215B0 (11670960 decimal)
		int32_t weekOfMonth; // 0xB215B4 (11670964 decimal)
		int32_t dayOfMonth; // 0xB215B8 (11670968 decimal)
		int32_t hour; // 0xB215BC (11670972 decimal)
		int32_t minute; // 0xB215C0 (11670976 decimal)
		int32_t second; // 0xB215C4 (11670980 decimal)
		SKIP(352);
		uint32_t turnBased; // 4-byte boolean | 0xB21728 (11671336 decimal)
		SKIP(373596);
		bool needRender : 1; // 0xB7CA88 (12044936 decimal), bit index 6
		SKIP(222075);
		int32_t armageddonTimeLeft; // 0xBB2E04 (12267012 decimal)
		int32_t armageddonSkill; // 0xBB2E08 (12267016 decimal)
		std::array<int32_t, 5> turnBasedDelays; // 0xBB2E0C (12267020 decimal)
		SKIP(432);
		union
		{
			std::array<char*, 122> classNames; // EditPChar | 0xBB2FD0 (12267472 decimal)
			struct
			{
				SKIP(144);
				std::array<char*, 39> skillNames; // EditPChar | 0xBB3060 (12267616 decimal)
			};
		};
		SKIP(3205064);
		union
		{
			mm8::GameScreen screen; // 0xEC1980 (15473024 decimal)
			struct
			{
				SKIP(262196);
				std::array<std::array<mm8::ObjectRef, 640>, 480>* objectByPixel; // 0xF019B4 (15735220 decimal)
				SKIP(32);
				uint32_t windowed; // 0xF019D8 (15735256 decimal)
				SKIP(44);
				uint32_t rendererD3D; // 0xF01A08 (15735304 decimal)
			};
		};
		SKIP(957332);
		int32_t redbookHandle; // 0xFEB604 (16692740 decimal)
		int32_t MSSHandle; // 0xFEB608 (16692744 decimal)
		SKIP(2520);
		uint32_t soundsBin_size; // 0xFEBFE4 (16695268 decimal)
		mm8::SoundsItem* soundsBin; // size field offset is 0xFEBFE4 | 0xFEBFE8 (16695272 decimal)
		SKIP(70632);
		std::array<int32_t, 12> guildItemIconPtr; // 0xFFD3D4 (16765908 decimal)
		int32_t housePicType; // 0xFFD404 (16765956 decimal)
		int32_t houseScreen; // 0xFFD408 (16765960 decimal)
		SKIP(8);
		int32_t houseAllowAction; // 0xFFD414 (16765972 decimal)
		int32_t houseActionInfo; // 0xFFD418 (16765976 decimal)
		int32_t houseTeachMastery; // 0xFFD41C (16765980 decimal)
		int32_t houseCost; // 0xFFD420 (16765984 decimal)
		SKIP(44);
		int32_t houseItemsCount; // 0xFFD450 (16766032 decimal)
		SKIP(4);
		char* NPCMessage; // EditPChar | 0xFFD458 (16766040 decimal)
		SKIP(2380);
		int32_t smackVideo; // 0xFFDDA8 (16768424 decimal)
		SKIP(56);
		uint32_t isMovieLooped; // 4-byte boolean | 0xFFDDE4 (16768484 decimal)
		SKIP(24);
		int32_t binkVideo; // 0xFFDE00 (16768512 decimal)
		SKIP(24);
		int32_t movieKind; // 0xFFDE1C (16768540 decimal)
		SKIP(33580);
		uint32_t inOODialog; // 4-byte boolean | 0x100614C (16802124 decimal)
		SKIP(36729364);
		mm8::PatchOptions patchOptions; // 0x330D364 (53531492 decimal)
		SKIP(1640740);
		mm8::CustomLods customLods; // 0x349DE00 (55172608 decimal)
		SKIP(17685076);
		union
		{
			struct __NPCText
			{
				char* value;
				SKIP(4);
			};
			std::array<__NPCText, 2797> NPCText; // MMExt: 1..2797, here 0..2796 | EditPChar | 0x458005C (72876124 decimal) | 0x458005C (72876124 decimal)
			struct
			{
				struct __NPCTopic
				{
					char* value;
					SKIP(4);
				};
				std::array<__NPCTopic, 1796> NPCTopic; // MMExt: 1..1796, here 0..1795 | EditPChar | 0x4580058 (72876120 decimal) | 0x4580058 (72876120 decimal)
			};
		};
		SKIP(36);
		std::array<char*, 271> transTxt; // MMExt: 1..271, here 0..270 | EditPChar | 0x45857E8 (72898536 decimal)
		SKIP(1956);
		std::array<mm8::HousesExtra, 1607> housesExtra; // MMExt: 1..1607, here 0..1606 | 0x45863C8 (72901576 decimal)
		SKIP(18508);
		std::array<uint8_t, 4> titleTrackOffset; // Unknown type | 0x458C530 (72926512 decimal)
		SKIP(148);
		mm8::MonsterKind* monsterKinds; // 0x458C5C8 (72926664 decimal)
		uint32_t monsterKinds_size; // 0x458C5CC (72926668 decimal)
		SKIP(90840);
		mm8::MissileSetup* missileSetup; // 0x45A28A8 (73017512 decimal)
		uint32_t missileSetup_size; // 0x45A28AC (73017516 decimal)
		SKIP(275888);
		std::array<mm8::CharacterPortrait, 77> characterPortraits; // 0x45E5E60 (73293408 decimal)
		SKIP(281532);
		std::array<mm8::CharacterDollType, 6> characterDollTypes; // 0x462BE90 (73580176 decimal)
		SKIP(50);
		std::array<mm8::ItemsTxtItem, 2201> itemsTxt; // 0x462BFDC (73580508 decimal)
		std::array<mm8::StdItemsTxtItem, 29> stdItemsTxt; // 0x4645C8C (73686156 decimal)
		std::array<mm8::SpcItemsTxtItem, 138> spcItemsTxt; // 0x4645ED0 (73686736 decimal)
		SKIP(16);
		std::array<std::array<uint16_t, 70>, 70> mixPotions; // MMExt: 1..70, here 0..69 | MMExt: 1..70, here 0..69 | 0x4646DF8 (73690616 decimal)
		SKIP(10816);
		std::array<mm8::NPC, 1227> NPCDataTxt; // 0x464BE80 (73711232 decimal)
		std::array<mm8::NPC, 1227> NPC; // 0x4662AC4 (73804484 decimal)
		SKIP(168);
		std::array<char*, 921> NPCNews; // EditPChar | 0x46797B0 (73897904 decimal)
		SKIP(4932);
		std::array<std::array<char*, 2>, 334> NPCGreet; // EditPChar | 0x467B958 (73906520 decimal)
		SKIP(246);
		std::array<int16_t, 102> NPCGroup; // 0x467C4BE (73909438 decimal)
		SKIP(10606);
		std::array<mm8::ReagentSettings, 43> reagentSettings; // 0x467EEF8 (73920248 decimal)
		SKIP(78256940);
		std::array<mm8::FogChances, 208> mapFogChances; // 0x9120AD0 (152177360 decimal)
		SKIP(1359088);
		std::array<char*, 163> placeMonTxt; // EditPChar | 0x926CB00 (153537280 decimal)
		SKIP(130858980);
		std::array<mm8::Events2DItem, 1608> houses; // 0x10F38D70 (284396912 decimal)
		SKIP(662368);
		std::array<int16_t, 208> mapDoorSound; // 0x10FEEF70 (285142896 decimal)
		SKIP(338640);
		std::array<mm8::HouseMovie, 488> houseMovies; // 0x11041BE0 (285481952 decimal)
		SKIP(13833232);
		std::array<mm8::MapStatsItem, 208> mapStats; // 0x11D74E70 (299322992 decimal)
		uint32_t mapStats_size; // 0x11D785B0 (299337136 decimal)
		SKIP(227916);
		std::array<mm8::MonstersTxtItem, 652> monstersTxt; // 0x11DB0000 (299565056 decimal)
		SKIP(2944);
		std::array<std::array<uint8_t, 218>, 218> hostileTxt; // 0x11DC0000 (299630592 decimal)
		SKIP(146069068);
		std::array<char*, 1735> questsTxt; // EditPChar | 0x1A918FF0 (445747184 decimal)
		SKIP(276);
		union
		{
			struct __autonoteTxt
			{
				char* value;
				SKIP(4);
			};
			std::array<__autonoteTxt, 698> autonoteTxt; // EditPChar | 0x1A91AC20 (445754400 decimal) | 0x1A91AC20 (445754400 decimal)
			struct
			{
				SKIP(4);
				struct __autonoteCategory
				{
					int32_t value;
					SKIP(4);
				};
				std::array<__autonoteCategory, 698> autonoteCategory; // 0x1A91AC24 (445754404 decimal) | 0x1A91AC24 (445754404 decimal)
			};
		};
		SKIP(34553428);
		uint32_t globalEvtLines_size; // 0x1CA10048 (480313416 decimal)
		SKIP(113645);
		std::array<mm8::EventLine, 12071> globalEvtLines; // 0x1CA2BC39 (480427065 decimal)
		SKIP(147943);
		std::array<std::array<int8_t, 4>, 56> transportIndex; // MMExt: 1..4, here 0..3 | 0x1CA733F4 (480719860 decimal)
		SKIP(1450);
		std::array<int64_t, 133> shopNextRefill; // 0x1CA73A7E (480721534 decimal)
		SKIP(616);
		std::array<int64_t, 7> guildNextRefill2; // 0x1CA7410E (480723214 decimal)
		union
		{
			std::array<std::array<mm8::Item, 12>, 133> shopItems; // 0x1CA745A6 (480724390 decimal)
			struct
			{
				std::array<int64_t, 1120> shopTheftExpireTime; // 0x1CA74146 (480723270 decimal)
			};
		};
		std::array<std::array<mm8::Item, 12>, 133> shopSpecialItems; // 0x1CA82616 (480781846 decimal)
		std::array<std::array<std::array<mm8::Item, 12>, 12>, 7> guildItems; // 0x1CA90686 (480839302 decimal)
		SKIP(1130239110);
	};
	static_assert(sizeof(mm8::GameStructure) == 0x6007A8CC, "Invalid \"mm8::GameStructure\" structure size");
	static_assert(offsetof(mm8::GameStructure, minMeleeRecoveryTime) == 4381266);
	static_assert(offsetof(mm8::GameStructure, winMapIndex) == 4395820);
	static_assert(offsetof(mm8::GameStructure, summonElementalC) == 4510173);
	static_assert(offsetof(mm8::GameStructure, summonElementalB) == 4510196);
	static_assert(offsetof(mm8::GameStructure, summonElementalA) == 4510203);
	static_assert(offsetof(mm8::GameStructure, newGameMap) == 4605484);
	static_assert(offsetof(mm8::GameStructure, maxBirthYear) == 4785383);
	static_assert(offsetof(mm8::GameStructure, dialogTopicsLimit) == 4927164);
	static_assert(offsetof(mm8::GameStructure, titleTrack) == 4969357);
	static_assert(offsetof(mm8::GameStructure, currentScreen) == 5191640);
	static_assert(offsetof(mm8::GameStructure, monsterClassInfoY) == 5192320);
	static_assert(offsetof(mm8::GameStructure, spellObjId) == 5195336);
	static_assert(offsetof(mm8::GameStructure, spells) == 5195888);
	static_assert(offsetof(mm8::GameStructure, specialEnterX) == 5198832);
	static_assert(offsetof(mm8::GameStructure, specialEnterY) == 5198856);
	static_assert(offsetof(mm8::GameStructure, specialEnterZ) == 5198880);
	static_assert(offsetof(mm8::GameStructure, specialEnterDirection) == 5198904);
	static_assert(offsetof(mm8::GameStructure, equipStat2ItemSlot) == 5209932);
	static_assert(offsetof(mm8::GameStructure, townPortalInfo) == 5229192);
	static_assert(offsetof(mm8::GameStructure, playerFaces) == 5229432);
	static_assert(offsetof(mm8::GameStructure, standardPlayerSoundsCount) == 5230752);
	static_assert(offsetof(mm8::GameStructure, standardFaceAnimations) == 5232224);
	static_assert(offsetof(mm8::GameStructure, skillRecoveryTimes) == 5234568);
	static_assert(offsetof(mm8::GameStructure, spellSounds) == 5234986);
	static_assert(offsetof(mm8::GameStructure, shopWeaponKinds) == 5244912);
	static_assert(offsetof(mm8::GameStructure, shopArmorKinds) == 5245056);
	static_assert(offsetof(mm8::GameStructure, shopMagicLevels) == 5245336);
	static_assert(offsetof(mm8::GameStructure, shopAlchemistLevels) == 5245342);
	static_assert(offsetof(mm8::GameStructure, shopWeaponKindsSpecial) == 5245488);
	static_assert(offsetof(mm8::GameStructure, shopArmorKindsSpecial) == 5245632);
	static_assert(offsetof(mm8::GameStructure, shopMagicLevelsSpecial) == 5245912);
	static_assert(offsetof(mm8::GameStructure, shopAlchemistLevelsSpecial) == 5245940);
	static_assert(offsetof(mm8::GameStructure, guildJoinCost) == 5246256);
	static_assert(offsetof(mm8::GameStructure, trainingLevels) == 5246300);
	static_assert(offsetof(mm8::GameStructure, guildAwards) == 5246328);
	static_assert(offsetof(mm8::GameStructure, transportLocations) == 5246456);
	static_assert(offsetof(mm8::GameStructure, guildSpellLevels) == 5247544);
	static_assert(offsetof(mm8::GameStructure, townPortalX) == 5249992);
	static_assert(offsetof(mm8::GameStructure, townPortalY) == 5250004);
	static_assert(offsetof(mm8::GameStructure, turnBasedPhase) == 5282972);
	static_assert(offsetof(mm8::GameStructure, currentCharScreen) == 5342632);
	static_assert(offsetof(mm8::GameStructure, statsNames) == 5346028);
	static_assert(offsetof(mm8::GameStructure, ctrlPressed) == 5346060);
	static_assert(offsetof(mm8::GameStructure, currentNPCDialog) == 5346084);
	static_assert(offsetof(mm8::GameStructure, currentPlayer) == 5346128);
	static_assert(offsetof(mm8::GameStructure, rightButtonPressed) == 5346132);
	static_assert(offsetof(mm8::GameStructure, paused2) == 5362452);
	static_assert(offsetof(mm8::GameStructure, paused) == 5362492);
	static_assert(offsetof(mm8::GameStructure, timeDelta) == 5362516);
	static_assert(offsetof(mm8::GameStructure, actionsNext_size) == 5366088);
	static_assert(offsetof(mm8::GameStructure, actionsNext) == 5366092);
	static_assert(offsetof(mm8::GameStructure, actions_size) == 5366576);
	static_assert(offsetof(mm8::GameStructure, actions) == 5366580);
	static_assert(offsetof(mm8::GameStructure, progressBar) == 5798104);
	static_assert(offsetof(mm8::GameStructure, loadingScreen) == 5798164);
	static_assert(offsetof(mm8::GameStructure, needRedraw) == 5798620);
	static_assert(offsetof(mm8::GameStructure, NPCCommand) == 5919608);
	static_assert(offsetof(mm8::GameStructure, dialogNPC) == 5919612);
	static_assert(offsetof(mm8::GameStructure, houseOwnerPic) == 5919620);
	static_assert(offsetof(mm8::GameStructure, houseNPCs) == 5920456);
	static_assert(offsetof(mm8::GameStructure, houseNPCSlot) == 5920480);
	static_assert(offsetof(mm8::GameStructure, houseNPCSlotsCount) == 5920532);
	static_assert(offsetof(mm8::GameStructure, streetMessage) == 6056008);
	static_assert(offsetof(mm8::GameStructure, mapEvtLines_size) == 6058016);
	static_assert(offsetof(mm8::GameStructure, moveToMap) == 6081720);
	static_assert(offsetof(mm8::GameStructure, inQuestionDialog) == 6081764);
	static_assert(offsetof(mm8::GameStructure, mapEvtLines) == 6081768);
	static_assert(offsetof(mm8::GameStructure, statusMessage) == 6141784);
	static_assert(offsetof(mm8::GameStructure, mouseOverStatusMessage) == 6141984);
	static_assert(offsetof(mm8::GameStructure, statusDisappearTime) == 6142184);
	static_assert(offsetof(mm8::GameStructure, houseExitMap) == 6142204);
	static_assert(offsetof(mm8::GameStructure, autonote_fnt) == 6142224);
	static_assert(offsetof(mm8::GameStructure, spell_fnt) == 6142228);
	static_assert(offsetof(mm8::GameStructure, arrus_fnt) == 6142232);
	static_assert(offsetof(mm8::GameStructure, lucida_fnt) == 6142236);
	static_assert(offsetof(mm8::GameStructure, book2_fnt) == 6142240);
	static_assert(offsetof(mm8::GameStructure, book_fnt) == 6142244);
	static_assert(offsetof(mm8::GameStructure, create_fnt) == 6142252);
	static_assert(offsetof(mm8::GameStructure, comic_fnt) == 6142260);
	static_assert(offsetof(mm8::GameStructure, smallnum_fnt) == 6142264);
	static_assert(offsetof(mm8::GameStructure, textBuffer) == 6156512);
	static_assert(offsetof(mm8::GameStructure, textBuffer2) == 6164512);
	static_assert(offsetof(mm8::GameStructure, TFTBin_size) == 6172616);
	static_assert(offsetof(mm8::GameStructure, TFTBin) == 6172620);
	static_assert(offsetof(mm8::GameStructure, SFTBin) == 6172624);
	static_assert(offsetof(mm8::GameStructure, classDescriptions) == 6179056);
	static_assert(offsetof(mm8::GameStructure, statsDescriptions) == 6179216);
	static_assert(offsetof(mm8::GameStructure, skillDescriptions) == 6180016);
	static_assert(offsetof(mm8::GameStructure, historyTxt) == 6180264);
	static_assert(offsetof(mm8::GameStructure, spellsTxt) == 6193784);
	static_assert(offsetof(mm8::GameStructure, monstersTxt_size) == 6224828);
	static_assert(offsetof(mm8::GameStructure, placeMonTxt_size) == 6224832);
	static_assert(offsetof(mm8::GameStructure, itemsTxt_size) == 6224840);
	static_assert(offsetof(mm8::GameStructure, potionTxt) == 6286352);
	static_assert(offsetof(mm8::GameStructure, globalTxt) == 6296648);
	static_assert(offsetof(mm8::GameStructure, overlayBin_size) == 6300976);
	static_assert(offsetof(mm8::GameStructure, overlayBin) == 6300980);
	static_assert(offsetof(mm8::GameStructure, chestBin_size) == 6407468);
	static_assert(offsetof(mm8::GameStructure, chestBin) == 6407472);
	static_assert(offsetof(mm8::GameStructure, monListBin_size) == 6407476);
	static_assert(offsetof(mm8::GameStructure, monListBin) == 6407480);
	static_assert(offsetof(mm8::GameStructure, objListBin_size) == 7005496);
	static_assert(offsetof(mm8::GameStructure, objListBin) == 7005500);
	static_assert(offsetof(mm8::GameStructure, decListBin_size) == 7113564);
	static_assert(offsetof(mm8::GameStructure, decListBin) == 7113568);
	static_assert(offsetof(mm8::GameStructure, keyCodes) == 7113668);
	static_assert(offsetof(mm8::GameStructure, keyTypes) == 7113788);
	static_assert(offsetof(mm8::GameStructure, saveGameLod) == 7136760);
	static_assert(offsetof(mm8::GameStructure, gamesLod) == 7137336);
	static_assert(offsetof(mm8::GameStructure, mainMenuCode) == 7138084);
	static_assert(offsetof(mm8::GameStructure, exitMapAction) == 7138088);
	static_assert(offsetof(mm8::GameStructure, tile3Bin_size) == 7286716);
	static_assert(offsetof(mm8::GameStructure, tile3Bin) == 7286720);
	static_assert(offsetof(mm8::GameStructure, tile2Bin_size) == 7286724);
	static_assert(offsetof(mm8::GameStructure, tile2Bin) == 7286728);
	static_assert(offsetof(mm8::GameStructure, tileBin_size) == 7286732);
	static_assert(offsetof(mm8::GameStructure, tileBin) == 7286736);
	static_assert(offsetof(mm8::GameStructure, currentTileBin) == 7286740);
	static_assert(offsetof(mm8::GameStructure, exitLevelCode) == 7286744);
	static_assert(offsetof(mm8::GameStructure, outdoorViewMul) == 7286796);
	static_assert(offsetof(mm8::GameStructure, speedModifiers) == 7288708);
	static_assert(offsetof(mm8::GameStructure, windowHandle) == 7289140);
	static_assert(offsetof(mm8::GameStructure, soundVolume) == 7289263);
	static_assert(offsetof(mm8::GameStructure, iconsLod) == 7394280);
	static_assert(offsetof(mm8::GameStructure, spritesLod) == 7466920);
	static_assert(offsetof(mm8::GameStructure, bitmapsLod) == 7527520);
	static_assert(offsetof(mm8::GameStructure, awardsTxt) == 7738952);
	static_assert(offsetof(mm8::GameStructure, awardsSort) == 7738956);
	static_assert(offsetof(mm8::GameStructure, streetNPC) == 7827872);
	static_assert(offsetof(mm8::GameStructure, streetNPC_size) == 7839732);
	static_assert(offsetof(mm8::GameStructure, merchantTxt) == 7840224);
	static_assert(offsetof(mm8::GameStructure, delayedFaceAnimation) == 11218944);
	static_assert(offsetof(mm8::GameStructure, IFTBin_size) == 11218956);
	static_assert(offsetof(mm8::GameStructure, IFTBin) == 11218960);
	static_assert(offsetof(mm8::GameStructure, PFTBin_size) == 11218964);
	static_assert(offsetof(mm8::GameStructure, PFTBin) == 11218968);
	static_assert(offsetof(mm8::GameStructure, time) == 11669180);
	static_assert(offsetof(mm8::GameStructure, guildNextRefill) == 11669700);
	static_assert(offsetof(mm8::GameStructure, year) == 11670956);
	static_assert(offsetof(mm8::GameStructure, month) == 11670960);
	static_assert(offsetof(mm8::GameStructure, weekOfMonth) == 11670964);
	static_assert(offsetof(mm8::GameStructure, dayOfMonth) == 11670968);
	static_assert(offsetof(mm8::GameStructure, hour) == 11670972);
	static_assert(offsetof(mm8::GameStructure, minute) == 11670976);
	static_assert(offsetof(mm8::GameStructure, second) == 11670980);
	static_assert(offsetof(mm8::GameStructure, turnBased) == 11671336);
	static_assert(offsetof(mm8::GameStructure, armageddonTimeLeft) == 12267012);
	static_assert(offsetof(mm8::GameStructure, armageddonSkill) == 12267016);
	static_assert(offsetof(mm8::GameStructure, turnBasedDelays) == 12267020);
	static_assert(offsetof(mm8::GameStructure, classNames) == 12267472);
	static_assert(offsetof(mm8::GameStructure, skillNames) == 12267616);
	static_assert(offsetof(mm8::GameStructure, screen) == 15473024);
	static_assert(offsetof(mm8::GameStructure, objectByPixel) == 15735220);
	static_assert(offsetof(mm8::GameStructure, windowed) == 15735256);
	static_assert(offsetof(mm8::GameStructure, rendererD3D) == 15735304);
	static_assert(offsetof(mm8::GameStructure, redbookHandle) == 16692740);
	static_assert(offsetof(mm8::GameStructure, MSSHandle) == 16692744);
	static_assert(offsetof(mm8::GameStructure, soundsBin_size) == 16695268);
	static_assert(offsetof(mm8::GameStructure, soundsBin) == 16695272);
	static_assert(offsetof(mm8::GameStructure, guildItemIconPtr) == 16765908);
	static_assert(offsetof(mm8::GameStructure, housePicType) == 16765956);
	static_assert(offsetof(mm8::GameStructure, houseScreen) == 16765960);
	static_assert(offsetof(mm8::GameStructure, houseAllowAction) == 16765972);
	static_assert(offsetof(mm8::GameStructure, houseActionInfo) == 16765976);
	static_assert(offsetof(mm8::GameStructure, houseTeachMastery) == 16765980);
	static_assert(offsetof(mm8::GameStructure, houseCost) == 16765984);
	static_assert(offsetof(mm8::GameStructure, houseItemsCount) == 16766032);
	static_assert(offsetof(mm8::GameStructure, NPCMessage) == 16766040);
	static_assert(offsetof(mm8::GameStructure, smackVideo) == 16768424);
	static_assert(offsetof(mm8::GameStructure, isMovieLooped) == 16768484);
	static_assert(offsetof(mm8::GameStructure, binkVideo) == 16768512);
	static_assert(offsetof(mm8::GameStructure, movieKind) == 16768540);
	static_assert(offsetof(mm8::GameStructure, inOODialog) == 16802124);
	static_assert(offsetof(mm8::GameStructure, patchOptions) == 53531492);
	static_assert(offsetof(mm8::GameStructure, customLods) == 55172608);
	static_assert(offsetof(mm8::GameStructure, NPCText) == 72876124);
	static_assert(offsetof(mm8::GameStructure, NPCTopic) == 72876120);
	static_assert(offsetof(mm8::GameStructure, transTxt) == 72898536);
	static_assert(offsetof(mm8::GameStructure, housesExtra) == 72901576);
	static_assert(offsetof(mm8::GameStructure, titleTrackOffset) == 72926512);
	static_assert(offsetof(mm8::GameStructure, monsterKinds) == 72926664);
	static_assert(offsetof(mm8::GameStructure, monsterKinds_size) == 72926668);
	static_assert(offsetof(mm8::GameStructure, missileSetup) == 73017512);
	static_assert(offsetof(mm8::GameStructure, missileSetup_size) == 73017516);
	static_assert(offsetof(mm8::GameStructure, characterPortraits) == 73293408);
	static_assert(offsetof(mm8::GameStructure, characterDollTypes) == 73580176);
	static_assert(offsetof(mm8::GameStructure, itemsTxt) == 73580508);
	static_assert(offsetof(mm8::GameStructure, stdItemsTxt) == 73686156);
	static_assert(offsetof(mm8::GameStructure, spcItemsTxt) == 73686736);
	static_assert(offsetof(mm8::GameStructure, mixPotions) == 73690616);
	static_assert(offsetof(mm8::GameStructure, NPCDataTxt) == 73711232);
	static_assert(offsetof(mm8::GameStructure, NPC) == 73804484);
	static_assert(offsetof(mm8::GameStructure, NPCNews) == 73897904);
	static_assert(offsetof(mm8::GameStructure, NPCGreet) == 73906520);
	static_assert(offsetof(mm8::GameStructure, NPCGroup) == 73909438);
	static_assert(offsetof(mm8::GameStructure, reagentSettings) == 73920248);
	static_assert(offsetof(mm8::GameStructure, mapFogChances) == 152177360);
	static_assert(offsetof(mm8::GameStructure, placeMonTxt) == 153537280);
	static_assert(offsetof(mm8::GameStructure, houses) == 284396912);
	static_assert(offsetof(mm8::GameStructure, mapDoorSound) == 285142896);
	static_assert(offsetof(mm8::GameStructure, houseMovies) == 285481952);
	static_assert(offsetof(mm8::GameStructure, mapStats) == 299322992);
	static_assert(offsetof(mm8::GameStructure, mapStats_size) == 299337136);
	static_assert(offsetof(mm8::GameStructure, monstersTxt) == 299565056);
	static_assert(offsetof(mm8::GameStructure, hostileTxt) == 299630592);
	static_assert(offsetof(mm8::GameStructure, questsTxt) == 445747184);
	static_assert(offsetof(mm8::GameStructure, autonoteTxt) == 445754400);
	static_assert(offsetof(mm8::GameStructure, autonoteCategory) == 445754404);
	static_assert(offsetof(mm8::GameStructure, globalEvtLines_size) == 480313416);
	static_assert(offsetof(mm8::GameStructure, globalEvtLines) == 480427065);
	static_assert(offsetof(mm8::GameStructure, transportIndex) == 480719860);
	static_assert(offsetof(mm8::GameStructure, shopNextRefill) == 480721534);
	static_assert(offsetof(mm8::GameStructure, guildNextRefill2) == 480723214);
	static_assert(offsetof(mm8::GameStructure, shopItems) == 480724390);
	static_assert(offsetof(mm8::GameStructure, shopTheftExpireTime) == 480723270);
	static_assert(offsetof(mm8::GameStructure, shopSpecialItems) == 480781846);
	static_assert(offsetof(mm8::GameStructure, guildItems) == 480839302);
}

#pragma pack(pop)