#pragma once
#include "pch.h"
#include "main.h"
#include "Common.h"

#pragma pack(push, 1)

namespace mm8
{
	struct FaceAnimationInfo // size: 0x8
	{
		union
		{
			std::array<uint8_t, 2> sounds; // MMExt: 1..2, here 0..1 | 0x0 (0 decimal)
			struct
			{
				uint8_t sound1; // 0x0 (0 decimal)
				uint8_t sound2; // 0x1 (1 decimal)
			};
		};
		SKIP(1);
		union
		{
			std::array<uint8_t, 5> expressions; // MMExt: 1..5, here 0..4 | 0x3 (3 decimal)
			struct
			{
				uint8_t expression1; // 0x3 (3 decimal)
				uint8_t expression2; // 0x4 (4 decimal)
				uint8_t expression3; // 0x5 (5 decimal)
				uint8_t expression4; // 0x6 (6 decimal)
				uint8_t expression5; // 0x7 (7 decimal)
			};
		};
	};
	static_assert(sizeof(mm8::FaceAnimationInfo) == 0x8, "Invalid \"mm8::FaceAnimationInfo\" structure size");
	static_assert(offsetof(mm8::FaceAnimationInfo, sound2) == 1);
	static_assert(offsetof(mm8::FaceAnimationInfo, expressions) == 3);
	static_assert(offsetof(mm8::FaceAnimationInfo, expression1) == 3);
	static_assert(offsetof(mm8::FaceAnimationInfo, expression2) == 4);
	static_assert(offsetof(mm8::FaceAnimationInfo, expression3) == 5);
	static_assert(offsetof(mm8::FaceAnimationInfo, expression4) == 6);
	static_assert(offsetof(mm8::FaceAnimationInfo, expression5) == 7);



	struct BaseBonus // size: 0x4
	{
		int16_t base; // 0x0 (0 decimal)
		int16_t bonus; // 0x2 (2 decimal)
	};
	static_assert(sizeof(mm8::BaseBonus) == 0x4, "Invalid \"mm8::BaseBonus\" structure size");
	static_assert(offsetof(mm8::BaseBonus, bonus) == 2);



	struct LloydBeaconSlot // size: 0x1C
	{
		int64_t expireTime; // 0x0 (0 decimal)
		union
		{
			std::array<int32_t, 3> pos; // 0x8 (8 decimal)
			struct
			{
				int32_t x; // 0x8 (8 decimal)
				int32_t y; // 0xC (12 decimal)
				int32_t z; // 0x10 (16 decimal)
			};
		};
		int16_t direction; // 0x14 (20 decimal)
		int16_t lookAngle; // 0x16 (22 decimal)
		int16_t active; // 0x18 (24 decimal)
		union
		{
			std::array<uint8_t, 2> map; // Unknown type | 0x1A (26 decimal)
			int16_t mapIndex; // 0x1A (26 decimal)
		};
	};
	static_assert(sizeof(mm8::LloydBeaconSlot) == 0x1C, "Invalid \"mm8::LloydBeaconSlot\" structure size");
	static_assert(offsetof(mm8::LloydBeaconSlot, pos) == 8);
	static_assert(offsetof(mm8::LloydBeaconSlot, x) == 8);
	static_assert(offsetof(mm8::LloydBeaconSlot, y) == 12);
	static_assert(offsetof(mm8::LloydBeaconSlot, z) == 16);
	static_assert(offsetof(mm8::LloydBeaconSlot, direction) == 20);
	static_assert(offsetof(mm8::LloydBeaconSlot, lookAngle) == 22);
	static_assert(offsetof(mm8::LloydBeaconSlot, active) == 24);
	static_assert(offsetof(mm8::LloydBeaconSlot, map) == 26);
	static_assert(offsetof(mm8::LloydBeaconSlot, mapIndex) == 26);



	struct Player // size: 0x1D28
	{
		union
		{
			std::array<int64_t, 20> conditions; // 0x0 (0 decimal)
			struct
			{
				int64_t cursed; // 0x0 (0 decimal)
				int64_t weak; // 0x8 (8 decimal)
				int64_t asleep; // 0x10 (16 decimal)
				int64_t afraid; // 0x18 (24 decimal)
				int64_t drunk; // 0x20 (32 decimal)
				int64_t insane; // 0x28 (40 decimal)
				int64_t poison1; // 0x30 (48 decimal)
				int64_t disease1; // 0x38 (56 decimal)
				int64_t poison2; // 0x40 (64 decimal)
				int64_t disease2; // 0x48 (72 decimal)
				int64_t poison3; // 0x50 (80 decimal)
				int64_t disease3; // 0x58 (88 decimal)
				int64_t paralyzed; // 0x60 (96 decimal)
				int64_t unconscious; // 0x68 (104 decimal)
				int64_t dead; // 0x70 (112 decimal)
				int64_t stoned; // 0x78 (120 decimal)
				int64_t eradicated; // 0x80 (128 decimal)
				int64_t zombie; // 0x88 (136 decimal)
				int64_t good; // 0x90 (144 decimal)
			};
		};
		union
		{
			int64_t exp; // 0xA0 (160 decimal)
			int64_t experience; // 0xA0 (160 decimal)
		};
		std::array<char, 32> name; // fixed size string, requires null terminator | 0xA8 (168 decimal)
		std::array<char, 256> biography; // fixed size string, requires null terminator | 0xC8 (200 decimal)
		SKIP(394);
		uint8_t clas; // 0x352 (850 decimal)
		uint8_t face; // 0x353 (851 decimal)
		union
		{
			struct // size: 0x1C, MMExt union
			{
				mm8::BaseBonus _0; // 0x354 (852 decimal)
				mm8::BaseBonus _1; // 0x358 (856 decimal)
				mm8::BaseBonus _2; // 0x35C (860 decimal)
				mm8::BaseBonus _3; // 0x360 (864 decimal)
				mm8::BaseBonus _5; // 0x364 (868 decimal)
				mm8::BaseBonus _4; // 0x368 (872 decimal)
				mm8::BaseBonus _6; // 0x36C (876 decimal)
			} stats;
			static_assert(sizeof(stats) == 0x1C, "Invalid \"stats\" structure size");
			struct
			{
				int16_t mightBase; // 0x354 (852 decimal)
				int16_t mightBonus; // 0x356 (854 decimal)
				int16_t intellectBase; // 0x358 (856 decimal)
				int16_t intellectBonus; // 0x35A (858 decimal)
				int16_t personalityBase; // 0x35C (860 decimal)
				int16_t personalityBonus; // 0x35E (862 decimal)
				int16_t enduranceBase; // 0x360 (864 decimal)
				int16_t enduranceBonus; // 0x362 (866 decimal)
				int16_t speedBase; // 0x364 (868 decimal)
				int16_t speedBonus; // 0x366 (870 decimal)
				int16_t accuracyBase; // 0x368 (872 decimal)
				int16_t accuracyBonus; // 0x36A (874 decimal)
				int16_t luckBase; // 0x36C (876 decimal)
				int16_t luckBonus; // 0x36E (878 decimal)
			};
		};
		int16_t armorClassBonus; // 0x370 (880 decimal)
		int16_t levelBase; // 0x372 (882 decimal)
		int16_t levelBonus; // 0x374 (884 decimal)
		int16_t ageBonus; // 0x376 (886 decimal)
		std::array<int16_t, 39> skills; // 0x378 (888 decimal)
		std::array<uint8_t, 13> awards; // MMExt: 1..104, here 0..103 | array of abits (real index = 7 - usual) | 0x3C6 (966 decimal)
		SKIP(51);
		std::array<bool, 132> spells; // MMExt: 1..132, here 0..131 | 0x406 (1030 decimal)
		SKIP(2);
		std::array<uint32_t, 7> usedBlackPotions; // MMExt: 264..270, here 0..6 | 4-byte boolean | 0x48C (1164 decimal)
		std::array<mm8::Item, 138> items; // MMExt: 1..138, here 0..137 | 0x4A8 (1192 decimal)
		std::array<int32_t, 126> inventory; // 0x1810 (6160 decimal)
		union
		{
			std::array<int16_t, 11> resistanceBase; // 0x1A08 (6664 decimal)
			struct
			{
				int16_t fireResistanceBase; // 0x1A08 (6664 decimal)
				int16_t airResistanceBase; // 0x1A0A (6666 decimal)
				int16_t waterResistanceBase; // 0x1A0C (6668 decimal)
				int16_t earthResistanceBase; // 0x1A0E (6670 decimal)
				SKIP(4);
				int16_t spiritResistanceBase; // 0x1A14 (6676 decimal)
				int16_t mindResistanceBase; // 0x1A16 (6678 decimal)
				int16_t bodyResistanceBase; // 0x1A18 (6680 decimal)
				int16_t lightResistanceBase; // 0x1A1A (6682 decimal)
				int16_t darkResistanceBase; // 0x1A1C (6684 decimal)
			};
		};
		union
		{
			std::array<int16_t, 11> resistanceBonus; // 0x1A1E (6686 decimal)
			struct
			{
				int16_t fireResistanceBonus; // 0x1A1E (6686 decimal)
				int16_t airResistanceBonus; // 0x1A20 (6688 decimal)
				int16_t waterResistanceBonus; // 0x1A22 (6690 decimal)
				int16_t earthResistanceBonus; // 0x1A24 (6692 decimal)
				SKIP(4);
				int16_t spiritResistanceBonus; // 0x1A2A (6698 decimal)
				int16_t mindResistanceBonus; // 0x1A2C (6700 decimal)
				int16_t bodyResistanceBonus; // 0x1A2E (6702 decimal)
				int16_t lightResistanceBonus; // 0x1A30 (6704 decimal)
				int16_t darkResistanceBonus; // 0x1A32 (6706 decimal)
			};
		};
		std::array<mm8::SpellBuff, 27> spellBuffs; // 0x1A34 (6708 decimal)
		int32_t voice; // 0x1BE4 (7140 decimal)
		SKIP(8);
		int16_t rosterBitIndex; // 0x1BF0 (7152 decimal)
		int16_t recoveryDelay; // 0x1BF2 (7154 decimal)
		int32_t skillPoints; // 0x1BF4 (7156 decimal)
		union
		{
			int32_t HP; // 0x1BF8 (7160 decimal)
			int32_t hitPoints; // 0x1BF8 (7160 decimal)
		};
		union
		{
			int32_t SP; // 0x1BFC (7164 decimal)
			int32_t spellPoints; // 0x1BFC (7164 decimal)
		};
		int32_t birthYear; // 0x1C00 (7168 decimal)
		union
		{
			std::array<int32_t, 16> equippedItems; // 0x1C04 (7172 decimal)
			struct
			{
				int32_t itemExtraHand; // 0x1C04 (7172 decimal)
				int32_t itemMainHand; // 0x1C08 (7176 decimal)
				int32_t itemBow; // 0x1C0C (7180 decimal)
				int32_t itemArmor; // 0x1C10 (7184 decimal)
				int32_t itemHelm; // 0x1C14 (7188 decimal)
				int32_t itemBelt; // 0x1C18 (7192 decimal)
				int32_t itemCloak; // 0x1C1C (7196 decimal)
				int32_t itemGountlets; // 0x1C20 (7200 decimal)
				int32_t itemBoots; // 0x1C24 (7204 decimal)
				int32_t itemAmulet; // 0x1C28 (7208 decimal)
				int32_t itemRing1; // 0x1C2C (7212 decimal)
				int32_t itemRing2; // 0x1C30 (7216 decimal)
				int32_t itemRing3; // 0x1C34 (7220 decimal)
				int32_t itemRing4; // 0x1C38 (7224 decimal)
				int32_t itemRing5; // 0x1C3C (7228 decimal)
				int32_t itemRing6; // 0x1C40 (7232 decimal)
			};
		};
		int8_t spellBookPage; // 0x1C44 (7236 decimal)
		uint8_t quickSpell; // 0x1C45 (7237 decimal)
		std::array<uint8_t, 64> playerBits; // MMExt: 1..512, here 0..511 | array of abits (real index = 7 - usual) | 0x1C46 (7238 decimal)
		int16_t expression; // 0x1C86 (7302 decimal)
		int16_t expressionTimePassed; // 0x1C88 (7304 decimal)
		int16_t expressionLength; // 0x1C8A (7306 decimal)
		SKIP(12);
		std::array<mm8::LloydBeaconSlot, 5> beacons; // 0x1C98 (7320 decimal)
		uint8_t devineInterventionCasts; // 0x1D24 (7460 decimal)
		uint8_t armageddonCasts; // 0x1D25 (7461 decimal)
		uint8_t fireSpikeCasts; // 0x1D26 (7462 decimal)
		SKIP(1);
	};
	static_assert(sizeof(mm8::Player) == 0x1D28, "Invalid \"mm8::Player\" structure size");
	static_assert(offsetof(mm8::Player, weak) == 8);
	static_assert(offsetof(mm8::Player, asleep) == 16);
	static_assert(offsetof(mm8::Player, afraid) == 24);
	static_assert(offsetof(mm8::Player, drunk) == 32);
	static_assert(offsetof(mm8::Player, insane) == 40);
	static_assert(offsetof(mm8::Player, poison1) == 48);
	static_assert(offsetof(mm8::Player, disease1) == 56);
	static_assert(offsetof(mm8::Player, poison2) == 64);
	static_assert(offsetof(mm8::Player, disease2) == 72);
	static_assert(offsetof(mm8::Player, poison3) == 80);
	static_assert(offsetof(mm8::Player, disease3) == 88);
	static_assert(offsetof(mm8::Player, paralyzed) == 96);
	static_assert(offsetof(mm8::Player, unconscious) == 104);
	static_assert(offsetof(mm8::Player, dead) == 112);
	static_assert(offsetof(mm8::Player, stoned) == 120);
	static_assert(offsetof(mm8::Player, eradicated) == 128);
	static_assert(offsetof(mm8::Player, zombie) == 136);
	static_assert(offsetof(mm8::Player, good) == 144);
	static_assert(offsetof(mm8::Player, exp) == 160);
	static_assert(offsetof(mm8::Player, experience) == 160);
	static_assert(offsetof(mm8::Player, name) == 168);
	static_assert(offsetof(mm8::Player, biography) == 200);
	static_assert(offsetof(mm8::Player, clas) == 850);
	static_assert(offsetof(mm8::Player, face) == 851);
	static_assert(offsetof(mm8::Player, stats) == 852);
	static_assert(offsetof(mm8::Player, mightBase) == 852);
	static_assert(offsetof(mm8::Player, mightBonus) == 854);
	static_assert(offsetof(mm8::Player, intellectBase) == 856);
	static_assert(offsetof(mm8::Player, intellectBonus) == 858);
	static_assert(offsetof(mm8::Player, personalityBase) == 860);
	static_assert(offsetof(mm8::Player, personalityBonus) == 862);
	static_assert(offsetof(mm8::Player, enduranceBase) == 864);
	static_assert(offsetof(mm8::Player, enduranceBonus) == 866);
	static_assert(offsetof(mm8::Player, speedBase) == 868);
	static_assert(offsetof(mm8::Player, speedBonus) == 870);
	static_assert(offsetof(mm8::Player, accuracyBase) == 872);
	static_assert(offsetof(mm8::Player, accuracyBonus) == 874);
	static_assert(offsetof(mm8::Player, luckBase) == 876);
	static_assert(offsetof(mm8::Player, luckBonus) == 878);
	static_assert(offsetof(mm8::Player, armorClassBonus) == 880);
	static_assert(offsetof(mm8::Player, levelBase) == 882);
	static_assert(offsetof(mm8::Player, levelBonus) == 884);
	static_assert(offsetof(mm8::Player, ageBonus) == 886);
	static_assert(offsetof(mm8::Player, skills) == 888);
	static_assert(offsetof(mm8::Player, spells) == 1030);
	static_assert(offsetof(mm8::Player, usedBlackPotions) == 1164);
	static_assert(offsetof(mm8::Player, items) == 1192);
	static_assert(offsetof(mm8::Player, inventory) == 6160);
	static_assert(offsetof(mm8::Player, resistanceBase) == 6664);
	static_assert(offsetof(mm8::Player, fireResistanceBase) == 6664);
	static_assert(offsetof(mm8::Player, airResistanceBase) == 6666);
	static_assert(offsetof(mm8::Player, waterResistanceBase) == 6668);
	static_assert(offsetof(mm8::Player, earthResistanceBase) == 6670);
	static_assert(offsetof(mm8::Player, spiritResistanceBase) == 6676);
	static_assert(offsetof(mm8::Player, mindResistanceBase) == 6678);
	static_assert(offsetof(mm8::Player, bodyResistanceBase) == 6680);
	static_assert(offsetof(mm8::Player, lightResistanceBase) == 6682);
	static_assert(offsetof(mm8::Player, darkResistanceBase) == 6684);
	static_assert(offsetof(mm8::Player, resistanceBonus) == 6686);
	static_assert(offsetof(mm8::Player, fireResistanceBonus) == 6686);
	static_assert(offsetof(mm8::Player, airResistanceBonus) == 6688);
	static_assert(offsetof(mm8::Player, waterResistanceBonus) == 6690);
	static_assert(offsetof(mm8::Player, earthResistanceBonus) == 6692);
	static_assert(offsetof(mm8::Player, spiritResistanceBonus) == 6698);
	static_assert(offsetof(mm8::Player, mindResistanceBonus) == 6700);
	static_assert(offsetof(mm8::Player, bodyResistanceBonus) == 6702);
	static_assert(offsetof(mm8::Player, lightResistanceBonus) == 6704);
	static_assert(offsetof(mm8::Player, darkResistanceBonus) == 6706);
	static_assert(offsetof(mm8::Player, spellBuffs) == 6708);
	static_assert(offsetof(mm8::Player, voice) == 7140);
	static_assert(offsetof(mm8::Player, rosterBitIndex) == 7152);
	static_assert(offsetof(mm8::Player, recoveryDelay) == 7154);
	static_assert(offsetof(mm8::Player, skillPoints) == 7156);
	static_assert(offsetof(mm8::Player, HP) == 7160);
	static_assert(offsetof(mm8::Player, hitPoints) == 7160);
	static_assert(offsetof(mm8::Player, SP) == 7164);
	static_assert(offsetof(mm8::Player, spellPoints) == 7164);
	static_assert(offsetof(mm8::Player, birthYear) == 7168);
	static_assert(offsetof(mm8::Player, equippedItems) == 7172);
	static_assert(offsetof(mm8::Player, itemExtraHand) == 7172);
	static_assert(offsetof(mm8::Player, itemMainHand) == 7176);
	static_assert(offsetof(mm8::Player, itemBow) == 7180);
	static_assert(offsetof(mm8::Player, itemArmor) == 7184);
	static_assert(offsetof(mm8::Player, itemHelm) == 7188);
	static_assert(offsetof(mm8::Player, itemBelt) == 7192);
	static_assert(offsetof(mm8::Player, itemCloak) == 7196);
	static_assert(offsetof(mm8::Player, itemGountlets) == 7200);
	static_assert(offsetof(mm8::Player, itemBoots) == 7204);
	static_assert(offsetof(mm8::Player, itemAmulet) == 7208);
	static_assert(offsetof(mm8::Player, itemRing1) == 7212);
	static_assert(offsetof(mm8::Player, itemRing2) == 7216);
	static_assert(offsetof(mm8::Player, itemRing3) == 7220);
	static_assert(offsetof(mm8::Player, itemRing4) == 7224);
	static_assert(offsetof(mm8::Player, itemRing5) == 7228);
	static_assert(offsetof(mm8::Player, itemRing6) == 7232);
	static_assert(offsetof(mm8::Player, spellBookPage) == 7236);
	static_assert(offsetof(mm8::Player, quickSpell) == 7237);
	static_assert(offsetof(mm8::Player, expression) == 7302);
	static_assert(offsetof(mm8::Player, expressionTimePassed) == 7304);
	static_assert(offsetof(mm8::Player, expressionLength) == 7306);
	static_assert(offsetof(mm8::Player, beacons) == 7320);
	static_assert(offsetof(mm8::Player, devineInterventionCasts) == 7460);
	static_assert(offsetof(mm8::Player, armageddonCasts) == 7461);
	static_assert(offsetof(mm8::Player, fireSpikeCasts) == 7462);
}

#pragma pack(pop)