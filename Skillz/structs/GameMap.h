#pragma once
#include "pch.h"
#include "main.h"
#include "Common.h"
#include "MapModel.h"
#include "MapElements.h"
#include "MapMisc.h"
#include "Monster.h"

#pragma pack(push, 1)

namespace mm8
{
	struct MapExtra // size: 0x38
	{
		int64_t lastVisitTime; // 0x0 (0 decimal)
		std::array<char, 12> skyBitmap; // fixed size string, doesn't require null terminator | 0x8 (8 decimal)
		union
		{
			uint32_t dayBits; // 0x14 (20 decimal)
			struct
			{
				bool foggy : 1; // 0x14 (20 decimal), bit index 7
			};
		};
		int32_t fogRange1; // 0x18 (24 decimal)
		int32_t fogRange2; // 0x1C (28 decimal)
		union
		{
			uint32_t bits; // 0x20 (32 decimal)
			struct
			{
				bool raining : 1; // 0x20 (32 decimal), bit index 7
				bool snowing : 1; // 0x20 (32 decimal), bit index 6
				bool underwater : 1; // 0x20 (32 decimal), bit index 5
				bool noTerrain : 1; // 0x20 (32 decimal), bit index 4
				bool alwaysDark : 1; // 0x20 (32 decimal), bit index 3
				bool alwaysLight : 1; // 0x21 (33 decimal), bit index 2
				bool alwaysFoggy : 1; // 0x21 (33 decimal), bit index 1
				bool redFog : 1; // 0x21 (33 decimal), bit index 0
			};
		};
		int32_t ceiling; // 0x24 (36 decimal)
		uint32_t lastWeeklyTimer; // 0x28 (40 decimal)
		uint32_t lastMonthlyTimer; // 0x2C (44 decimal)
		uint32_t lastYearlyTimer; // 0x30 (48 decimal)
		uint32_t lastDailyTimer; // 0x34 (52 decimal)
	};
	static_assert(sizeof(mm8::MapExtra) == 0x38, "Invalid \"mm8::MapExtra\" structure size");
	static_assert(offsetof(mm8::MapExtra, skyBitmap) == 8);
	static_assert(offsetof(mm8::MapExtra, dayBits) == 20);
	static_assert(offsetof(mm8::MapExtra, fogRange1) == 24);
	static_assert(offsetof(mm8::MapExtra, fogRange2) == 28);
	static_assert(offsetof(mm8::MapExtra, bits) == 32);
	static_assert(offsetof(mm8::MapExtra, ceiling) == 36);
	static_assert(offsetof(mm8::MapExtra, lastWeeklyTimer) == 40);
	static_assert(offsetof(mm8::MapExtra, lastMonthlyTimer) == 44);
	static_assert(offsetof(mm8::MapExtra, lastYearlyTimer) == 48);
	static_assert(offsetof(mm8::MapExtra, lastDailyTimer) == 52);



	struct MapObject // size: 0x70
	{
		int16_t type; // 0x0 (0 decimal)
		int16_t typeIndex; // 0x2 (2 decimal)
		union
		{
			std::array<int32_t, 3> pos; // 0x4 (4 decimal)
			struct
			{
				int32_t x; // 0x4 (4 decimal)
				int32_t y; // 0x8 (8 decimal)
				int32_t z; // 0xC (12 decimal)
			};
		};
		union
		{
			std::array<int16_t, 3> velocity; // 0x10 (16 decimal)
			struct
			{
				int16_t velocityX; // 0x10 (16 decimal)
				int16_t velocityY; // 0x12 (18 decimal)
				int16_t velocityZ; // 0x14 (20 decimal)
			};
		};
		int16_t direction; // 0x16 (22 decimal)
		int16_t lookAngle; // 0x18 (24 decimal)
		union
		{
			uint16_t bits; // 0x1A (26 decimal)
			struct
			{
				bool visible : 1; // 0x1A (26 decimal), bit index 7
				bool temporary : 1; // 0x1A (26 decimal), bit index 6
				bool haltTurnBased : 1; // 0x1A (26 decimal), bit index 5
				bool droppedByPlayer : 1; // 0x1A (26 decimal), bit index 4
				bool ignoreRange : 1; // 0x1A (26 decimal), bit index 3
				bool noZBuffer : 1; // 0x1B (27 decimal), bit index 2
				bool skipAFrame : 1; // 0x1B (27 decimal), bit index 1
				bool attachToHead : 1; // 0x1B (27 decimal), bit index 0
				bool missile : 1; // 0x1B (27 decimal), bit index 7
				bool removed : 1; // 0x1B (27 decimal), bit index 6
			};
		};
		int16_t room; // 0x1C (28 decimal)
		int16_t age; // 0x1E (30 decimal)
		int16_t maxAge; // 0x20 (32 decimal)
		int16_t lightMultiplier; // 0x22 (34 decimal)
		mm8::Item item; // 0x24 (36 decimal)
		union
		{
			int32_t spell; // 0x48 (72 decimal)
			int32_t spellType; // 0x48 (72 decimal)
		};
		int32_t spellSkill; // 0x4C (76 decimal)
		union
		{
			int32_t spellLevel; // 0x50 (80 decimal)
			int32_t spellMastery; // 0x50 (80 decimal)
		};
		mm8::SpellEffect* spellEffect; // 0x54 (84 decimal)
		int32_t owner; // 0x58 (88 decimal)
		int32_t target; // 0x5C (92 decimal)
		uint8_t range; // 0x60 (96 decimal)
		uint8_t attackType; // 0x61 (97 decimal)
		SKIP(2);
		union
		{
			std::array<int32_t, 3> startPos; // 0x64 (100 decimal)
			struct
			{
				int32_t startX; // 0x64 (100 decimal)
				int32_t startY; // 0x68 (104 decimal)
				int32_t startZ; // 0x6C (108 decimal)
			};
		};
	};
	static_assert(sizeof(mm8::MapObject) == 0x70, "Invalid \"mm8::MapObject\" structure size");
	static_assert(offsetof(mm8::MapObject, typeIndex) == 2);
	static_assert(offsetof(mm8::MapObject, pos) == 4);
	static_assert(offsetof(mm8::MapObject, x) == 4);
	static_assert(offsetof(mm8::MapObject, y) == 8);
	static_assert(offsetof(mm8::MapObject, z) == 12);
	static_assert(offsetof(mm8::MapObject, velocity) == 16);
	static_assert(offsetof(mm8::MapObject, velocityX) == 16);
	static_assert(offsetof(mm8::MapObject, velocityY) == 18);
	static_assert(offsetof(mm8::MapObject, velocityZ) == 20);
	static_assert(offsetof(mm8::MapObject, direction) == 22);
	static_assert(offsetof(mm8::MapObject, lookAngle) == 24);
	static_assert(offsetof(mm8::MapObject, bits) == 26);
	static_assert(offsetof(mm8::MapObject, room) == 28);
	static_assert(offsetof(mm8::MapObject, age) == 30);
	static_assert(offsetof(mm8::MapObject, maxAge) == 32);
	static_assert(offsetof(mm8::MapObject, lightMultiplier) == 34);
	static_assert(offsetof(mm8::MapObject, item) == 36);
	static_assert(offsetof(mm8::MapObject, spell) == 72);
	static_assert(offsetof(mm8::MapObject, spellType) == 72);
	static_assert(offsetof(mm8::MapObject, spellSkill) == 76);
	static_assert(offsetof(mm8::MapObject, spellLevel) == 80);
	static_assert(offsetof(mm8::MapObject, spellMastery) == 80);
	static_assert(offsetof(mm8::MapObject, spellEffect) == 84);
	static_assert(offsetof(mm8::MapObject, owner) == 88);
	static_assert(offsetof(mm8::MapObject, target) == 92);
	static_assert(offsetof(mm8::MapObject, range) == 96);
	static_assert(offsetof(mm8::MapObject, attackType) == 97);
	static_assert(offsetof(mm8::MapObject, startPos) == 100);
	static_assert(offsetof(mm8::MapObject, startX) == 100);
	static_assert(offsetof(mm8::MapObject, startY) == 104);
	static_assert(offsetof(mm8::MapObject, startZ) == 108);



	struct GameMap // size: 0x7AB360
	{
		std::array<mm8::MapMonster, 1500> monsters; // size field offset is 0x692FB0 | 0x0 (0 decimal)
		SKIP(3956748);
		std::array<mm8::BaseLight, 400> spriteLights; // size field offset is 0x52B21C | 0x529F5C (5414748 decimal)
		uint32_t spriteLights_size; // 0x52B21C (5419548 decimal)
		SKIP(880216);
		std::array<uint8_t, 200> vars; // 0x602078 (6299768 decimal)
		SKIP(1016);
		std::array<mm8::MapChest, 20> chests; // size field offset is 0x61C528 | 0x602538 (6300984 decimal)
		uint32_t chests_size; // 0x61C528 (6407464 decimal)
		SKIP(486020);
		uint32_t monsters_size; // 0x692FB0 (6893488 decimal)
		uint32_t objects_size; // 0x692FB4 (6893492 decimal)
		std::array<mm8::MapObject, 1000> objects; // size field offset is 0x692FB4 | 0x692FB8 (6893496 decimal)
		SKIP(8);
		std::array<int32_t, 10> soundSprites; // size field offset is 0x6AE568 | 0x6AE540 (7005504 decimal)
		uint32_t soundSprites_size; // 0x6AE568 (7005544 decimal)
		SKIP(12012);
		std::array<mm8::MapSprite, 3000> sprites; // size field offset is 0x6C8B58 | 0x6B1458 (7017560 decimal)
		uint32_t sprites_size; // 0x6C8B58 (7113560 decimal)
		SKIP(24692);
		union
		{
			mm8::OdmHeader outdoorHeader; // 0x6CEBD0 (7138256 decimal)
			struct
			{
				SKIP(95);
				uint8_t tilesetsFile; // 0x6CEC2F (7138351 decimal)
				SKIP(64);
				std::array<mm8::TilesetDef, 4> tilesets; // 0x6CEC70 (7138416 decimal)
			};
		};
		uint32_t models_size; // 0x6CEC84 (7138436 decimal)
		std::array<std::array<uint8_t, 128>, 128>* heightMap; // 0x6CEC88 (7138440 decimal)
		std::array<std::array<uint8_t, 128>, 128>* tileMap; // 0x6CEC8C (7138444 decimal)
		union
		{
			std::array<std::array<uint8_t, 128>, 128>* unknownMap; // 0x6CEC90 (7138448 decimal)
			std::array<std::array<uint8_t, 128>, 128>* unknownMap2; // 0x6CEC90 (7138448 decimal)
		};
		SKIP(24);
		mm8::MapModel* models; // size field offset is 0x6CEC84 | 0x6CECAC (7138476 decimal)
		uint32_t IDList_size; // 0x6CECB0 (7138480 decimal)
		mm8::ObjectRef2* IDList; // size field offset is 0x6CECB0 | 0x6CECB4 (7138484 decimal)
		std::array<std::array<int32_t, 128>, 128>* IDOffsets; // 0x6CECB8 (7138488 decimal)
		int32_t loadedSkyBitmap; // 0x6CECBC (7138492 decimal)
		SKIP(980);
		uint32_t outdoorSpawns_size; // 0x6CF094 (7139476 decimal)
		mm8::SpawnPoint* outdoorSpawns; // size field offset is 0x6CF094 | 0x6CF098 (7139480 decimal)
		int32_t outdoorRefillCount; // 0x6CF09C (7139484 decimal)
		int32_t outdoorLastRefillDay; // 0x6CF0A0 (7139488 decimal)
		SKIP(8);
		int32_t outdoorReputation; // 0x6CF0AC (7139500 decimal)
		SKIP(28);
		union
		{
			mm8::MapExtra outdoorExtra; // 0x6CF0CC (7139532 decimal)
			struct
			{
				int64_t outdoorLastVisitTime; // 0x6CF0CC (7139532 decimal)
			};
		};
		std::array<std::array<uint8_t, 11>, 88> visibleMap1; // array of abits (real index = 7 - usual) | 0x6CF104 (7139588 decimal)
		std::array<std::array<uint8_t, 11>, 88> visibleMap2; // array of abits (real index = 7 - usual) | 0x6CF4CC (7140556 decimal)
		std::array<mm8::MapNote, 100> notes; // 0x6CF894 (7141524 decimal)
		SKIP(114856);
		int32_t mapStatsIndex; // 0x6F30C4 (7286980 decimal)
		SKIP(2236);
		std::array<uint8_t, 20> name; // Unknown type | 0x6F3984 (7289220 decimal)
		SKIP(8);
		uint32_t indoorOrOutdoor; // 0x6F39A0 (7289248 decimal)
		SKIP(588);
		mm8::BlvHeader indoorHeader; // 0x6F3BF0 (7289840 decimal)
		uint32_t vertexes_size; // 0x6F3C78 (7289976 decimal)
		mm8::MapVertex* vertexes; // size field offset is 0x6F3C78 | 0x6F3C7C (7289980 decimal)
		uint32_t facets_size; // 0x6F3C80 (7289984 decimal)
		mm8::MapFacet* facets; // size field offset is 0x6F3C80 | 0x6F3C84 (7289988 decimal)
		uint32_t facetData_size; // 0x6F3C88 (7289992 decimal)
		mm8::FacetData* facetData; // size field offset is 0x6F3C88 | 0x6F3C8C (7289996 decimal)
		uint32_t rooms_size; // 0x6F3C90 (7290000 decimal)
		mm8::MapRoom* rooms; // size field offset is 0x6F3C90 | 0x6F3C94 (7290004 decimal)
		uint32_t lights_size; // 0x6F3C98 (7290008 decimal)
		mm8::MapLight* lights; // size field offset is 0x6F3C98 | 0x6F3C9C (7290012 decimal)
		uint32_t doors_size; // 0x6F3CA0 (7290016 decimal)
		std::array<mm8::MapDoor, 200>* doors; // size field offset is 0x6F3CA0 | 0x6F3CA4 (7290020 decimal)
		uint32_t BSPNodes_size; // 0x6F3CA8 (7290024 decimal)
		mm8::BSPNode* BSPNodes; // size field offset is 0x6F3CA8 | 0x6F3CAC (7290028 decimal)
		mm8::MapOutlines* outlines; // 0x6F3CB0 (7290032 decimal)
		SKIP(16);
		uint32_t indoorSpawns_size; // 0x6F3CC4 (7290052 decimal)
		mm8::SpawnPoint* indoorSpawns; // size field offset is 0x6F3CC4 | 0x6F3CC8 (7290056 decimal)
		int32_t indoorRefillCount; // 0x6F3CCC (7290060 decimal)
		int32_t indoorLastRefillDay; // 0x6F3CD0 (7290064 decimal)
		int32_t indoorReputation; // 0x6F3CD4 (7290068 decimal)
		// uint8_t* lastRefillDay; // real size is 0 | Unknown type | 0x6F3CD8 (7290072 decimal)
		// uint8_t* lastVisitTime; // real size is 0 | Unknown type | 0x6F3CD8 (7290072 decimal)
		// uint8_t* refillCount; // real size is 0 | Unknown type | 0x6F3CD8 (7290072 decimal)
		// uint8_t* reputation; // real size is 0 | Unknown type | 0x6F3CD8 (7290072 decimal)
		// uint8_t* spawns; // real size is 0 | Unknown type | 0x6F3CD8 (7290072 decimal)
		SKIP(28);
		union
		{
			mm8::MapExtra indoorExtra; // 0x6F3CF4 (7290100 decimal)
			struct
			{
				int64_t indoorLastVisitTime; // 0x6F3CF4 (7290100 decimal)
			};
		};
		std::array<uint8_t, 875> visibileOutlines; // array of abits (real index = 7 - usual) | 0x6F3D2C (7290156 decimal)
		SKIP(553665);
		uint32_t terNorm_size; // 0x77B358 (7844696 decimal)
		std::array<std::array<std::array<float, 2>, 128>, 128> terNormDist; // 0x77B35C (7844700 decimal)
		std::array<std::array<std::array<int16_t, 2>, 128>, 128> terNormId; // 0x79B35C (7975772 decimal)
		mm8::FloatVector* terNorm; // size field offset is 0x77B358 | 0x7AB35C (8041308 decimal)
	};
	static_assert(sizeof(mm8::GameMap) == 0x7AB360, "Invalid \"mm8::GameMap\" structure size");
	static_assert(offsetof(mm8::GameMap, spriteLights) == 5414748);
	static_assert(offsetof(mm8::GameMap, spriteLights_size) == 5419548);
	static_assert(offsetof(mm8::GameMap, vars) == 6299768);
	static_assert(offsetof(mm8::GameMap, chests) == 6300984);
	static_assert(offsetof(mm8::GameMap, chests_size) == 6407464);
	static_assert(offsetof(mm8::GameMap, monsters_size) == 6893488);
	static_assert(offsetof(mm8::GameMap, objects_size) == 6893492);
	static_assert(offsetof(mm8::GameMap, objects) == 6893496);
	static_assert(offsetof(mm8::GameMap, soundSprites) == 7005504);
	static_assert(offsetof(mm8::GameMap, soundSprites_size) == 7005544);
	static_assert(offsetof(mm8::GameMap, sprites) == 7017560);
	static_assert(offsetof(mm8::GameMap, sprites_size) == 7113560);
	static_assert(offsetof(mm8::GameMap, outdoorHeader) == 7138256);
	static_assert(offsetof(mm8::GameMap, tilesetsFile) == 7138351);
	static_assert(offsetof(mm8::GameMap, tilesets) == 7138416);
	static_assert(offsetof(mm8::GameMap, models_size) == 7138436);
	static_assert(offsetof(mm8::GameMap, heightMap) == 7138440);
	static_assert(offsetof(mm8::GameMap, tileMap) == 7138444);
	static_assert(offsetof(mm8::GameMap, unknownMap) == 7138448);
	static_assert(offsetof(mm8::GameMap, unknownMap2) == 7138448);
	static_assert(offsetof(mm8::GameMap, models) == 7138476);
	static_assert(offsetof(mm8::GameMap, IDList_size) == 7138480);
	static_assert(offsetof(mm8::GameMap, IDList) == 7138484);
	static_assert(offsetof(mm8::GameMap, IDOffsets) == 7138488);
	static_assert(offsetof(mm8::GameMap, loadedSkyBitmap) == 7138492);
	static_assert(offsetof(mm8::GameMap, outdoorSpawns_size) == 7139476);
	static_assert(offsetof(mm8::GameMap, outdoorSpawns) == 7139480);
	static_assert(offsetof(mm8::GameMap, outdoorRefillCount) == 7139484);
	static_assert(offsetof(mm8::GameMap, outdoorLastRefillDay) == 7139488);
	static_assert(offsetof(mm8::GameMap, outdoorReputation) == 7139500);
	static_assert(offsetof(mm8::GameMap, outdoorExtra) == 7139532);
	static_assert(offsetof(mm8::GameMap, outdoorLastVisitTime) == 7139532);
	static_assert(offsetof(mm8::GameMap, notes) == 7141524);
	static_assert(offsetof(mm8::GameMap, mapStatsIndex) == 7286980);
	static_assert(offsetof(mm8::GameMap, name) == 7289220);
	static_assert(offsetof(mm8::GameMap, indoorOrOutdoor) == 7289248);
	static_assert(offsetof(mm8::GameMap, indoorHeader) == 7289840);
	static_assert(offsetof(mm8::GameMap, vertexes_size) == 7289976);
	static_assert(offsetof(mm8::GameMap, vertexes) == 7289980);
	static_assert(offsetof(mm8::GameMap, facets_size) == 7289984);
	static_assert(offsetof(mm8::GameMap, facets) == 7289988);
	static_assert(offsetof(mm8::GameMap, facetData_size) == 7289992);
	static_assert(offsetof(mm8::GameMap, facetData) == 7289996);
	static_assert(offsetof(mm8::GameMap, rooms_size) == 7290000);
	static_assert(offsetof(mm8::GameMap, rooms) == 7290004);
	static_assert(offsetof(mm8::GameMap, lights_size) == 7290008);
	static_assert(offsetof(mm8::GameMap, lights) == 7290012);
	static_assert(offsetof(mm8::GameMap, doors_size) == 7290016);
	static_assert(offsetof(mm8::GameMap, doors) == 7290020);
	static_assert(offsetof(mm8::GameMap, BSPNodes_size) == 7290024);
	static_assert(offsetof(mm8::GameMap, BSPNodes) == 7290028);
	static_assert(offsetof(mm8::GameMap, outlines) == 7290032);
	static_assert(offsetof(mm8::GameMap, indoorSpawns_size) == 7290052);
	static_assert(offsetof(mm8::GameMap, indoorSpawns) == 7290056);
	static_assert(offsetof(mm8::GameMap, indoorRefillCount) == 7290060);
	static_assert(offsetof(mm8::GameMap, indoorLastRefillDay) == 7290064);
	static_assert(offsetof(mm8::GameMap, indoorReputation) == 7290068);
	static_assert(offsetof(mm8::GameMap, indoorExtra) == 7290100);
	static_assert(offsetof(mm8::GameMap, indoorLastVisitTime) == 7290100);
	static_assert(offsetof(mm8::GameMap, terNorm_size) == 7844696);
	static_assert(offsetof(mm8::GameMap, terNormDist) == 7844700);
	static_assert(offsetof(mm8::GameMap, terNormId) == 7975772);
	static_assert(offsetof(mm8::GameMap, terNorm) == 8041308);
}

#pragma pack(pop)