#pragma once
#include "pch.h"
#include "main.h"

#pragma pack(push, 1)

namespace mm8
{
	struct ReagentSettings // size: 0x4
	{
		uint16_t item; // 0x0 (0 decimal)
		uint16_t result; // 0x2 (2 decimal)
	};
	static_assert(sizeof(mm8::ReagentSettings) == 0x4, "Invalid \"mm8::ReagentSettings\" structure size");
	static_assert(offsetof(mm8::ReagentSettings, result) == 2);



	struct HousesExtra // size: 0x4
	{
		int16_t indexByType; // 0x0 (0 decimal)
		int16_t map; // 0x2 (2 decimal)
	};
	static_assert(sizeof(mm8::HousesExtra) == 0x4, "Invalid \"mm8::HousesExtra\" structure size");
	static_assert(offsetof(mm8::HousesExtra, map) == 2);



	struct WeaponShopRule // size: 0xA
	{
		int16_t quality; // 0x0 (0 decimal)
		std::array<int16_t, 4> itemTypes; // MMExt: 1..4, here 0..3 | 0x2 (2 decimal)
	};
	static_assert(sizeof(mm8::WeaponShopRule) == 0xA, "Invalid \"mm8::WeaponShopRule\" structure size");
	static_assert(offsetof(mm8::WeaponShopRule, itemTypes) == 2);



	struct ArmorShopRule // size: 0x14
	{
		int16_t qualityShelf; // 0x0 (0 decimal)
		std::array<int16_t, 4> itemTypesShelf; // MMExt: 1..4, here 0..3 | 0x2 (2 decimal)
		int16_t qualityCase; // 0xA (10 decimal)
		std::array<int16_t, 4> itemTypesCase; // MMExt: 1..4, here 0..3 | 0xC (12 decimal)
	};
	static_assert(sizeof(mm8::ArmorShopRule) == 0x14, "Invalid \"mm8::ArmorShopRule\" structure size");
	static_assert(offsetof(mm8::ArmorShopRule, itemTypesShelf) == 2);
	static_assert(offsetof(mm8::ArmorShopRule, qualityCase) == 10);
	static_assert(offsetof(mm8::ArmorShopRule, itemTypesCase) == 12);



	struct ArcomageRule // size: 0x18
	{
		int16_t towerToWin; // 0x0 (0 decimal)
		int16_t resToWin; // 0x2 (2 decimal)
		int16_t towerAtStart; // 0x4 (4 decimal)
		int16_t wallAtStart; // 0x6 (6 decimal)
		int16_t quarry; // 0x8 (8 decimal)
		int16_t magic; // 0xA (10 decimal)
		int16_t dungeon; // 0xC (12 decimal)
		int16_t bricks; // 0xE (14 decimal)
		int16_t gems; // 0x10 (16 decimal)
		int16_t recruits; // 0x12 (18 decimal)
		int32_t ai; // 0x14 (20 decimal)
	};
	static_assert(sizeof(mm8::ArcomageRule) == 0x18, "Invalid \"mm8::ArcomageRule\" structure size");
	static_assert(offsetof(mm8::ArcomageRule, resToWin) == 2);
	static_assert(offsetof(mm8::ArcomageRule, towerAtStart) == 4);
	static_assert(offsetof(mm8::ArcomageRule, wallAtStart) == 6);
	static_assert(offsetof(mm8::ArcomageRule, quarry) == 8);
	static_assert(offsetof(mm8::ArcomageRule, magic) == 10);
	static_assert(offsetof(mm8::ArcomageRule, dungeon) == 12);
	static_assert(offsetof(mm8::ArcomageRule, bricks) == 14);
	static_assert(offsetof(mm8::ArcomageRule, gems) == 16);
	static_assert(offsetof(mm8::ArcomageRule, recruits) == 18);
	static_assert(offsetof(mm8::ArcomageRule, ai) == 20);



	struct ShopRule // size: 0x2
	{
		int16_t quality; // 0x0 (0 decimal)
	};
	static_assert(sizeof(mm8::ShopRule) == 0x2, "Invalid \"mm8::ShopRule\" structure size");



	struct HouseRules // size: 0x1CA73A7E
	{
		SKIP(480717328);
		std::array<mm8::WeaponShopRule, 35> weaponShopsStandart; // MMExt: 1..35, here 0..34 | 0x1CA72A10 (480717328 decimal)
		std::array<mm8::WeaponShopRule, 35> weaponShopsSpecial; // MMExt: 1..35, here 0..34 | 0x1CA72B6E (480717678 decimal)
		std::array<mm8::ArmorShopRule, 40> armorShopsStandart; // MMExt: 1..40, here 0..39 | 0x1CA72CCC (480718028 decimal)
		std::array<mm8::ArmorShopRule, 40> armorShopsSpecial; // MMExt: 1..40, here 0..39 | 0x1CA72FEC (480718828 decimal)
		std::array<mm8::ShopRule, 36> magicShopsStandart; // MMExt: 1..36, here 0..35 | 0x1CA7330C (480719628 decimal)
		std::array<mm8::ShopRule, 36> magicShopsSpecial; // MMExt: 1..36, here 0..35 | 0x1CA73354 (480719700 decimal)
		std::array<mm8::ShopRule, 22> alchemistsStandart; // MMExt: 1..22, here 0..21 | 0x1CA7339C (480719772 decimal)
		std::array<mm8::ShopRule, 22> alchemistsSpecial; // MMExt: 1..22, here 0..21 | 0x1CA733C8 (480719816 decimal)
		SKIP(224);
		std::array<mm8::ShopRule, 7> spellbookShops; // MMExt: 1..7, here 0..6 | 0x1CA734D4 (480720084 decimal)
		std::array<mm8::ShopRule, 29> training; // MMExt: 1..29, here 0..28 | 0x1CA734E2 (480720098 decimal)
		std::array<mm8::ArcomageRule, 53> arcomage; // MMExt: 1..53, here 0..52 | 0x1CA7351C (480720156 decimal)
		std::array<int16_t, 53> arcomageTexts; // MMExt: 1..53, here 0..52 | 0x1CA73A14 (480721428 decimal)
	};
	static_assert(sizeof(mm8::HouseRules) == 0x1CA73A7E, "Invalid \"mm8::HouseRules\" structure size");
	static_assert(offsetof(mm8::HouseRules, weaponShopsStandart) == 480717328);
	static_assert(offsetof(mm8::HouseRules, weaponShopsSpecial) == 480717678);
	static_assert(offsetof(mm8::HouseRules, armorShopsStandart) == 480718028);
	static_assert(offsetof(mm8::HouseRules, armorShopsSpecial) == 480718828);
	static_assert(offsetof(mm8::HouseRules, magicShopsStandart) == 480719628);
	static_assert(offsetof(mm8::HouseRules, magicShopsSpecial) == 480719700);
	static_assert(offsetof(mm8::HouseRules, alchemistsStandart) == 480719772);
	static_assert(offsetof(mm8::HouseRules, alchemistsSpecial) == 480719816);
	static_assert(offsetof(mm8::HouseRules, spellbookShops) == 480720084);
	static_assert(offsetof(mm8::HouseRules, training) == 480720098);
	static_assert(offsetof(mm8::HouseRules, arcomage) == 480720156);
	static_assert(offsetof(mm8::HouseRules, arcomageTexts) == 480721428);



	struct CharacterVoices // size: 0x45A16D2
	{
		SKIP(72996672);
		std::array<std::array<uint16_t, 100>, 79> sounds; // 0x459D740 (72996672 decimal)
		std::array<std::array<uint8_t, 6>, 79> avail; // 0x45A14F8 (73012472 decimal)
	};
	static_assert(sizeof(mm8::CharacterVoices) == 0x45A16D2, "Invalid \"mm8::CharacterVoices\" structure size");
	static_assert(offsetof(mm8::CharacterVoices, sounds) == 72996672);
	static_assert(offsetof(mm8::CharacterVoices, avail) == 73012472);



	struct CharacterPortrait // size: 0x44
	{
		uint8_t dollType; // 0x0 (0 decimal)
		uint8_t defClass; // 0x1 (1 decimal)
		uint8_t defVoice; // 0x2 (2 decimal)
		uint8_t defSex; // 0x3 (3 decimal)
		uint8_t availableAtStart; // 0x4 (4 decimal)
		int16_t bodyX; // 0x5 (5 decimal)
		int16_t bodyY; // 0x7 (7 decimal)
		char* background; // EditPChar | 0x9 (9 decimal)
		char* body; // EditPChar | 0xD (13 decimal)
		char* head; // EditPChar | 0x11 (17 decimal)
		char* LHd; // EditPChar | 0x15 (21 decimal)
		char* LHu; // EditPChar | 0x19 (25 decimal)
		char* LHo; // EditPChar | 0x1D (29 decimal)
		char* RHb; // EditPChar | 0x21 (33 decimal)
		char* unk1; // EditPChar | 0x25 (37 decimal)
		char* unk2; // EditPChar | 0x29 (41 decimal)
		char* RHd; // EditPChar | 0x2D (45 decimal)
		char* RHu; // EditPChar | 0x31 (49 decimal)
		char* facePrefix; // EditPChar | 0x35 (53 decimal)
		uint16_t defAttackM; // 0x39 (57 decimal)
		uint16_t defAttackR; // 0x3B (59 decimal)
		uint16_t NPCPic; // 0x3D (61 decimal)
		uint8_t race; // 0x3F (63 decimal)
		int16_t helmX; // 0x40 (64 decimal)
		int16_t helmY; // 0x42 (66 decimal)
	};
	static_assert(sizeof(mm8::CharacterPortrait) == 0x44, "Invalid \"mm8::CharacterPortrait\" structure size");
	static_assert(offsetof(mm8::CharacterPortrait, defClass) == 1);
	static_assert(offsetof(mm8::CharacterPortrait, defVoice) == 2);
	static_assert(offsetof(mm8::CharacterPortrait, defSex) == 3);
	static_assert(offsetof(mm8::CharacterPortrait, availableAtStart) == 4);
	static_assert(offsetof(mm8::CharacterPortrait, bodyX) == 5);
	static_assert(offsetof(mm8::CharacterPortrait, bodyY) == 7);
	static_assert(offsetof(mm8::CharacterPortrait, background) == 9);
	static_assert(offsetof(mm8::CharacterPortrait, body) == 13);
	static_assert(offsetof(mm8::CharacterPortrait, head) == 17);
	static_assert(offsetof(mm8::CharacterPortrait, LHd) == 21);
	static_assert(offsetof(mm8::CharacterPortrait, LHu) == 25);
	static_assert(offsetof(mm8::CharacterPortrait, LHo) == 29);
	static_assert(offsetof(mm8::CharacterPortrait, RHb) == 33);
	static_assert(offsetof(mm8::CharacterPortrait, unk1) == 37);
	static_assert(offsetof(mm8::CharacterPortrait, unk2) == 41);
	static_assert(offsetof(mm8::CharacterPortrait, RHd) == 45);
	static_assert(offsetof(mm8::CharacterPortrait, RHu) == 49);
	static_assert(offsetof(mm8::CharacterPortrait, facePrefix) == 53);
	static_assert(offsetof(mm8::CharacterPortrait, defAttackM) == 57);
	static_assert(offsetof(mm8::CharacterPortrait, defAttackR) == 59);
	static_assert(offsetof(mm8::CharacterPortrait, NPCPic) == 61);
	static_assert(offsetof(mm8::CharacterPortrait, race) == 63);
	static_assert(offsetof(mm8::CharacterPortrait, helmX) == 64);
	static_assert(offsetof(mm8::CharacterPortrait, helmY) == 66);



	struct CharacterDollType // size: 0x2F
	{
		bool bow; // 0x0 (0 decimal)
		bool armor; // 0x1 (1 decimal)
		bool helm; // 0x2 (2 decimal)
		bool belt; // 0x3 (3 decimal)
		bool boots; // 0x4 (4 decimal)
		bool cloak; // 0x5 (5 decimal)
		bool weapon; // 0x6 (6 decimal)
		int16_t RHoX; // 0x7 (7 decimal)
		int16_t RHoY; // 0x9 (9 decimal)
		int16_t RHcX; // 0xB (11 decimal)
		int16_t RHcY; // 0xD (13 decimal)
		int16_t RHfX; // 0xF (15 decimal)
		int16_t RHfY; // 0x11 (17 decimal)
		int16_t LHoX; // 0x13 (19 decimal)
		int16_t LHoY; // 0x15 (21 decimal)
		int16_t LHcX; // 0x17 (23 decimal)
		int16_t LHcY; // 0x19 (25 decimal)
		int16_t LHfX; // 0x1B (27 decimal)
		int16_t LHfY; // 0x1D (29 decimal)
		int16_t OHOffsetX; // 0x1F (31 decimal)
		int16_t OHOffsetY; // 0x21 (33 decimal)
		int16_t MHOffsetX; // 0x23 (35 decimal)
		int16_t MHOffsetY; // 0x25 (37 decimal)
		int16_t bowX; // 0x27 (39 decimal)
		int16_t bowY; // 0x29 (41 decimal)
		int16_t shieldX; // 0x2B (43 decimal)
		int16_t shieldY; // 0x2D (45 decimal)
	};
	static_assert(sizeof(mm8::CharacterDollType) == 0x2F, "Invalid \"mm8::CharacterDollType\" structure size");
	static_assert(offsetof(mm8::CharacterDollType, armor) == 1);
	static_assert(offsetof(mm8::CharacterDollType, helm) == 2);
	static_assert(offsetof(mm8::CharacterDollType, belt) == 3);
	static_assert(offsetof(mm8::CharacterDollType, boots) == 4);
	static_assert(offsetof(mm8::CharacterDollType, cloak) == 5);
	static_assert(offsetof(mm8::CharacterDollType, weapon) == 6);
	static_assert(offsetof(mm8::CharacterDollType, RHoX) == 7);
	static_assert(offsetof(mm8::CharacterDollType, RHoY) == 9);
	static_assert(offsetof(mm8::CharacterDollType, RHcX) == 11);
	static_assert(offsetof(mm8::CharacterDollType, RHcY) == 13);
	static_assert(offsetof(mm8::CharacterDollType, RHfX) == 15);
	static_assert(offsetof(mm8::CharacterDollType, RHfY) == 17);
	static_assert(offsetof(mm8::CharacterDollType, LHoX) == 19);
	static_assert(offsetof(mm8::CharacterDollType, LHoY) == 21);
	static_assert(offsetof(mm8::CharacterDollType, LHcX) == 23);
	static_assert(offsetof(mm8::CharacterDollType, LHcY) == 25);
	static_assert(offsetof(mm8::CharacterDollType, LHfX) == 27);
	static_assert(offsetof(mm8::CharacterDollType, LHfY) == 29);
	static_assert(offsetof(mm8::CharacterDollType, OHOffsetX) == 31);
	static_assert(offsetof(mm8::CharacterDollType, OHOffsetY) == 33);
	static_assert(offsetof(mm8::CharacterDollType, MHOffsetX) == 35);
	static_assert(offsetof(mm8::CharacterDollType, MHOffsetY) == 37);
	static_assert(offsetof(mm8::CharacterDollType, bowX) == 39);
	static_assert(offsetof(mm8::CharacterDollType, bowY) == 41);
	static_assert(offsetof(mm8::CharacterDollType, shieldX) == 43);
	static_assert(offsetof(mm8::CharacterDollType, shieldY) == 45);



	struct EquipCoordsCloak // size: 0x8
	{
		int16_t x; // 0x0 (0 decimal)
		int16_t y; // 0x2 (2 decimal)
		int16_t colX; // 0x4 (4 decimal)
		int16_t colY; // 0x6 (6 decimal)
	};
	static_assert(sizeof(mm8::EquipCoordsCloak) == 0x8, "Invalid \"mm8::EquipCoordsCloak\" structure size");
	static_assert(offsetof(mm8::EquipCoordsCloak, y) == 2);
	static_assert(offsetof(mm8::EquipCoordsCloak, colX) == 4);
	static_assert(offsetof(mm8::EquipCoordsCloak, colY) == 6);



	struct EquipCoordinates // size: 0x4
	{
		int16_t x; // 0x0 (0 decimal)
		int16_t y; // 0x2 (2 decimal)
	};
	static_assert(sizeof(mm8::EquipCoordinates) == 0x4, "Invalid \"mm8::EquipCoordinates\" structure size");
	static_assert(offsetof(mm8::EquipCoordinates, y) == 2);



	struct ArmorPicsCoords // size: 0x45EAF48
	{
		SKIP(73309776);
		std::array<std::array<mm8::EquipCoordinates, 6>, 54> armors; // MMExt: 1..54, here 0..53 | 0x45E9E50 (73309776 decimal)
		std::array<std::array<mm8::EquipCoordinates, 6>, 40> helms; // MMExt: 1..40, here 0..39 | 0x45EA360 (73311072 decimal)
		std::array<std::array<mm8::EquipCoordinates, 6>, 18> belts; // MMExt: 1..18, here 0..17 | 0x45EA720 (73312032 decimal)
		std::array<std::array<mm8::EquipCoordinates, 6>, 21> boots; // MMExt: 1..21, here 0..20 | 0x45EA8D0 (73312464 decimal)
		std::array<std::array<mm8::EquipCoordsCloak, 6>, 24> cloaks; // MMExt: 1..24, here 0..23 | 0x45EAAC8 (73312968 decimal)
	};
	static_assert(sizeof(mm8::ArmorPicsCoords) == 0x45EAF48, "Invalid \"mm8::ArmorPicsCoords\" structure size");
	static_assert(offsetof(mm8::ArmorPicsCoords, armors) == 73309776);
	static_assert(offsetof(mm8::ArmorPicsCoords, helms) == 73311072);
	static_assert(offsetof(mm8::ArmorPicsCoords, belts) == 73312032);
	static_assert(offsetof(mm8::ArmorPicsCoords, boots) == 73312464);
	static_assert(offsetof(mm8::ArmorPicsCoords, cloaks) == 73312968);
}

#pragma pack(pop)