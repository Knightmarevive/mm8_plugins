#pragma once
#include "pch.h"
#include "main.h"
#include "Common.h"

#pragma pack(push, 1)

namespace mm8
{
	struct Weather // size: 0x6F2FBC
	{
		SKIP(7286676);
		uint32_t rain; // 4-byte boolean | 0x6F2F94 (7286676 decimal)
		int32_t shade; // 0x6F2F98 (7286680 decimal)
		SKIP(8);
		// uint8_t* fog; // real size is 0 | Unknown type | 0x6F2FA4 (7286692 decimal)
		SKIP(16);
		std::array<uint8_t, 4> fogRange1; // Unknown type | 0x6F2FB4 (7286708 decimal)
		std::array<uint8_t, 4> fogRange2; // Unknown type | 0x6F2FB8 (7286712 decimal)
	};
	static_assert(sizeof(mm8::Weather) == 0x6F2FBC, "Invalid \"mm8::Weather\" structure size");
	static_assert(offsetof(mm8::Weather, rain) == 7286676);
	static_assert(offsetof(mm8::Weather, shade) == 7286680);
	static_assert(offsetof(mm8::Weather, fogRange1) == 7286708);
	static_assert(offsetof(mm8::Weather, fogRange2) == 7286712);



	struct TravelInfo // size: 0x20
	{
		union
		{
			std::array<uint8_t, 1> map; // Unknown type | 0x0 (0 decimal)
			uint8_t mapIndex; // 0x0 (0 decimal)
		};
		union
		{
			std::array<uint8_t, 7> daysAvailable; // 0x1 (1 decimal)
			struct
			{
				bool monday; // 0x1 (1 decimal)
				bool tuesday; // 0x2 (2 decimal)
				bool wednesday; // 0x3 (3 decimal)
				bool thursday; // 0x4 (4 decimal)
				bool friday; // 0x5 (5 decimal)
				bool saturday; // 0x6 (6 decimal)
				bool sunday; // 0x7 (7 decimal)
			};
		};
		uint8_t daysCount; // 0x8 (8 decimal)
		SKIP(3);
		union
		{
			std::array<int32_t, 3> pos; // 0xC (12 decimal)
			struct
			{
				int32_t x; // 0xC (12 decimal)
				int32_t y; // 0x10 (16 decimal)
				int32_t z; // 0x14 (20 decimal)
			};
		};
		int32_t direction; // 0x18 (24 decimal)
		int32_t QBit; // 0x1C (28 decimal)
	};
	static_assert(sizeof(mm8::TravelInfo) == 0x20, "Invalid \"mm8::TravelInfo\" structure size");
	static_assert(offsetof(mm8::TravelInfo, daysAvailable) == 1);
	static_assert(offsetof(mm8::TravelInfo, monday) == 1);
	static_assert(offsetof(mm8::TravelInfo, tuesday) == 2);
	static_assert(offsetof(mm8::TravelInfo, wednesday) == 3);
	static_assert(offsetof(mm8::TravelInfo, thursday) == 4);
	static_assert(offsetof(mm8::TravelInfo, friday) == 5);
	static_assert(offsetof(mm8::TravelInfo, saturday) == 6);
	static_assert(offsetof(mm8::TravelInfo, sunday) == 7);
	static_assert(offsetof(mm8::TravelInfo, daysCount) == 8);
	static_assert(offsetof(mm8::TravelInfo, pos) == 12);
	static_assert(offsetof(mm8::TravelInfo, x) == 12);
	static_assert(offsetof(mm8::TravelInfo, y) == 16);
	static_assert(offsetof(mm8::TravelInfo, z) == 20);
	static_assert(offsetof(mm8::TravelInfo, direction) == 24);
	static_assert(offsetof(mm8::TravelInfo, QBit) == 28);



	struct TownPortalTownInfo // size: 0x14
	{
		union
		{
			std::array<int32_t, 3> pos; // 0x0 (0 decimal)
			struct
			{
				int32_t x; // 0x0 (0 decimal)
				int32_t y; // 0x4 (4 decimal)
				int32_t z; // 0x8 (8 decimal)
			};
		};
		int16_t direction; // 0xC (12 decimal)
		int16_t lookAngle; // 0xE (14 decimal)
		union
		{
			std::array<uint8_t, 2> map; // Unknown type | 0x10 (16 decimal)
			int16_t mapStatsIndex; // 0x10 (16 decimal)
		};
		SKIP(2);
	};
	static_assert(sizeof(mm8::TownPortalTownInfo) == 0x14, "Invalid \"mm8::TownPortalTownInfo\" structure size");
	static_assert(offsetof(mm8::TownPortalTownInfo, y) == 4);
	static_assert(offsetof(mm8::TownPortalTownInfo, z) == 8);
	static_assert(offsetof(mm8::TownPortalTownInfo, direction) == 12);
	static_assert(offsetof(mm8::TownPortalTownInfo, lookAngle) == 14);
	static_assert(offsetof(mm8::TownPortalTownInfo, map) == 16);
	static_assert(offsetof(mm8::TownPortalTownInfo, mapStatsIndex) == 16);



	struct SpellInfo // size: 0x14
	{
		union
		{
			std::array<int16_t, 4> spellPoints; // MMExt: 1..4, here 0..3 | 0x0 (0 decimal)
			struct
			{
				int16_t spellPointsNormal; // 0x0 (0 decimal)
				int16_t spellPointsExpert; // 0x2 (2 decimal)
				int16_t spellPointsMaster; // 0x4 (4 decimal)
				int16_t spellPointsGM; // 0x6 (6 decimal)
			};
		};
		union
		{
			std::array<int16_t, 4> delay; // MMExt: 1..4, here 0..3 | 0x8 (8 decimal)
			struct
			{
				int16_t delayNormal; // 0x8 (8 decimal)
				int16_t delayExpert; // 0xA (10 decimal)
				int16_t delayMaster; // 0xC (12 decimal)
				int16_t delayGM; // 0xE (14 decimal)
			};
		};
		uint8_t damageAdd; // 0x10 (16 decimal)
		uint8_t damageDiceSides; // 0x11 (17 decimal)
		union
		{
			uint16_t bits; // 0x12 (18 decimal)
			struct
			{
				bool castByMonster : 1; // 0x12 (18 decimal), bit index 7
				bool castByEvent : 1; // 0x12 (18 decimal), bit index 6
				bool causeDamage : 1; // 0x12 (18 decimal), bit index 5
				bool specialDamage : 1; // 0x12 (18 decimal), bit index 4
			};
		};
	};
	static_assert(sizeof(mm8::SpellInfo) == 0x14, "Invalid \"mm8::SpellInfo\" structure size");
	static_assert(offsetof(mm8::SpellInfo, spellPointsExpert) == 2);
	static_assert(offsetof(mm8::SpellInfo, spellPointsMaster) == 4);
	static_assert(offsetof(mm8::SpellInfo, spellPointsGM) == 6);
	static_assert(offsetof(mm8::SpellInfo, delay) == 8);
	static_assert(offsetof(mm8::SpellInfo, delayNormal) == 8);
	static_assert(offsetof(mm8::SpellInfo, delayExpert) == 10);
	static_assert(offsetof(mm8::SpellInfo, delayMaster) == 12);
	static_assert(offsetof(mm8::SpellInfo, delayGM) == 14);
	static_assert(offsetof(mm8::SpellInfo, damageAdd) == 16);
	static_assert(offsetof(mm8::SpellInfo, damageDiceSides) == 17);
	static_assert(offsetof(mm8::SpellInfo, bits) == 18);



	struct ShopItemKind // size: 0xA
	{
		int16_t level; // 0x0 (0 decimal)
		std::array<int16_t, 4> types; // MMExt: 1..4, here 0..3 | 0x2 (2 decimal)
	};
	static_assert(sizeof(mm8::ShopItemKind) == 0xA, "Invalid \"mm8::ShopItemKind\" structure size");
	static_assert(offsetof(mm8::ShopItemKind, types) == 2);



	struct ProgressBar // size: 0x10
	{
		SKIP(10);
		uint8_t max; // 0xA (10 decimal)
		uint8_t current; // 0xB (11 decimal)
		int32_t kind; // 0xC (12 decimal)
	};
	static_assert(sizeof(mm8::ProgressBar) == 0x10, "Invalid \"mm8::ProgressBar\" structure size");
	static_assert(offsetof(mm8::ProgressBar, max) == 10);
	static_assert(offsetof(mm8::ProgressBar, current) == 11);
	static_assert(offsetof(mm8::ProgressBar, kind) == 12);



	struct PatchOptions // size: 0x178
	{
		int32_t size; // 0x0 (0 decimal)
		int32_t maxMLookAngle; // 0x4 (4 decimal)
		uint32_t mouseLook; // 4-byte boolean | 0x8 (8 decimal)
		uint32_t mouseLookUseAltMode; // 4-byte boolean | 0xC (12 decimal)
		uint32_t capsLockToggleMouseLook; // 4-byte boolean | 0x10 (16 decimal)
		uint32_t mouseFly; // 4-byte boolean | 0x14 (20 decimal)
		uint32_t mouseWheelFly; // 4-byte boolean | 0x18 (24 decimal)
		int32_t mouseLookTempKey; // 0x1C (28 decimal)
		int32_t mouseLookChangeKey; // 0x20 (32 decimal)
		int32_t inventoryKey; // 0x24 (36 decimal)
		int32_t charScreenKey; // 0x28 (40 decimal)
		int32_t doubleSpeedKey; // 0x2C (44 decimal)
		int32_t quickLoadKey; // 0x30 (48 decimal)
		int32_t autorunKey; // 0x34 (52 decimal)
		uint32_t HDWTRCount; // 0x38 (56 decimal)
		uint32_t HDWTRDelay; // 0x3C (60 decimal)
		int32_t horsemanSpeakTime; // 0x40 (64 decimal)
		int32_t boatmanSpeakTime; // 0x44 (68 decimal)
		float paletteSMul; // 0x48 (72 decimal)
		float paletteVMul; // 0x4C (76 decimal)
		uint32_t noBitmapsHwl; // 4-byte boolean | 0x50 (80 decimal)
		uint32_t playMP3; // 4-byte boolean | 0x54 (84 decimal)
		int32_t musicLoopsCount; // 0x58 (88 decimal)
		uint32_t hardenArtifacts; // 4-byte boolean | 0x5C (92 decimal)
		uint32_t progressiveDaggerTrippleDamage; // 4-byte boolean | 0x60 (96 decimal)
		uint32_t fixChests; // 4-byte boolean | 0x64 (100 decimal)
		uint32_t dataFiles; // 4-byte boolean | 0x68 (104 decimal)
		uint32_t fixDualWeaponsRecovery; // 4-byte boolean | 0x6C (108 decimal)
		int32_t increaseRecoveryRateStrength; // 0x70 (112 decimal)
		int32_t blasterRecovery; // 0x74 (116 decimal)
		uint32_t fixSkyBitmap; // 4-byte boolean | 0x78 (120 decimal)
		uint32_t noCD; // 4-byte boolean | 0x7C (124 decimal)
		uint32_t fixChestsByReorder; // 4-byte boolean | 0x80 (128 decimal)
		int32_t lastLoadedFileSize; // 0x84 (132 decimal)
		uint32_t fixTimers; // 4-byte boolean | 0x88 (136 decimal)
		uint32_t fixMovement; // 4-byte boolean | 0x8C (140 decimal)
		int32_t monsterJumpDownLimit; // 0x90 (144 decimal)
		uint32_t fixHeroismPedestal; // 4-byte boolean | 0x94 (148 decimal)
		uint32_t skipUnsellableItemCheck; // 4-byte boolean | 0x98 (152 decimal)
		uint32_t fixGMStaff; // 4-byte boolean | 0x9C (156 decimal)
		uint32_t fixObelisks; // 4-byte boolean | 0xA0 (160 decimal)
		uint32_t borderlessWindowed; // 4-byte boolean | 0xA4 (164 decimal)
		uint32_t compatibleMovieRender; // 4-byte boolean | 0xA8 (168 decimal)
		uint32_t smoothMovieScaling; // 4-byte boolean | 0xAC (172 decimal)
		uint32_t supportTrueColor; // 4-byte boolean | 0xB0 (176 decimal)
		int32_t renderRectLeft; // 0xB4 (180 decimal)
		int32_t renderRectTop; // 0xB8 (184 decimal)
		int32_t renderRectRight; // 0xBC (188 decimal)
		int32_t renderRectBottom; // 0xC0 (192 decimal)
		uint32_t fixUnimplementedSpells; // 4-byte boolean | 0xC4 (196 decimal)
		int32_t indoorMinimapZoomMul; // 0xC8 (200 decimal)
		int32_t indoorMinimapZoomPower; // 0xCC (204 decimal)
		uint32_t fixMonsterSummon; // 4-byte boolean | 0xD0 (208 decimal)
		uint32_t fixInterfaceBugs; // 4-byte boolean | 0xD4 (212 decimal)
		const char* UILayout; // PChar (read-only) | 0xD8 (216 decimal)
		int32_t paperDollInChests; // 0xDC (220 decimal)
		uint32_t higherCloseRingsButton; // 4-byte boolean | 0xE0 (224 decimal)
		int32_t renderBottomPixel; // 0xE4 (228 decimal)
		uint32_t trueColorTextures; // 4-byte boolean | 0xE8 (232 decimal)
		uint32_t resetPalettes; // 4-byte boolean | 0xEC (236 decimal)
		uint32_t fixSFT; // 4-byte boolean | 0xF0 (240 decimal)
		uint32_t axeGMFullProbabilityAt; // 4-byte boolean | 0xF4 (244 decimal)
		double mouseDX; // 0xF8 (248 decimal)
		double mouseDY; // 0x100 (256 decimal)
		uint32_t trueColorSprites; // 4-byte boolean | 0x108 (264 decimal)
		uint32_t fixMonstersBlockingShots; // 4-byte boolean | 0x10C (268 decimal)
		uint32_t fixParalyze; // 4-byte boolean | 0x110 (272 decimal)
		uint32_t enableAttackSpell; // 4-byte boolean | 0x114 (276 decimal)
		int32_t shooterMode; // 0x118 (280 decimal)
		int32_t maxMLookUpAngle; // 0x11C (284 decimal)
		uint32_t fixIceBoltBlast; // 4-byte boolean | 0x120 (288 decimal)
		int32_t monSpritesSizeMul; // 0x124 (292 decimal)
		uint32_t fixMonsterAttackTypes; // 4-byte boolean | 0x128 (296 decimal)
		uint32_t fixMonsterSpells; // 4-byte boolean | 0x12C (300 decimal)
		uint32_t fixSouldrinker; // 4-byte boolean | 0x130 (304 decimal)
		int32_t mouseLookPermKey; // 0x134 (308 decimal)
		int32_t lastSoundSample; // 0x138 (312 decimal)
		int32_t waterWalkDamage; // 0x13C (316 decimal)
		uint32_t fixUnmarkedArtifacts; // 4-byte boolean | 0x140 (320 decimal)
		uint32_t fixClubsDelay; // 4-byte boolean | 0x144 (324 decimal)
		uint32_t fixDarkTrainers; // 4-byte boolean | 0x148 (328 decimal)
		uint32_t fixLightBolt; // 4-byte boolean | 0x14C (332 decimal)
		int32_t armageddonElement; // 0x150 (336 decimal)
		uint32_t fixKelebrim; // 4-byte boolean | 0x154 (340 decimal)
		uint32_t fixBarrels; // 4-byte boolean | 0x158 (344 decimal)
		uint32_t climbBetter; // 4-byte boolean | 0x15C (348 decimal)
		uint32_t fixWaterWalkManaDrain; // 4-byte boolean | 0x160 (352 decimal)
		uint32_t keepEmptyWands; // 4-byte boolean | 0x164 (356 decimal)
		uint32_t dontSkipSimpleMessage; // 4-byte boolean | 0x168 (360 decimal)
		uint32_t fixItemDuplicates; // 4-byte boolean | 0x16C (364 decimal)
		uint32_t fixClubsGeneration; // 4-byte boolean | 0x170 (368 decimal)
		uint32_t fixAcidBurst; // 4-byte boolean | 0x174 (372 decimal)
	};
	static_assert(sizeof(mm8::PatchOptions) == 0x178, "Invalid \"mm8::PatchOptions\" structure size");
	static_assert(offsetof(mm8::PatchOptions, maxMLookAngle) == 4);
	static_assert(offsetof(mm8::PatchOptions, mouseLook) == 8);
	static_assert(offsetof(mm8::PatchOptions, mouseLookUseAltMode) == 12);
	static_assert(offsetof(mm8::PatchOptions, capsLockToggleMouseLook) == 16);
	static_assert(offsetof(mm8::PatchOptions, mouseFly) == 20);
	static_assert(offsetof(mm8::PatchOptions, mouseWheelFly) == 24);
	static_assert(offsetof(mm8::PatchOptions, mouseLookTempKey) == 28);
	static_assert(offsetof(mm8::PatchOptions, mouseLookChangeKey) == 32);
	static_assert(offsetof(mm8::PatchOptions, inventoryKey) == 36);
	static_assert(offsetof(mm8::PatchOptions, charScreenKey) == 40);
	static_assert(offsetof(mm8::PatchOptions, doubleSpeedKey) == 44);
	static_assert(offsetof(mm8::PatchOptions, quickLoadKey) == 48);
	static_assert(offsetof(mm8::PatchOptions, autorunKey) == 52);
	static_assert(offsetof(mm8::PatchOptions, HDWTRCount) == 56);
	static_assert(offsetof(mm8::PatchOptions, HDWTRDelay) == 60);
	static_assert(offsetof(mm8::PatchOptions, horsemanSpeakTime) == 64);
	static_assert(offsetof(mm8::PatchOptions, boatmanSpeakTime) == 68);
	static_assert(offsetof(mm8::PatchOptions, paletteSMul) == 72);
	static_assert(offsetof(mm8::PatchOptions, paletteVMul) == 76);
	static_assert(offsetof(mm8::PatchOptions, noBitmapsHwl) == 80);
	static_assert(offsetof(mm8::PatchOptions, playMP3) == 84);
	static_assert(offsetof(mm8::PatchOptions, musicLoopsCount) == 88);
	static_assert(offsetof(mm8::PatchOptions, hardenArtifacts) == 92);
	static_assert(offsetof(mm8::PatchOptions, progressiveDaggerTrippleDamage) == 96);
	static_assert(offsetof(mm8::PatchOptions, fixChests) == 100);
	static_assert(offsetof(mm8::PatchOptions, dataFiles) == 104);
	static_assert(offsetof(mm8::PatchOptions, fixDualWeaponsRecovery) == 108);
	static_assert(offsetof(mm8::PatchOptions, increaseRecoveryRateStrength) == 112);
	static_assert(offsetof(mm8::PatchOptions, blasterRecovery) == 116);
	static_assert(offsetof(mm8::PatchOptions, fixSkyBitmap) == 120);
	static_assert(offsetof(mm8::PatchOptions, noCD) == 124);
	static_assert(offsetof(mm8::PatchOptions, fixChestsByReorder) == 128);
	static_assert(offsetof(mm8::PatchOptions, lastLoadedFileSize) == 132);
	static_assert(offsetof(mm8::PatchOptions, fixTimers) == 136);
	static_assert(offsetof(mm8::PatchOptions, fixMovement) == 140);
	static_assert(offsetof(mm8::PatchOptions, monsterJumpDownLimit) == 144);
	static_assert(offsetof(mm8::PatchOptions, fixHeroismPedestal) == 148);
	static_assert(offsetof(mm8::PatchOptions, skipUnsellableItemCheck) == 152);
	static_assert(offsetof(mm8::PatchOptions, fixGMStaff) == 156);
	static_assert(offsetof(mm8::PatchOptions, fixObelisks) == 160);
	static_assert(offsetof(mm8::PatchOptions, borderlessWindowed) == 164);
	static_assert(offsetof(mm8::PatchOptions, compatibleMovieRender) == 168);
	static_assert(offsetof(mm8::PatchOptions, smoothMovieScaling) == 172);
	static_assert(offsetof(mm8::PatchOptions, supportTrueColor) == 176);
	static_assert(offsetof(mm8::PatchOptions, renderRectLeft) == 180);
	static_assert(offsetof(mm8::PatchOptions, renderRectTop) == 184);
	static_assert(offsetof(mm8::PatchOptions, renderRectRight) == 188);
	static_assert(offsetof(mm8::PatchOptions, renderRectBottom) == 192);
	static_assert(offsetof(mm8::PatchOptions, fixUnimplementedSpells) == 196);
	static_assert(offsetof(mm8::PatchOptions, indoorMinimapZoomMul) == 200);
	static_assert(offsetof(mm8::PatchOptions, indoorMinimapZoomPower) == 204);
	static_assert(offsetof(mm8::PatchOptions, fixMonsterSummon) == 208);
	static_assert(offsetof(mm8::PatchOptions, fixInterfaceBugs) == 212);
	static_assert(offsetof(mm8::PatchOptions, UILayout) == 216);
	static_assert(offsetof(mm8::PatchOptions, paperDollInChests) == 220);
	static_assert(offsetof(mm8::PatchOptions, higherCloseRingsButton) == 224);
	static_assert(offsetof(mm8::PatchOptions, renderBottomPixel) == 228);
	static_assert(offsetof(mm8::PatchOptions, trueColorTextures) == 232);
	static_assert(offsetof(mm8::PatchOptions, resetPalettes) == 236);
	static_assert(offsetof(mm8::PatchOptions, fixSFT) == 240);
	static_assert(offsetof(mm8::PatchOptions, axeGMFullProbabilityAt) == 244);
	static_assert(offsetof(mm8::PatchOptions, mouseDX) == 248);
	static_assert(offsetof(mm8::PatchOptions, mouseDY) == 256);
	static_assert(offsetof(mm8::PatchOptions, trueColorSprites) == 264);
	static_assert(offsetof(mm8::PatchOptions, fixMonstersBlockingShots) == 268);
	static_assert(offsetof(mm8::PatchOptions, fixParalyze) == 272);
	static_assert(offsetof(mm8::PatchOptions, enableAttackSpell) == 276);
	static_assert(offsetof(mm8::PatchOptions, shooterMode) == 280);
	static_assert(offsetof(mm8::PatchOptions, maxMLookUpAngle) == 284);
	static_assert(offsetof(mm8::PatchOptions, fixIceBoltBlast) == 288);
	static_assert(offsetof(mm8::PatchOptions, monSpritesSizeMul) == 292);
	static_assert(offsetof(mm8::PatchOptions, fixMonsterAttackTypes) == 296);
	static_assert(offsetof(mm8::PatchOptions, fixMonsterSpells) == 300);
	static_assert(offsetof(mm8::PatchOptions, fixSouldrinker) == 304);
	static_assert(offsetof(mm8::PatchOptions, mouseLookPermKey) == 308);
	static_assert(offsetof(mm8::PatchOptions, lastSoundSample) == 312);
	static_assert(offsetof(mm8::PatchOptions, waterWalkDamage) == 316);
	static_assert(offsetof(mm8::PatchOptions, fixUnmarkedArtifacts) == 320);
	static_assert(offsetof(mm8::PatchOptions, fixClubsDelay) == 324);
	static_assert(offsetof(mm8::PatchOptions, fixDarkTrainers) == 328);
	static_assert(offsetof(mm8::PatchOptions, fixLightBolt) == 332);
	static_assert(offsetof(mm8::PatchOptions, armageddonElement) == 336);
	static_assert(offsetof(mm8::PatchOptions, fixKelebrim) == 340);
	static_assert(offsetof(mm8::PatchOptions, fixBarrels) == 344);
	static_assert(offsetof(mm8::PatchOptions, climbBetter) == 348);
	static_assert(offsetof(mm8::PatchOptions, fixWaterWalkManaDrain) == 352);
	static_assert(offsetof(mm8::PatchOptions, keepEmptyWands) == 356);
	static_assert(offsetof(mm8::PatchOptions, dontSkipSimpleMessage) == 360);
	static_assert(offsetof(mm8::PatchOptions, fixItemDuplicates) == 364);
	static_assert(offsetof(mm8::PatchOptions, fixClubsGeneration) == 368);
	static_assert(offsetof(mm8::PatchOptions, fixAcidBurst) == 372);



	struct MoveToMap // size: 0x1C
	{
		union
		{
			std::array<int32_t, 3> pos; // 0x0 (0 decimal)
			struct
			{
				int32_t x; // 0x0 (0 decimal)
				int32_t y; // 0x4 (4 decimal)
				int32_t z; // 0x8 (8 decimal)
			};
		};
		int32_t direction; // 0xC (12 decimal)
		int32_t lookAngle; // 0x10 (16 decimal)
		int32_t speedZ; // 0x14 (20 decimal)
		uint32_t defined; // 4-byte boolean | 0x18 (24 decimal)
	};
	static_assert(sizeof(mm8::MoveToMap) == 0x1C, "Invalid \"mm8::MoveToMap\" structure size");
	static_assert(offsetof(mm8::MoveToMap, y) == 4);
	static_assert(offsetof(mm8::MoveToMap, z) == 8);
	static_assert(offsetof(mm8::MoveToMap, direction) == 12);
	static_assert(offsetof(mm8::MoveToMap, lookAngle) == 16);
	static_assert(offsetof(mm8::MoveToMap, speedZ) == 20);
	static_assert(offsetof(mm8::MoveToMap, defined) == 24);



	struct MouseStruct // size: 0x110
	{
		static inline mm8::ObjectRef* const target = 0;

		SKIP(264);
		int32_t x; // 0x108 (264 decimal)
		int32_t y; // 0x10C (268 decimal)
	};
	static_assert(sizeof(mm8::MouseStruct) == 0x110, "Invalid \"mm8::MouseStruct\" structure size");
	static_assert(offsetof(mm8::MouseStruct, x) == 264);
	static_assert(offsetof(mm8::MouseStruct, y) == 268);



	struct MissileSetup // size: 0x1
	{
		bool alwaysShowSprite : 1; // 0x0 (0 decimal)
		bool hideSpecialDisplay : 1; // 0x0 (0 decimal)
		bool autoCollision : 1; // 0x0 (0 decimal)
	};
	static_assert(sizeof(mm8::MissileSetup) == 0x1, "Invalid \"mm8::MissileSetup\" structure size");



	struct FogChances // size: 0x4
	{
		uint8_t thick; // 0x0 (0 decimal)
		uint8_t medium; // 0x1 (1 decimal)
		uint8_t light; // 0x2 (2 decimal)
		SKIP(1);
	};
	static_assert(sizeof(mm8::FogChances) == 0x4, "Invalid \"mm8::FogChances\" structure size");
	static_assert(offsetof(mm8::FogChances, medium) == 1);
	static_assert(offsetof(mm8::FogChances, light) == 2);



	struct HouseMovie // size: 0x10
	{
		char* fileName; // EditPChar | 0x0 (0 decimal)
		uint8_t background; // 0x4 (4 decimal)
		SKIP(3);
		int32_t NPCPic; // 0x8 (8 decimal)
		uint8_t houseType; // 0xC (12 decimal)
		uint8_t sounds; // 0xD (13 decimal)
		SKIP(2);
	};
	static_assert(sizeof(mm8::HouseMovie) == 0x10, "Invalid \"mm8::HouseMovie\" structure size");
	static_assert(offsetof(mm8::HouseMovie, background) == 4);
	static_assert(offsetof(mm8::HouseMovie, NPCPic) == 8);
	static_assert(offsetof(mm8::HouseMovie, houseType) == 12);
	static_assert(offsetof(mm8::HouseMovie, sounds) == 13);



	struct EventLine // size: 0xC
	{
		int32_t event; // 0x0 (0 decimal)
		int32_t line; // 0x4 (4 decimal)
		int32_t offset; // 0x8 (8 decimal)
	};
	static_assert(sizeof(mm8::EventLine) == 0xC, "Invalid \"mm8::EventLine\" structure size");
	static_assert(offsetof(mm8::EventLine, line) == 4);
	static_assert(offsetof(mm8::EventLine, offset) == 8);



	struct GameMouse // size: 0xB7CA88
	{
		SKIP(7722864);
		mm8::MouseStruct* mouseStruct; // 0x75D770 (7722864 decimal)
		SKIP(4322032);
		mm8::Item item; // 0xB7CA64 (12044900 decimal)
	};
	static_assert(sizeof(mm8::GameMouse) == 0xB7CA88, "Invalid \"mm8::GameMouse\" structure size");
	static_assert(offsetof(mm8::GameMouse, mouseStruct) == 7722864);
	static_assert(offsetof(mm8::GameMouse, item) == 12044900);



	struct ActionItem // size: 0xC
	{
		int32_t action; // 0x0 (0 decimal)
		int32_t param; // 0x4 (4 decimal)
		int32_t param2; // 0x8 (8 decimal)
	};
	static_assert(sizeof(mm8::ActionItem) == 0xC, "Invalid \"mm8::ActionItem\" structure size");
	static_assert(offsetof(mm8::ActionItem, param) == 4);
	static_assert(offsetof(mm8::ActionItem, param2) == 8);
}

#pragma pack(pop)