#pragma once
#include "pch.h"
#include "main.h"

#pragma pack(push, 1)

namespace mm8
{
	struct LodSpriteLine // size: 0x8
	{
		int16_t l; // 0x0 (0 decimal)
		int16_t r; // 0x2 (2 decimal)
		uint8_t* pos; // 0x4 (4 decimal)
	};
	static_assert(sizeof(mm8::LodSpriteLine) == 0x8, "Invalid \"mm8::LodSpriteLine\" structure size");
	static_assert(offsetof(mm8::LodSpriteLine, r) == 2);
	static_assert(offsetof(mm8::LodSpriteLine, pos) == 4);



	struct LodSprite // size: 0x28
	{
		std::array<char, 12> name; // fixed size string, requires null terminator | 0x0 (0 decimal)
		int32_t dataSize; // 0xC (12 decimal)
		int16_t width; // 0x10 (16 decimal)
		union
		{
			uint32_t lines_size; // 0x12 (18 decimal)
			struct
			{
				int16_t height; // 0x12 (18 decimal)
				int16_t palette; // 0x14 (20 decimal)
			};
		};
		SKIP(2);
		int16_t YSkip; // 0x18 (24 decimal)
		SKIP(2);
		int32_t unpackedSize; // 0x1C (28 decimal)
		mm8::LodSpriteLine* lines; // size field offset is 0x12 | 0x20 (32 decimal)
		uint32_t buffer; // 0x24 (36 decimal)
	};
	static_assert(sizeof(mm8::LodSprite) == 0x28, "Invalid \"mm8::LodSprite\" structure size");
	static_assert(offsetof(mm8::LodSprite, dataSize) == 12);
	static_assert(offsetof(mm8::LodSprite, width) == 16);
	static_assert(offsetof(mm8::LodSprite, lines_size) == 18);
	static_assert(offsetof(mm8::LodSprite, height) == 18);
	static_assert(offsetof(mm8::LodSprite, palette) == 20);
	static_assert(offsetof(mm8::LodSprite, YSkip) == 24);
	static_assert(offsetof(mm8::LodSprite, unpackedSize) == 28);
	static_assert(offsetof(mm8::LodSprite, lines) == 32);
	static_assert(offsetof(mm8::LodSprite, buffer) == 36);



	struct LodSpriteD3D // size: 0x28
	{
		const char* name; // PChar (read-only) | 0x0 (0 decimal)
		int32_t palette; // 0x4 (4 decimal)
		uint32_t surface; // 0x8 (8 decimal)
		uint32_t texture; // 0xC (12 decimal)
		int32_t areaX; // 0x10 (16 decimal)
		int32_t areaY; // 0x14 (20 decimal)
		int32_t bufferWidth; // 0x18 (24 decimal)
		int32_t bufferHeight; // 0x1C (28 decimal)
		int32_t areaWidth; // 0x20 (32 decimal)
		int32_t areaHeight; // 0x24 (36 decimal)
	};
	static_assert(sizeof(mm8::LodSpriteD3D) == 0x28, "Invalid \"mm8::LodSpriteD3D\" structure size");
	static_assert(offsetof(mm8::LodSpriteD3D, palette) == 4);
	static_assert(offsetof(mm8::LodSpriteD3D, surface) == 8);
	static_assert(offsetof(mm8::LodSpriteD3D, texture) == 12);
	static_assert(offsetof(mm8::LodSpriteD3D, areaX) == 16);
	static_assert(offsetof(mm8::LodSpriteD3D, areaY) == 20);
	static_assert(offsetof(mm8::LodSpriteD3D, bufferWidth) == 24);
	static_assert(offsetof(mm8::LodSpriteD3D, bufferHeight) == 28);
	static_assert(offsetof(mm8::LodSpriteD3D, areaWidth) == 32);
	static_assert(offsetof(mm8::LodSpriteD3D, areaHeight) == 36);



	struct LodRecord // size: 0x48
	{
		uint32_t lodPtr; // 0x0 (0 decimal)
		uint32_t namePtr; // 0x4 (4 decimal)
		std::array<char, 64> name; // fixed size string, requires null terminator | 0x8 (8 decimal)
	};
	static_assert(sizeof(mm8::LodRecord) == 0x48, "Invalid \"mm8::LodRecord\" structure size");
	static_assert(offsetof(mm8::LodRecord, namePtr) == 4);
	static_assert(offsetof(mm8::LodRecord, name) == 8);



	struct CustomLods // size: 0x4804
	{
		int32_t recordIndex; // 0x0 (0 decimal)
		std::array<mm8::LodRecord, 256> records; // 0x4 (4 decimal)
	};
	static_assert(sizeof(mm8::CustomLods) == 0x4804, "Invalid \"mm8::CustomLods\" structure size");
	static_assert(offsetof(mm8::CustomLods, records) == 4);



	struct LodFile // size: 0x20
	{
		std::array<char, 16> name; // fixed size string, requires null terminator | 0x0 (0 decimal)
		int32_t offset; // 0x10 (16 decimal)
		int32_t size; // 0x14 (20 decimal)
		SKIP(8);
	};
	static_assert(sizeof(mm8::LodFile) == 0x20, "Invalid \"mm8::LodFile\" structure size");
	static_assert(offsetof(mm8::LodFile, offset) == 16);
	static_assert(offsetof(mm8::LodFile, size) == 20);



	struct SpritesLod // size: 0xECB8
	{
		static mm8::LodSprite* spritesSW; // original offset 0x2BFA790 (46114704 decimal)element size: 0x28 (40 decimal)
		static uint32_t* spritesSW_sizePtr; // Offset/size: 0xEC9C (60572 decimal)

		uint32_t file; // 0x0 (0 decimal)
		std::array<char, 256> fileName; // fixed size string, requires null terminator | 0x4 (4 decimal)
		uint32_t loaded; // 4-byte boolean | 0x104 (260 decimal)
		uint32_t IOBuffer; // 0x108 (264 decimal)
		int32_t IOBufferSize; // 0x10C (268 decimal)
		int32_t lodHeaderSignature; // 0x110 (272 decimal)
		SKIP(80);
		std::array<char, 80> description; // fixed size string, requires null terminator | 0x164 (356 decimal)
		SKIP(8);
		int32_t archivesCount; // 0x1BC (444 decimal)
		SKIP(80);
		uint32_t archivesCArray; // 0x210 (528 decimal)
		std::array<char, 16> type; // fixed size string, requires null terminator | 0x214 (532 decimal)
		uint32_t chapterHandle; // 0x224 (548 decimal)
		uint32_t chapterSize; // 0x228 (552 decimal)
		uint32_t files_size; // 0x22C (556 decimal)
		mm8::LodFile* files; // size field offset is 0x22C | 0x230 (560 decimal)
		uint32_t filesOffset; // 0x234 (564 decimal)
		SKIP(60004);
		uint32_t spritesSW_size; // 0xEC9C (60572 decimal)
		SKIP(12);
		uint32_t isHardware; // 4-byte boolean | 0xECAC (60588 decimal)
		mm8::LodSpriteD3D* spritesD3D; // size field offset is 0xECB4 | 0xECB0 (60592 decimal)
		uint32_t spritesD3D_size; // 0xECB4 (60596 decimal)
		SKIP(46054104);
		std::array<mm8::LodSprite, 10000> spritesSW; // 0x2BFA790 (46114704 decimal)
	};
	static_assert(sizeof(mm8::SpritesLod) == 0xECB8, "Invalid \"mm8::SpritesLod\" structure size");
	static_assert(offsetof(mm8::SpritesLod, fileName) == 4);
	static_assert(offsetof(mm8::SpritesLod, loaded) == 260);
	static_assert(offsetof(mm8::SpritesLod, IOBuffer) == 264);
	static_assert(offsetof(mm8::SpritesLod, IOBufferSize) == 268);
	static_assert(offsetof(mm8::SpritesLod, lodHeaderSignature) == 272);
	static_assert(offsetof(mm8::SpritesLod, description) == 356);
	static_assert(offsetof(mm8::SpritesLod, archivesCount) == 444);
	static_assert(offsetof(mm8::SpritesLod, archivesCArray) == 528);
	static_assert(offsetof(mm8::SpritesLod, type) == 532);
	static_assert(offsetof(mm8::SpritesLod, chapterHandle) == 548);
	static_assert(offsetof(mm8::SpritesLod, chapterSize) == 552);
	static_assert(offsetof(mm8::SpritesLod, files_size) == 556);
	static_assert(offsetof(mm8::SpritesLod, files) == 560);
	static_assert(offsetof(mm8::SpritesLod, filesOffset) == 564);
	static_assert(offsetof(mm8::SpritesLod, spritesSW_size) == 60572);
	static_assert(offsetof(mm8::SpritesLod, isHardware) == 60588);
	static_assert(offsetof(mm8::SpritesLod, spritesD3D) == 60592);
	static_assert(offsetof(mm8::SpritesLod, spritesD3D_size) == 60596);
	static_assert(offsetof(mm8::SpritesLod, spritesSW) == 46114704);



	struct Lod // size: 0x23C
	{
		uint32_t file; // 0x0 (0 decimal)
		std::array<char, 256> fileName; // fixed size string, requires null terminator | 0x4 (4 decimal)
		uint32_t loaded; // 4-byte boolean | 0x104 (260 decimal)
		uint32_t IOBuffer; // 0x108 (264 decimal)
		int32_t IOBufferSize; // 0x10C (268 decimal)
		int32_t lodHeaderSignature; // 0x110 (272 decimal)
		SKIP(80);
		std::array<char, 80> description; // fixed size string, requires null terminator | 0x164 (356 decimal)
		SKIP(8);
		int32_t archivesCount; // 0x1BC (444 decimal)
		SKIP(80);
		uint32_t archivesCArray; // 0x210 (528 decimal)
		std::array<char, 16> type; // fixed size string, requires null terminator | 0x214 (532 decimal)
		uint32_t chapterHandle; // 0x224 (548 decimal)
		uint32_t chapterSize; // 0x228 (552 decimal)
		uint32_t files_size; // 0x22C (556 decimal)
		mm8::LodFile* files; // size field offset is 0x22C | 0x230 (560 decimal)
		uint32_t filesOffset; // 0x234 (564 decimal)
		SKIP(4);
	};
	static_assert(sizeof(mm8::Lod) == 0x23C, "Invalid \"mm8::Lod\" structure size");
	static_assert(offsetof(mm8::Lod, fileName) == 4);
	static_assert(offsetof(mm8::Lod, loaded) == 260);
	static_assert(offsetof(mm8::Lod, IOBuffer) == 264);
	static_assert(offsetof(mm8::Lod, IOBufferSize) == 268);
	static_assert(offsetof(mm8::Lod, lodHeaderSignature) == 272);
	static_assert(offsetof(mm8::Lod, description) == 356);
	static_assert(offsetof(mm8::Lod, archivesCount) == 444);
	static_assert(offsetof(mm8::Lod, archivesCArray) == 528);
	static_assert(offsetof(mm8::Lod, type) == 532);
	static_assert(offsetof(mm8::Lod, chapterHandle) == 548);
	static_assert(offsetof(mm8::Lod, chapterSize) == 552);
	static_assert(offsetof(mm8::Lod, files_size) == 556);
	static_assert(offsetof(mm8::Lod, files) == 560);
	static_assert(offsetof(mm8::Lod, filesOffset) == 564);



	struct LodBitmap // size: 0x48
	{
		std::array<char, 16> name; // fixed size string, requires null terminator | 0x0 (0 decimal)
		int32_t bmpSize; // 0x10 (16 decimal)
		int32_t dataSize; // 0x14 (20 decimal)
		int16_t width; // 0x18 (24 decimal)
		int16_t height; // 0x1A (26 decimal)
		int16_t widthLn2; // 0x1C (28 decimal)
		int16_t heightLn2; // 0x1E (30 decimal)
		int16_t widthMinus1; // 0x20 (32 decimal)
		int16_t heightMinus1; // 0x22 (34 decimal)
		int16_t palette; // 0x24 (36 decimal)
		int16_t loadedPalette; // 0x26 (38 decimal)
		int32_t unpackedSize; // 0x28 (40 decimal)
		uint32_t bits; // 0x2C (44 decimal)
		int32_t image; // 0x30 (48 decimal)
		int32_t imageDiv2; // 0x34 (52 decimal)
		int32_t imageDiv4; // 0x38 (56 decimal)
		int32_t imageDiv8; // 0x3C (60 decimal)
		int32_t palette16; // 0x40 (64 decimal)
		int32_t palette24; // 0x44 (68 decimal)
	};
	static_assert(sizeof(mm8::LodBitmap) == 0x48, "Invalid \"mm8::LodBitmap\" structure size");
	static_assert(offsetof(mm8::LodBitmap, bmpSize) == 16);
	static_assert(offsetof(mm8::LodBitmap, dataSize) == 20);
	static_assert(offsetof(mm8::LodBitmap, width) == 24);
	static_assert(offsetof(mm8::LodBitmap, height) == 26);
	static_assert(offsetof(mm8::LodBitmap, widthLn2) == 28);
	static_assert(offsetof(mm8::LodBitmap, heightLn2) == 30);
	static_assert(offsetof(mm8::LodBitmap, widthMinus1) == 32);
	static_assert(offsetof(mm8::LodBitmap, heightMinus1) == 34);
	static_assert(offsetof(mm8::LodBitmap, palette) == 36);
	static_assert(offsetof(mm8::LodBitmap, loadedPalette) == 38);
	static_assert(offsetof(mm8::LodBitmap, unpackedSize) == 40);
	static_assert(offsetof(mm8::LodBitmap, bits) == 44);
	static_assert(offsetof(mm8::LodBitmap, image) == 48);
	static_assert(offsetof(mm8::LodBitmap, imageDiv2) == 52);
	static_assert(offsetof(mm8::LodBitmap, imageDiv4) == 56);
	static_assert(offsetof(mm8::LodBitmap, imageDiv8) == 60);
	static_assert(offsetof(mm8::LodBitmap, palette16) == 64);
	static_assert(offsetof(mm8::LodBitmap, palette24) == 68);



	struct BitmapsLod // size: 0x11BB8
	{
		uint32_t file; // 0x0 (0 decimal)
		std::array<char, 256> fileName; // fixed size string, requires null terminator | 0x4 (4 decimal)
		uint32_t loaded; // 4-byte boolean | 0x104 (260 decimal)
		uint32_t IOBuffer; // 0x108 (264 decimal)
		int32_t IOBufferSize; // 0x10C (268 decimal)
		int32_t lodHeaderSignature; // 0x110 (272 decimal)
		SKIP(80);
		std::array<char, 80> description; // fixed size string, requires null terminator | 0x164 (356 decimal)
		SKIP(8);
		int32_t archivesCount; // 0x1BC (444 decimal)
		SKIP(80);
		uint32_t archivesCArray; // 0x210 (528 decimal)
		std::array<char, 16> type; // fixed size string, requires null terminator | 0x214 (532 decimal)
		uint32_t chapterHandle; // 0x224 (548 decimal)
		uint32_t chapterSize; // 0x228 (552 decimal)
		uint32_t files_size; // 0x22C (556 decimal)
		mm8::LodFile* files; // size field offset is 0x22C | 0x230 (560 decimal)
		uint32_t filesOffset; // 0x234 (564 decimal)
		SKIP(4);
		std::array<mm8::LodBitmap, 1000> bitmaps; // size field offset is 0x11B7C | 0x23C (572 decimal)
		union
		{
			uint32_t bitmaps_size; // 0x11B7C (72572 decimal)
			uint32_t D3D_Surfaces_size; // 0x11B7C (72572 decimal)
			uint32_t D3D_Textures_size; // 0x11B7C (72572 decimal)
		};
		SKIP(36);
		uint32_t keepCompressed; // 4-byte boolean | 0x11BA4 (72612 decimal)
		uint32_t isHardware; // 4-byte boolean | 0x11BA8 (72616 decimal)
		SKIP(4);
		uint32_t* D3D_Surfaces; // size field offset is 0x11B7C | 0x11BB0 (72624 decimal)
		uint32_t* D3D_Textures; // size field offset is 0x11B7C | 0x11BB4 (72628 decimal)
	};
	static_assert(sizeof(mm8::BitmapsLod) == 0x11BB8, "Invalid \"mm8::BitmapsLod\" structure size");
	static_assert(offsetof(mm8::BitmapsLod, fileName) == 4);
	static_assert(offsetof(mm8::BitmapsLod, loaded) == 260);
	static_assert(offsetof(mm8::BitmapsLod, IOBuffer) == 264);
	static_assert(offsetof(mm8::BitmapsLod, IOBufferSize) == 268);
	static_assert(offsetof(mm8::BitmapsLod, lodHeaderSignature) == 272);
	static_assert(offsetof(mm8::BitmapsLod, description) == 356);
	static_assert(offsetof(mm8::BitmapsLod, archivesCount) == 444);
	static_assert(offsetof(mm8::BitmapsLod, archivesCArray) == 528);
	static_assert(offsetof(mm8::BitmapsLod, type) == 532);
	static_assert(offsetof(mm8::BitmapsLod, chapterHandle) == 548);
	static_assert(offsetof(mm8::BitmapsLod, chapterSize) == 552);
	static_assert(offsetof(mm8::BitmapsLod, files_size) == 556);
	static_assert(offsetof(mm8::BitmapsLod, files) == 560);
	static_assert(offsetof(mm8::BitmapsLod, filesOffset) == 564);
	static_assert(offsetof(mm8::BitmapsLod, bitmaps) == 572);
	static_assert(offsetof(mm8::BitmapsLod, bitmaps_size) == 72572);
	static_assert(offsetof(mm8::BitmapsLod, D3D_Surfaces_size) == 72572);
	static_assert(offsetof(mm8::BitmapsLod, D3D_Textures_size) == 72572);
	static_assert(offsetof(mm8::BitmapsLod, keepCompressed) == 72612);
	static_assert(offsetof(mm8::BitmapsLod, isHardware) == 72616);
	static_assert(offsetof(mm8::BitmapsLod, D3D_Surfaces) == 72624);
	static_assert(offsetof(mm8::BitmapsLod, D3D_Textures) == 72628);
}

#pragma pack(pop)