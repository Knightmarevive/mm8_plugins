#pragma once
#include "pch.h"
#include "main.h"

#pragma pack(push, 1)

namespace mm8
{
	struct TFTItem // size: 0x14
	{
		std::array<char, 12> name; // fixed size string, requires null terminator | 0x0 (0 decimal)
		int16_t index; // 0xC (12 decimal)
		int16_t time; // 0xE (14 decimal)
		int16_t totalTime; // 0x10 (16 decimal)
		union
		{
			uint16_t bits; // 0x12 (18 decimal)
			struct
			{
				bool notGroupEnd : 1; // 0x12 (18 decimal), bit index 7
				bool groupStart : 1; // 0x12 (18 decimal), bit index 6
			};
		};
	};
	static_assert(sizeof(mm8::TFTItem) == 0x14, "Invalid \"mm8::TFTItem\" structure size");
	static_assert(offsetof(mm8::TFTItem, index) == 12);
	static_assert(offsetof(mm8::TFTItem, time) == 14);
	static_assert(offsetof(mm8::TFTItem, totalTime) == 16);
	static_assert(offsetof(mm8::TFTItem, bits) == 18);



	struct SoundsItem // size: 0x98
	{
		std::array<char, 64> name; // fixed size string, requires null terminator | 0x0 (0 decimal)
		int32_t id; // 0x40 (64 decimal)
		int32_t type; // 0x44 (68 decimal)
		union
		{
			uint32_t bits; // 0x48 (72 decimal)
			struct
			{
				bool locked : 1; // 0x48 (72 decimal), bit index 7
				bool is3D : 1; // 0x48 (72 decimal), bit index 6
			};
		};
		std::array<uint32_t, 17> data; // 0x4C (76 decimal)
		uint32_t data3D; // 0x90 (144 decimal)
		uint32_t decompressed; // 4-byte boolean | 0x94 (148 decimal)
	};
	static_assert(sizeof(mm8::SoundsItem) == 0x98, "Invalid \"mm8::SoundsItem\" structure size");
	static_assert(offsetof(mm8::SoundsItem, id) == 64);
	static_assert(offsetof(mm8::SoundsItem, type) == 68);
	static_assert(offsetof(mm8::SoundsItem, bits) == 72);
	static_assert(offsetof(mm8::SoundsItem, data) == 76);
	static_assert(offsetof(mm8::SoundsItem, data3D) == 144);
	static_assert(offsetof(mm8::SoundsItem, decompressed) == 148);



	struct SFTItem // size: 0x3C
	{
		std::array<char, 12> groupName; // fixed size string, requires null terminator | 0x0 (0 decimal)
		std::array<char, 12> spriteName; // fixed size string, requires null terminator | 0xC (12 decimal)
		std::array<int16_t, 8> spriteIndex; // 0x18 (24 decimal)
		int32_t scale; // 0x28 (40 decimal)
		union
		{
			uint32_t bits; // 0x2C (44 decimal)
			struct
			{
				bool notGroupEnd : 1; // 0x2C (44 decimal), bit index 7
				bool luminous : 1; // 0x2C (44 decimal), bit index 6
				bool groupStart : 1; // 0x2C (44 decimal), bit index 5
				SKIPBITS(1);
				bool image1 : 1; // 0x2C (44 decimal), bit index 3
				bool center : 1; // 0x2D (45 decimal), bit index 2
				bool fidget : 1; // 0x2D (45 decimal), bit index 1
				bool loaded : 1; // 0x2D (45 decimal), bit index 0
				bool mirror0 : 1; // 0x2D (45 decimal), bit index 7
				bool mirror1 : 1; // 0x2D (45 decimal), bit index 6
				bool mirror2 : 1; // 0x2D (45 decimal), bit index 5
				bool mirror3 : 1; // 0x2D (45 decimal), bit index 4
				bool mirror4 : 1; // 0x2E (46 decimal), bit index 3
				bool mirror5 : 1; // 0x2E (46 decimal), bit index 2
				bool mirror6 : 1; // 0x2E (46 decimal), bit index 1
				bool mirror7 : 1; // 0x2E (46 decimal), bit index 0
				bool images3 : 1; // 0x2E (46 decimal), bit index 7
				bool glow : 1; // 0x2E (46 decimal), bit index 6
				bool transparent : 1; // 0x2E (46 decimal), bit index 5
			};
		};
		int16_t lightRadius; // 0x30 (48 decimal)
		int16_t paletteId; // 0x32 (50 decimal)
		int16_t paletteIndex; // 0x34 (52 decimal)
		int16_t time; // 0x36 (54 decimal)
		int16_t totalTime; // 0x38 (56 decimal)
		SKIP(2);
	};
	static_assert(sizeof(mm8::SFTItem) == 0x3C, "Invalid \"mm8::SFTItem\" structure size");
	static_assert(offsetof(mm8::SFTItem, spriteName) == 12);
	static_assert(offsetof(mm8::SFTItem, spriteIndex) == 24);
	static_assert(offsetof(mm8::SFTItem, scale) == 40);
	static_assert(offsetof(mm8::SFTItem, bits) == 44);
	static_assert(offsetof(mm8::SFTItem, lightRadius) == 48);
	static_assert(offsetof(mm8::SFTItem, paletteId) == 50);
	static_assert(offsetof(mm8::SFTItem, paletteIndex) == 52);
	static_assert(offsetof(mm8::SFTItem, time) == 54);
	static_assert(offsetof(mm8::SFTItem, totalTime) == 56);



	struct SFT // size: 0x18
	{
		uint32_t frames_size; // 0x0 (0 decimal)
		union
		{
			uint32_t groupIndex_size; // 0x4 (4 decimal)
			uint32_t groups_size; // 0x4 (4 decimal)
		};
		int32_t matchIndex; // 0x8 (8 decimal)
		mm8::SFTItem* frames; // size field offset is 0x0 | 0xC (12 decimal)
		mm8::SFTItem** groups; // size field offset is 0x4 | 0x10 (16 decimal)
		int16_t* groupIndex; // size field offset is 0x4 | 0x14 (20 decimal)
	};
	static_assert(sizeof(mm8::SFT) == 0x18, "Invalid \"mm8::SFT\" structure size");
	static_assert(offsetof(mm8::SFT, groupIndex_size) == 4);
	static_assert(offsetof(mm8::SFT, groups_size) == 4);
	static_assert(offsetof(mm8::SFT, matchIndex) == 8);
	static_assert(offsetof(mm8::SFT, frames) == 12);
	static_assert(offsetof(mm8::SFT, groups) == 16);
	static_assert(offsetof(mm8::SFT, groupIndex) == 20);



	struct PFTItem // size: 0xA
	{
		uint16_t groupId; // 0x0 (0 decimal)
		uint16_t frameIndex; // 0x2 (2 decimal)
		uint16_t time; // 0x4 (4 decimal)
		uint16_t totalTime; // 0x6 (6 decimal)
		union
		{
			uint16_t bits; // 0x8 (8 decimal)
			struct
			{
				bool notGroupEnd : 1; // 0x8 (8 decimal), bit index 7
				SKIPBITS(1);
				bool groupStart : 1; // 0x8 (8 decimal), bit index 5
			};
		};
	};
	static_assert(sizeof(mm8::PFTItem) == 0xA, "Invalid \"mm8::PFTItem\" structure size");
	static_assert(offsetof(mm8::PFTItem, frameIndex) == 2);
	static_assert(offsetof(mm8::PFTItem, time) == 4);
	static_assert(offsetof(mm8::PFTItem, totalTime) == 6);
	static_assert(offsetof(mm8::PFTItem, bits) == 8);



	struct OverlayItem // size: 0x8
	{
		int16_t id; // 0x0 (0 decimal)
		int16_t type; // 0x2 (2 decimal)
		union
		{
			std::array<uint8_t, 2> SFTGroup; // Unknown type | 0x4 (4 decimal)
			int16_t SFTIndex; // 0x4 (4 decimal)
		};
		SKIP(2);
	};
	static_assert(sizeof(mm8::OverlayItem) == 0x8, "Invalid \"mm8::OverlayItem\" structure size");
	static_assert(offsetof(mm8::OverlayItem, type) == 2);
	static_assert(offsetof(mm8::OverlayItem, SFTGroup) == 4);
	static_assert(offsetof(mm8::OverlayItem, SFTIndex) == 4);



	struct ObjListItem // size: 0x38
	{
		std::array<char, 32> name; // fixed size string, requires null terminator | 0x0 (0 decimal)
		int16_t id; // 0x20 (32 decimal)
		int16_t radius; // 0x22 (34 decimal)
		int16_t height; // 0x24 (36 decimal)
		union
		{
			uint16_t bits; // 0x26 (38 decimal)
			struct
			{
				bool invisible : 1; // 0x26 (38 decimal), bit index 7
				bool untouchable : 1; // 0x26 (38 decimal), bit index 6
				bool temporary : 1; // 0x26 (38 decimal), bit index 5
				bool lifetimeInSFT : 1; // 0x26 (38 decimal), bit index 4
				bool noPickup : 1; // 0x26 (38 decimal), bit index 3
				bool noGravity : 1; // 0x27 (39 decimal), bit index 2
				bool interceptAction : 1; // 0x27 (39 decimal), bit index 1
				bool bounce : 1; // 0x27 (39 decimal), bit index 0
				bool trailParticles : 1; // 0x27 (39 decimal), bit index 7
				bool trailFire : 1; // 0x27 (39 decimal), bit index 6
				bool trailLine : 1; // 0x27 (39 decimal), bit index 5
			};
		};
		union
		{
			std::array<uint8_t, 2> SFTGroup; // Unknown type | 0x28 (40 decimal)
			int16_t SFTIndex; // 0x28 (40 decimal)
		};
		int16_t lifeTime; // 0x2A (42 decimal)
		uint32_t loadedParticlesColor; // 0x2C (44 decimal)
		uint16_t speed; // 0x30 (48 decimal)
		union
		{
			std::array<uint8_t, 4> particlesColor; // 0x32 (50 decimal)
			struct
			{
				uint8_t particleR; // 0x32 (50 decimal)
				uint8_t particleG; // 0x33 (51 decimal)
				uint8_t particleB; // 0x34 (52 decimal)
				uint8_t particleA; // 0x35 (53 decimal)
			};
		};
		SKIP(2);
	};
	static_assert(sizeof(mm8::ObjListItem) == 0x38, "Invalid \"mm8::ObjListItem\" structure size");
	static_assert(offsetof(mm8::ObjListItem, id) == 32);
	static_assert(offsetof(mm8::ObjListItem, radius) == 34);
	static_assert(offsetof(mm8::ObjListItem, height) == 36);
	static_assert(offsetof(mm8::ObjListItem, bits) == 38);
	static_assert(offsetof(mm8::ObjListItem, SFTGroup) == 40);
	static_assert(offsetof(mm8::ObjListItem, SFTIndex) == 40);
	static_assert(offsetof(mm8::ObjListItem, lifeTime) == 42);
	static_assert(offsetof(mm8::ObjListItem, loadedParticlesColor) == 44);
	static_assert(offsetof(mm8::ObjListItem, speed) == 48);
	static_assert(offsetof(mm8::ObjListItem, particlesColor) == 50);
	static_assert(offsetof(mm8::ObjListItem, particleR) == 50);
	static_assert(offsetof(mm8::ObjListItem, particleG) == 51);
	static_assert(offsetof(mm8::ObjListItem, particleB) == 52);
	static_assert(offsetof(mm8::ObjListItem, particleA) == 53);



	struct MonListItem // size: 0xB8
	{
		int16_t height; // 0x0 (0 decimal)
		int16_t radius; // 0x2 (2 decimal)
		int16_t velocity; // 0x4 (4 decimal)
		int16_t radius2; // 0x6 (6 decimal)
		union
		{
			int32_t tint; // 0x8 (8 decimal)
			struct
			{
				uint8_t tintB; // 0x8 (8 decimal)
				uint8_t tintG; // 0x9 (9 decimal)
				uint8_t tintR; // 0xA (10 decimal)
				uint8_t tintA; // 0xB (11 decimal)
			};
		};
		union
		{
			std::array<int16_t, 4> sounds; // 0xC (12 decimal)
			struct
			{
				int16_t soundAttack; // 0xC (12 decimal)
				int16_t soundDie; // 0xE (14 decimal)
				int16_t soundGetHit; // 0x10 (16 decimal)
				int16_t soundFidget; // 0x12 (18 decimal)
			};
		};
		std::array<char, 64> name; // fixed size string, requires null terminator | 0x14 (20 decimal)
		union
		{
			std::array<std::array<char, 10>, 10> frameNames; // fixed size string, requires null terminator | 0x54 (84 decimal)
			struct
			{
				std::array<char, 10> framesStand; // fixed size string, requires null terminator | 0x54 (84 decimal)
				std::array<char, 10> framesWalk; // fixed size string, requires null terminator | 0x5E (94 decimal)
				std::array<char, 10> framesAttack; // fixed size string, requires null terminator | 0x68 (104 decimal)
				std::array<char, 10> framesShoot; // fixed size string, requires null terminator | 0x72 (114 decimal)
				std::array<char, 10> framesStun; // fixed size string, requires null terminator | 0x7C (124 decimal)
				std::array<char, 10> framesDie; // fixed size string, requires null terminator | 0x86 (134 decimal)
				std::array<char, 10> framesDead; // fixed size string, requires null terminator | 0x90 (144 decimal)
				std::array<char, 10> framesFidget; // fixed size string, requires null terminator | 0x9A (154 decimal)
			};
		};
	};
	static_assert(sizeof(mm8::MonListItem) == 0xB8, "Invalid \"mm8::MonListItem\" structure size");
	static_assert(offsetof(mm8::MonListItem, radius) == 2);
	static_assert(offsetof(mm8::MonListItem, velocity) == 4);
	static_assert(offsetof(mm8::MonListItem, radius2) == 6);
	static_assert(offsetof(mm8::MonListItem, tint) == 8);
	static_assert(offsetof(mm8::MonListItem, tintB) == 8);
	static_assert(offsetof(mm8::MonListItem, tintG) == 9);
	static_assert(offsetof(mm8::MonListItem, tintR) == 10);
	static_assert(offsetof(mm8::MonListItem, tintA) == 11);
	static_assert(offsetof(mm8::MonListItem, sounds) == 12);
	static_assert(offsetof(mm8::MonListItem, soundAttack) == 12);
	static_assert(offsetof(mm8::MonListItem, soundDie) == 14);
	static_assert(offsetof(mm8::MonListItem, soundGetHit) == 16);
	static_assert(offsetof(mm8::MonListItem, soundFidget) == 18);
	static_assert(offsetof(mm8::MonListItem, name) == 20);
	static_assert(offsetof(mm8::MonListItem, frameNames) == 84);
	static_assert(offsetof(mm8::MonListItem, framesStand) == 84);
	static_assert(offsetof(mm8::MonListItem, framesWalk) == 94);
	static_assert(offsetof(mm8::MonListItem, framesAttack) == 104);
	static_assert(offsetof(mm8::MonListItem, framesShoot) == 114);
	static_assert(offsetof(mm8::MonListItem, framesStun) == 124);
	static_assert(offsetof(mm8::MonListItem, framesDie) == 134);
	static_assert(offsetof(mm8::MonListItem, framesDead) == 144);
	static_assert(offsetof(mm8::MonListItem, framesFidget) == 154);



	struct IFTItem // size: 0x20
	{
		std::array<char, 12> groupName; // fixed size string, requires null terminator | 0x0 (0 decimal)
		std::array<char, 12> iconName; // fixed size string, requires null terminator | 0xC (12 decimal)
		uint16_t time; // 0x18 (24 decimal)
		uint16_t totalTime; // 0x1A (26 decimal)
		union
		{
			uint16_t bits; // 0x1C (28 decimal)
			struct
			{
				bool notGroupEnd : 1; // 0x1C (28 decimal), bit index 7
				SKIPBITS(1);
				bool groupStart : 1; // 0x1C (28 decimal), bit index 5
			};
		};
		uint16_t iconIndex; // 0x1E (30 decimal)
	};
	static_assert(sizeof(mm8::IFTItem) == 0x20, "Invalid \"mm8::IFTItem\" structure size");
	static_assert(offsetof(mm8::IFTItem, iconName) == 12);
	static_assert(offsetof(mm8::IFTItem, time) == 24);
	static_assert(offsetof(mm8::IFTItem, totalTime) == 26);
	static_assert(offsetof(mm8::IFTItem, bits) == 28);
	static_assert(offsetof(mm8::IFTItem, iconIndex) == 30);



	struct DecListItem // size: 0x54
	{
		std::array<char, 32> name; // fixed size string, requires null terminator | 0x0 (0 decimal)
		std::array<char, 32> gameName; // fixed size string, requires null terminator | 0x20 (32 decimal)
		int16_t type; // 0x40 (64 decimal)
		int16_t height; // 0x42 (66 decimal)
		int16_t radius; // 0x44 (68 decimal)
		int16_t lightRadius; // 0x46 (70 decimal)
		union
		{
			std::array<uint8_t, 2> SFTGroup; // Unknown type | 0x48 (72 decimal)
			int16_t SFTIndex; // 0x48 (72 decimal)
		};
		union
		{
			uint16_t bits; // 0x4A (74 decimal)
			struct
			{
				bool noBlockMovement : 1; // 0x4A (74 decimal), bit index 7
				bool noDraw : 1; // 0x4A (74 decimal), bit index 6
				bool flickerSlow : 1; // 0x4A (74 decimal), bit index 5
				bool flickerMedium : 1; // 0x4A (74 decimal), bit index 4
				bool flickerFast : 1; // 0x4A (74 decimal), bit index 3
				bool marker : 1; // 0x4B (75 decimal), bit index 2
				bool slowLoop : 1; // 0x4B (75 decimal), bit index 1
				bool emitFire : 1; // 0x4B (75 decimal), bit index 0
				bool soundOnDawn : 1; // 0x4B (75 decimal), bit index 7
				bool soundOnDusk : 1; // 0x4B (75 decimal), bit index 6
				bool emitSmoke : 1; // 0x4B (75 decimal), bit index 5
			};
		};
		int16_t soundId; // 0x4C (76 decimal)
		SKIP(2);
		uint8_t red; // 0x50 (80 decimal)
		uint8_t green; // 0x51 (81 decimal)
		uint8_t blue; // 0x52 (82 decimal)
		SKIP(1);
	};
	static_assert(sizeof(mm8::DecListItem) == 0x54, "Invalid \"mm8::DecListItem\" structure size");
	static_assert(offsetof(mm8::DecListItem, gameName) == 32);
	static_assert(offsetof(mm8::DecListItem, type) == 64);
	static_assert(offsetof(mm8::DecListItem, height) == 66);
	static_assert(offsetof(mm8::DecListItem, radius) == 68);
	static_assert(offsetof(mm8::DecListItem, lightRadius) == 70);
	static_assert(offsetof(mm8::DecListItem, SFTGroup) == 72);
	static_assert(offsetof(mm8::DecListItem, SFTIndex) == 72);
	static_assert(offsetof(mm8::DecListItem, bits) == 74);
	static_assert(offsetof(mm8::DecListItem, soundId) == 76);
	static_assert(offsetof(mm8::DecListItem, red) == 80);
	static_assert(offsetof(mm8::DecListItem, green) == 81);
	static_assert(offsetof(mm8::DecListItem, blue) == 82);



	struct TileItem // size: 0x1A
	{
		std::array<char, 16> name; // fixed size string, requires null terminator | 0x0 (0 decimal)
		int16_t id; // 0x10 (16 decimal)
		int16_t bitmap; // 0x12 (18 decimal)
		int16_t tileSet; // 0x14 (20 decimal)
		int16_t section; // 0x16 (22 decimal)
		union
		{
			uint16_t bits; // 0x18 (24 decimal)
			struct
			{
				bool burn : 1; // 0x18 (24 decimal), bit index 7
				bool water : 1; // 0x18 (24 decimal), bit index 6
				bool block : 1; // 0x18 (24 decimal), bit index 5
				bool repulse : 1; // 0x18 (24 decimal), bit index 4
				bool flat : 1; // 0x18 (24 decimal), bit index 3
				bool wave : 1; // 0x19 (25 decimal), bit index 2
				bool noDraw : 1; // 0x19 (25 decimal), bit index 1
				SKIPBITS(1);
				bool waterTransition : 1; // 0x19 (25 decimal), bit index 7
				bool transition : 1; // 0x19 (25 decimal), bit index 6
				bool scrollDown : 1; // 0x19 (25 decimal), bit index 5
				bool scrollUp : 1; // 0x19 (25 decimal), bit index 4
				bool scrollLeft : 1; // 0x1A (26 decimal), bit index 3
				bool scrollRight : 1; // 0x1A (26 decimal), bit index 2
			};
		};
	};
	static_assert(sizeof(mm8::TileItem) == 0x1A, "Invalid \"mm8::TileItem\" structure size");
	static_assert(offsetof(mm8::TileItem, id) == 16);
	static_assert(offsetof(mm8::TileItem, bitmap) == 18);
	static_assert(offsetof(mm8::TileItem, tileSet) == 20);
	static_assert(offsetof(mm8::TileItem, section) == 22);
	static_assert(offsetof(mm8::TileItem, bits) == 24);



	struct CurrentTileBin // size: 0x8
	{
		uint32_t items_size; // 0x0 (0 decimal)
		mm8::TileItem* items; // size field offset is 0x0 | 0x4 (4 decimal)
	};
	static_assert(sizeof(mm8::CurrentTileBin) == 0x8, "Invalid \"mm8::CurrentTileBin\" structure size");
	static_assert(offsetof(mm8::CurrentTileBin, items) == 4);



	struct DChestItem // size: 0x24
	{
		std::array<char, 32> name; // fixed size string, requires null terminator | 0x0 (0 decimal)
		uint8_t width; // 0x20 (32 decimal)
		uint8_t height; // 0x21 (33 decimal)
		int16_t imageIndex; // 0x22 (34 decimal)
	};
	static_assert(sizeof(mm8::DChestItem) == 0x24, "Invalid \"mm8::DChestItem\" structure size");
	static_assert(offsetof(mm8::DChestItem, width) == 32);
	static_assert(offsetof(mm8::DChestItem, height) == 33);
	static_assert(offsetof(mm8::DChestItem, imageIndex) == 34);
}

#pragma pack(pop)