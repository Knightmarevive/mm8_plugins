#pragma once
#include "pch.h"
#include "main.h"
#include "Monster.h"
#include "Common.h"

#pragma pack(push, 1)

namespace mm8
{
	struct GameScreen // size: 0x400F0
	{
		uint32_t isD3D; // 4-byte boolean | 0x0 (0 decimal)
		SKIP(12);
		int32_t width; // 0x10 (16 decimal)
		int32_t height; // 0x14 (20 decimal)
		int32_t pitch; // 0x18 (24 decimal)
		int32_t cx1; // 0x1C (28 decimal)
		int32_t cy1; // 0x20 (32 decimal)
		int32_t cx2; // 0x24 (36 decimal)
		int32_t cy2; // 0x28 (40 decimal)
		SKIP(262152);
		uint32_t objectByPixel; // 0x40034 (262196 decimal)
		SKIP(148);
		uint32_t redBits; // 0x400CC (262348 decimal)
		uint32_t blueBits; // 0x400D0 (262352 decimal)
		uint32_t greenBits; // 0x400D4 (262356 decimal)
		uint32_t redMask; // 0x400D8 (262360 decimal)
		uint32_t greenMask; // 0x400DC (262364 decimal)
		uint32_t blueMask; // 0x400E0 (262368 decimal)
		SKIP(8);
		uint32_t buffer; // 0x400EC (262380 decimal)
	};
	static_assert(sizeof(mm8::GameScreen) == 0x400F0, "Invalid \"mm8::GameScreen\" structure size");
	static_assert(offsetof(mm8::GameScreen, width) == 16);
	static_assert(offsetof(mm8::GameScreen, height) == 20);
	static_assert(offsetof(mm8::GameScreen, pitch) == 24);
	static_assert(offsetof(mm8::GameScreen, cx1) == 28);
	static_assert(offsetof(mm8::GameScreen, cy1) == 32);
	static_assert(offsetof(mm8::GameScreen, cx2) == 36);
	static_assert(offsetof(mm8::GameScreen, cy2) == 40);
	static_assert(offsetof(mm8::GameScreen, objectByPixel) == 262196);
	static_assert(offsetof(mm8::GameScreen, redBits) == 262348);
	static_assert(offsetof(mm8::GameScreen, blueBits) == 262352);
	static_assert(offsetof(mm8::GameScreen, greenBits) == 262356);
	static_assert(offsetof(mm8::GameScreen, redMask) == 262360);
	static_assert(offsetof(mm8::GameScreen, greenMask) == 262364);
	static_assert(offsetof(mm8::GameScreen, blueMask) == 262368);
	static_assert(offsetof(mm8::GameScreen, buffer) == 262380);



	struct DialogLogic // size: 0x5DC8E0
	{
		static int32_t* list; // original offset 0x5DB940 (6142272 decimal)element size: 0x4 (4 decimal)
		static uint32_t* list_sizePtr; // Offset/size: 0x517AF4 (5339892 decimal)

		SKIP(5191936);
		std::array<int32_t, 5> armorSkills; // requires unprotect before change | 0x4F3900 (5191936 decimal)
		std::array<int32_t, 9> weaponSkills; // requires unprotect before change | 0x4F3914 (5191956 decimal)
		std::array<int32_t, 12> miscSkills; // requires unprotect before change | 0x4F3938 (5191992 decimal)
		std::array<int32_t, 12> magicSkills; // requires unprotect before change | 0x4F3968 (5192040 decimal)
		SKIP(147796);
		int32_t scrollPage; // 0x517AEC (5339884 decimal)
		int32_t countOnScreen; // 0x517AF0 (5339888 decimal)
		union
		{
			int32_t listCount; // 0x517AF4 (5339892 decimal)
			uint32_t list_size; // 0x517AF4 (5339892 decimal)
		};
		int32_t scrollPos; // 0x517AF8 (5339896 decimal)
		uint32_t autonoteTab6Clicked; // 4-byte boolean | 0x517AFC (5339900 decimal)
		uint32_t autonoteTab5Clicked; // 4-byte boolean | 0x517B00 (5339904 decimal)
		union
		{
			uint32_t autonoteTab4Clicked; // 4-byte boolean | 0x517B04 (5339908 decimal)
			uint32_t mapMoveRightClicked; // 4-byte boolean | 0x517B04 (5339908 decimal)
		};
		union
		{
			uint32_t autonoteTab3Clicked; // 4-byte boolean | 0x517B08 (5339912 decimal)
			uint32_t mapMoveLeftClicked; // 4-byte boolean | 0x517B08 (5339912 decimal)
		};
		union
		{
			uint32_t autonoteTab2Clicked; // 4-byte boolean | 0x517B0C (5339916 decimal)
			uint32_t mapMoveDownClicked; // 4-byte boolean | 0x517B0C (5339916 decimal)
		};
		union
		{
			uint32_t autonoteTab1Clicked; // 4-byte boolean | 0x517B10 (5339920 decimal)
			uint32_t mapMoveUpClicked; // 4-byte boolean | 0x517B10 (5339920 decimal)
		};
		uint32_t scrollDownClicked; // 4-byte boolean | 0x517B14 (5339924 decimal)
		uint32_t scrollUpClicked; // 4-byte boolean | 0x517B18 (5339928 decimal)
		int32_t spellBookSelection; // 0x517B1C (5339932 decimal)
		bool spellBookSelectedNewSpell; // 0x517B20 (5339936 decimal)
		SKIP(23);
		int32_t autonotesCategory; // 0x517B38 (5339960 decimal)
		SKIP(20);
		mm8::MapMonster monsterInfoMonster; // 0x517B50 (5339984 decimal)
		SKIP(801316);
		std::array<int32_t, 1000> list; // 0x5DB940 (6142272 decimal)
	};
	static_assert(sizeof(mm8::DialogLogic) == 0x5DC8E0, "Invalid \"mm8::DialogLogic\" structure size");
	static_assert(offsetof(mm8::DialogLogic, armorSkills) == 5191936);
	static_assert(offsetof(mm8::DialogLogic, weaponSkills) == 5191956);
	static_assert(offsetof(mm8::DialogLogic, miscSkills) == 5191992);
	static_assert(offsetof(mm8::DialogLogic, magicSkills) == 5192040);
	static_assert(offsetof(mm8::DialogLogic, scrollPage) == 5339884);
	static_assert(offsetof(mm8::DialogLogic, countOnScreen) == 5339888);
	static_assert(offsetof(mm8::DialogLogic, listCount) == 5339892);
	static_assert(offsetof(mm8::DialogLogic, list_size) == 5339892);
	static_assert(offsetof(mm8::DialogLogic, scrollPos) == 5339896);
	static_assert(offsetof(mm8::DialogLogic, autonoteTab6Clicked) == 5339900);
	static_assert(offsetof(mm8::DialogLogic, autonoteTab5Clicked) == 5339904);
	static_assert(offsetof(mm8::DialogLogic, autonoteTab4Clicked) == 5339908);
	static_assert(offsetof(mm8::DialogLogic, mapMoveRightClicked) == 5339908);
	static_assert(offsetof(mm8::DialogLogic, autonoteTab3Clicked) == 5339912);
	static_assert(offsetof(mm8::DialogLogic, mapMoveLeftClicked) == 5339912);
	static_assert(offsetof(mm8::DialogLogic, autonoteTab2Clicked) == 5339916);
	static_assert(offsetof(mm8::DialogLogic, mapMoveDownClicked) == 5339916);
	static_assert(offsetof(mm8::DialogLogic, autonoteTab1Clicked) == 5339920);
	static_assert(offsetof(mm8::DialogLogic, mapMoveUpClicked) == 5339920);
	static_assert(offsetof(mm8::DialogLogic, scrollDownClicked) == 5339924);
	static_assert(offsetof(mm8::DialogLogic, scrollUpClicked) == 5339928);
	static_assert(offsetof(mm8::DialogLogic, spellBookSelection) == 5339932);
	static_assert(offsetof(mm8::DialogLogic, spellBookSelectedNewSpell) == 5339936);
	static_assert(offsetof(mm8::DialogLogic, autonotesCategory) == 5339960);
	static_assert(offsetof(mm8::DialogLogic, monsterInfoMonster) == 5339984);
	static_assert(offsetof(mm8::DialogLogic, list) == 6142272);



	struct Dlg // size: 0x54
	{
		int32_t left; // 0x0 (0 decimal)
		int32_t top; // 0x4 (4 decimal)
		int32_t width; // 0x8 (8 decimal)
		int32_t height; // 0xC (12 decimal)
		int32_t right_; // 0x10 (16 decimal)
		int32_t bottom_; // 0x14 (20 decimal)
		int32_t dlgID; // 0x18 (24 decimal)
		int32_t param; // 0x1C (28 decimal)
		int32_t itemsCount; // 0x20 (32 decimal)
		SKIP(4);
		int32_t keyboardItemsCount; // 0x28 (40 decimal)
		int32_t keyboardItem; // 0x2C (44 decimal)
		int32_t keyboardNavigationTrackMouse; // 0x30 (48 decimal)
		int32_t keyboardLeftRightStep; // 0x34 (52 decimal)
		int32_t keyboardItemsStart; // 0x38 (56 decimal)
		int32_t index; // 0x3C (60 decimal)
		SKIP(4);
		int32_t useKeyboadNavigation; // 0x44 (68 decimal)
		SKIP(4);
		uint32_t firstItemPtr; // 0x4C (76 decimal)
		uint32_t lastItemPtr; // 0x50 (80 decimal)
	};
	static_assert(sizeof(mm8::Dlg) == 0x54, "Invalid \"mm8::Dlg\" structure size");
	static_assert(offsetof(mm8::Dlg, top) == 4);
	static_assert(offsetof(mm8::Dlg, width) == 8);
	static_assert(offsetof(mm8::Dlg, height) == 12);
	static_assert(offsetof(mm8::Dlg, right_) == 16);
	static_assert(offsetof(mm8::Dlg, bottom_) == 20);
	static_assert(offsetof(mm8::Dlg, dlgID) == 24);
	static_assert(offsetof(mm8::Dlg, param) == 28);
	static_assert(offsetof(mm8::Dlg, itemsCount) == 32);
	static_assert(offsetof(mm8::Dlg, keyboardItemsCount) == 40);
	static_assert(offsetof(mm8::Dlg, keyboardItem) == 44);
	static_assert(offsetof(mm8::Dlg, keyboardNavigationTrackMouse) == 48);
	static_assert(offsetof(mm8::Dlg, keyboardLeftRightStep) == 52);
	static_assert(offsetof(mm8::Dlg, keyboardItemsStart) == 56);
	static_assert(offsetof(mm8::Dlg, index) == 60);
	static_assert(offsetof(mm8::Dlg, useKeyboadNavigation) == 68);
	static_assert(offsetof(mm8::Dlg, firstItemPtr) == 76);
	static_assert(offsetof(mm8::Dlg, lastItemPtr) == 80);



	struct GameClasses // size: 0x6007A8CC
	{
		static uint8_t* HPBase; // original offset 0x4FD9DC (5233116 decimal)element size: 0x1 (1 decimal)
		static uint32_t HPBase_size; // Offset/size: 0x7A (122 decimal)
		static uint8_t* HPFactor; // original offset 0x4FD9FC (5233148 decimal)element size: 0x1 (1 decimal)
		static uint32_t HPFactor_size; // Offset/size: 0x7A (122 decimal)
		static uint8_t* SPBase; // original offset 0x4FD9EC (5233132 decimal)element size: 0x1 (1 decimal)
		static uint32_t SPBase_size; // Offset/size: 0x7A (122 decimal)
		static uint8_t* SPFactor; // original offset 0x4FDA0C (5233164 decimal)element size: 0x1 (1 decimal)
		static uint32_t SPFactor_size; // Offset/size: 0x7A (122 decimal)
		static uint8_t* SPStats; // original offset 0x6007A8BC (1611114684 decimal)element size: 0x1 (1 decimal)
		static uint32_t SPStats_size; // Offset/size: 0x7A (122 decimal)
		static std::array<uint8_t, 39>* skills; // original offset 0x4FDD18 (5233944 decimal)element size: 0x27 (39 decimal)
		static uint32_t skills_size; // Offset/size: 0x7A (122 decimal)
		static std::array<mm8::StartStat, 7>* startingStats; // original offset 0x4FDA20 (5233184 decimal)element size: 0x1C (28 decimal)
		static uint32_t startingStats_size; // Offset/size: 0x28 (40 decimal)

		SKIP(5233116);
		union
		{
			std::array<std::array<uint8_t, 39>, 122> skills; // 0x4FDD18 (5233944 decimal)
			struct
			{
				union
				{
					std::array<std::array<mm8::StartStat, 7>, 40> startingStats; // 0x4FDA20 (5233184 decimal)
					struct
					{
						union
						{
							std::array<uint8_t, 122> HPBase; // 0x4FD9DC (5233116 decimal)
							std::array<uint8_t, 122> SPBase; // 0x4FD9EC (5233132 decimal)
							std::array<uint8_t, 122> HPFactor; // 0x4FD9FC (5233148 decimal)
							std::array<uint8_t, 122> SPFactor; // 0x4FDA0C (5233164 decimal)
						};
					};
				};
			};
		};
		SKIP(1605875982);
		std::array<uint8_t, 122> SPStats; // 0x6007A8BC (1611114684 decimal)
	};
	static_assert(sizeof(mm8::GameClasses) == 0x6007A8CC, "Invalid \"mm8::GameClasses\" structure size");
	static_assert(offsetof(mm8::GameClasses, skills) == 5233944);
	static_assert(offsetof(mm8::GameClasses, startingStats) == 5233184);
	static_assert(offsetof(mm8::GameClasses, HPBase) == 5233116);
	static_assert(offsetof(mm8::GameClasses, SPBase) == 5233132);
	static_assert(offsetof(mm8::GameClasses, HPFactor) == 5233148);
	static_assert(offsetof(mm8::GameClasses, SPFactor) == 5233164);
	static_assert(offsetof(mm8::GameClasses, SPStats) == 1611114684);



	struct GameClassKinds // size: 0x4FDD18
	{
		static std::array<uint8_t, 39>* startingSkills; // original offset 0x4FDBE0 (5233632 decimal)element size: 0x27 (39 decimal)
		static uint32_t startingSkills_size; // Offset/size: 0x3D (61 decimal)

		SKIP(5233632);
		std::array<std::array<uint8_t, 39>, 61> startingSkills; // 0x4FDBE0 (5233632 decimal)
	};
	static_assert(sizeof(mm8::GameClassKinds) == 0x4FDD18, "Invalid \"mm8::GameClassKinds\" structure size");
	static_assert(offsetof(mm8::GameClassKinds, startingSkills) == 5233632);
}

#pragma pack(pop)