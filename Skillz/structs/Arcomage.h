#pragma once
#include "pch.h"
#include "main.h"

#pragma pack(push, 1)

namespace mm8
{
	struct ArcomagePlayer // size: 0xBC
	{
		std::array<char, 32> name; // fixed size string, requires null terminator | 0x0 (0 decimal)
		bool human; // 0x20 (32 decimal)
		SKIP(3);
		int32_t tower; // 0x24 (36 decimal)
		int32_t wall; // 0x28 (40 decimal)
		std::array<std::array<uint8_t, 4>, 3> income; // Unknown type | 0x2C (44 decimal)
		std::array<int32_t, 3> res; // 0x38 (56 decimal)
		std::array<int32_t, 10> cards; // 0x44 (68 decimal)
		std::array<std::array<int32_t, 2>, 10> unk; // 0x6C (108 decimal)
	};
	static_assert(sizeof(mm8::ArcomagePlayer) == 0xBC, "Invalid \"mm8::ArcomagePlayer\" structure size");
	static_assert(offsetof(mm8::ArcomagePlayer, human) == 32);
	static_assert(offsetof(mm8::ArcomagePlayer, tower) == 36);
	static_assert(offsetof(mm8::ArcomagePlayer, wall) == 40);
	static_assert(offsetof(mm8::ArcomagePlayer, income) == 44);
	static_assert(offsetof(mm8::ArcomagePlayer, res) == 56);
	static_assert(offsetof(mm8::ArcomagePlayer, cards) == 68);
	static_assert(offsetof(mm8::ArcomagePlayer, unk) == 108);



	struct Arcomage // size: 0x516D68
	{
		SKIP(5177892);
		int32_t AI; // 0x4F0224 (5177892 decimal)
		SKIP(11020);
		int32_t startingTower; // 0x4F2D34 (5188916 decimal)
		int32_t startingWall; // 0x4F2D38 (5188920 decimal)
		std::array<std::array<uint8_t, 4>, 3> startingIncome; // Unknown type | 0x4F2D3C (5188924 decimal)
		std::array<uint8_t, 4> cardsCount; // Unknown type | 0x4F2D48 (5188936 decimal)
		std::array<int32_t, 3> minIncome; // 0x4F2D4C (5188940 decimal)
		int32_t towerToWin; // 0x4F2D58 (5188952 decimal)
		int32_t resToWin; // 0x4F2D5C (5188956 decimal)
		SKIP(147072);
		std::array<mm8::ArcomagePlayer, 2> players; // 0x516BE0 (5336032 decimal)
		SKIP(4);
		std::array<int32_t, 3> startingRes; // 0x516D5C (5336412 decimal)
	};
	static_assert(sizeof(mm8::Arcomage) == 0x516D68, "Invalid \"mm8::Arcomage\" structure size");
	static_assert(offsetof(mm8::Arcomage, AI) == 5177892);
	static_assert(offsetof(mm8::Arcomage, startingTower) == 5188916);
	static_assert(offsetof(mm8::Arcomage, startingWall) == 5188920);
	static_assert(offsetof(mm8::Arcomage, startingIncome) == 5188924);
	static_assert(offsetof(mm8::Arcomage, cardsCount) == 5188936);
	static_assert(offsetof(mm8::Arcomage, minIncome) == 5188940);
	static_assert(offsetof(mm8::Arcomage, towerToWin) == 5188952);
	static_assert(offsetof(mm8::Arcomage, resToWin) == 5188956);
	static_assert(offsetof(mm8::Arcomage, players) == 5336032);
	static_assert(offsetof(mm8::Arcomage, startingRes) == 5336412);
}

#pragma pack(pop)