#pragma once
#include "pch.h"
#include "main.h"
#include "Player.h"
#include "Common.h"
#include "GameDataStructs.h"

#pragma pack(push, 1)

namespace mm8
{
	struct NPC // size: 0x4C
	{
		union
		{
			uint32_t exist; // 4-byte boolean | 0x0 (0 decimal)
			char* name; // EditPChar | 0x0 (0 decimal)
		};
		int32_t pic; // 0x4 (4 decimal)
		union
		{
			uint32_t bits; // 0x8 (8 decimal)
			struct
			{
				bool bribedBefore : 1; // 0x8 (8 decimal), bit index 7
				bool talkedBefore : 1; // 0x8 (8 decimal), bit index 6
				bool threatenedBefore : 1; // 0x8 (8 decimal), bit index 5
				SKIPBITS(4);
				bool hired : 1; // 0x9 (9 decimal), bit index 0
			};
		};
		int32_t fame; // 0xC (12 decimal)
		int32_t rep; // 0x10 (16 decimal)
		int32_t house; // 0x14 (20 decimal)
		int32_t profession; // 0x18 (24 decimal)
		int32_t greet; // 0x1C (28 decimal)
		int32_t joins; // 0x20 (32 decimal)
		int32_t tellsNews; // 0x24 (36 decimal)
		union
		{
			std::array<int32_t, 6> events; // 0x28 (40 decimal)
			struct
			{
				int32_t eventA; // 0x28 (40 decimal)
				int32_t eventB; // 0x2C (44 decimal)
				int32_t eventC; // 0x30 (48 decimal)
				int32_t eventD; // 0x34 (52 decimal)
				int32_t eventE; // 0x38 (56 decimal)
				int32_t eventF; // 0x3C (60 decimal)
			};
		};
		int32_t sex; // 0x40 (64 decimal)
		int32_t usedSpell; // 0x44 (68 decimal)
		int32_t newsTopic; // 0x48 (72 decimal)
	};
	static_assert(sizeof(mm8::NPC) == 0x4C, "Invalid \"mm8::NPC\" structure size");
	static_assert(offsetof(mm8::NPC, pic) == 4);
	static_assert(offsetof(mm8::NPC, bits) == 8);
	static_assert(offsetof(mm8::NPC, fame) == 12);
	static_assert(offsetof(mm8::NPC, rep) == 16);
	static_assert(offsetof(mm8::NPC, house) == 20);
	static_assert(offsetof(mm8::NPC, profession) == 24);
	static_assert(offsetof(mm8::NPC, greet) == 28);
	static_assert(offsetof(mm8::NPC, joins) == 32);
	static_assert(offsetof(mm8::NPC, tellsNews) == 36);
	static_assert(offsetof(mm8::NPC, events) == 40);
	static_assert(offsetof(mm8::NPC, eventA) == 40);
	static_assert(offsetof(mm8::NPC, eventB) == 44);
	static_assert(offsetof(mm8::NPC, eventC) == 48);
	static_assert(offsetof(mm8::NPC, eventD) == 52);
	static_assert(offsetof(mm8::NPC, eventE) == 56);
	static_assert(offsetof(mm8::NPC, eventF) == 60);
	static_assert(offsetof(mm8::NPC, sex) == 64);
	static_assert(offsetof(mm8::NPC, usedSpell) == 68);
	static_assert(offsetof(mm8::NPC, newsTopic) == 72);



	struct GameParty // size: 0xBB2EF8
	{
		SKIP(5346128);
		std::array<uint8_t, 4> currentPlayer; // Unknown type | 0x519350 (5346128 decimal)
		SKIP(6323056);
		int64_t lastRegenerationTime; // 0xB20EC4 (11669188 decimal)
		union
		{
			std::array<int64_t, 5> monsHuntReset; // 0xB20ECC (11669196 decimal)
			std::array<int64_t, 5> nextBountyHunt; // 0xB20ECC (11669196 decimal)
		};
		SKIP(1240);
		std::array<int64_t, 29> history; // MMExt: 1..29, here 0..28 | 0xB213CC (11670476 decimal)
		std::array<int64_t, 20> specialDates; // MMExt: 1..20, here 0..19 | 0xB214B4 (11670708 decimal)
		union
		{
			std::array<int32_t, 3> pos; // 0xB21554 (11670868 decimal)
			struct
			{
				int32_t x; // 0xB21554 (11670868 decimal)
				int32_t y; // 0xB21558 (11670872 decimal)
				int32_t z; // 0xB2155C (11670876 decimal)
			};
		};
		int32_t direction; // 0xB21560 (11670880 decimal)
		int32_t lookAngle; // 0xB21564 (11670884 decimal)
		int32_t lastX; // 0xB21568 (11670888 decimal)
		int32_t lastY; // 0xB2156C (11670892 decimal)
		int32_t lastZ; // 0xB21570 (11670896 decimal)
		int32_t lastDirection; // 0xB21574 (11670900 decimal)
		int32_t lastLookAngle; // 0xB21578 (11670904 decimal)
		int32_t lastEyeLevel; // 0xB2157C (11670908 decimal)
		int32_t speedX; // 0xB21580 (11670912 decimal)
		int32_t speedY; // 0xB21584 (11670916 decimal)
		int32_t speedZ; // 0xB21588 (11670920 decimal)
		SKIP(4);
		int32_t stableZ; // 0xB21590 (11670928 decimal)
		int32_t lastStandFacet; // 0xB21594 (11670932 decimal)
		SKIP(8);
		int32_t fallStartZ; // 0xB215A0 (11670944 decimal)
		uint32_t flying; // 4-byte boolean | 0xB215A4 (11670948 decimal)
		SKIP(32);
		int32_t food; // 0xB215C8 (11670984 decimal)
		SKIP(8);
		int32_t gold; // 0xB215D4 (11670996 decimal)
		int32_t bankGold; // 0xB215D8 (11671000 decimal)
		int32_t deaths; // 0xB215DC (11671004 decimal)
		SKIP(4);
		union
		{
			int32_t prisonTerms; // 0xB215E4 (11671012 decimal)
			int32_t pritsonTerms; // 0xB215E4 (11671012 decimal)
		};
		int32_t bountiesCollected; // 0xB215E8 (11671016 decimal)
		SKIP(4);
		union
		{
			std::array<int16_t, 5> bountyHuntTarget; // 0xB215F0 (11671024 decimal)
			std::array<int16_t, 5> monsHuntTarget; // 0xB215F0 (11671024 decimal)
		};
		union
		{
			std::array<uint16_t, 5> bountyHuntKilled; // 2-byte boolean | 0xB215FA (11671034 decimal)
			std::array<int16_t, 5> monsHuntKilled; // 0xB215FA (11671034 decimal)
		};
		SKIP(11);
		union
		{
			std::array<uint8_t, 216> QBits; // MMExt: 1..1734, here 0..1733 | array of abits (real index = 7 - usual) | 0xB2160F (11671055 decimal)
			struct
			{
				SKIP(64);
				std::array<bool, 16> arcomageWins; // MMExt: 107..122, here 0..15 | 0xB2164F (11671119 decimal)
				int8_t inArenaQuest; // 0xB2165F (11671135 decimal)
				int8_t arenaWinsPage; // 0xB21660 (11671136 decimal)
				int8_t arenaWinsSquire; // 0xB21661 (11671137 decimal)
				int8_t arenaWinsKnight; // 0xB21662 (11671138 decimal)
				int8_t arenaWinsLord; // 0xB21663 (11671139 decimal)
				std::array<bool, 68> artifactsFound; // MMExt: 500..567, here 0..67 | 0xB21664 (11671140 decimal)
				union
				{
					std::array<uint8_t, 87> autonotesBits; // MMExt: 1..697, here 0..696 | array of abits (real index = 7 - usual) | 0xB216A8 (11671208 decimal)
					struct
					{
						// uint8_t* reputation; // real size is 0 | Unknown type | 0xB216A8 (11671208 decimal)
					};
				};
			};
		};
		SKIP(57);
		std::array<mm8::SpellBuff, 20> spellBuffs; // 0xB21738 (11671352 decimal)
		SKIP(4);
		std::array<mm8::Player, 50> playersArray; // 0xB2187C (11671676 decimal)
		union
		{
			std::array<std::array<uint8_t, 4>, 5> players; // size field offset is 0xB7CA60 | Unknown type | 0xB7CA4C (12044876 decimal)
			std::array<int32_t, 5> playersIndexes; // 0xB7CA4C (12044876 decimal)
		};
		uint32_t players_size; // 0xB7CA60 (12044896 decimal)
		SKIP(36);
		union
		{
			int32_t stateBits; // 0xB7CA88 (12044936 decimal)
			struct
			{
				SKIPBITS(1);
				bool needRender : 1; // 0xB7CA88 (12044936 decimal), bit index 6
				bool drowning : 1; // 0xB7CA88 (12044936 decimal), bit index 5
				bool inAir : 1; // 0xB7CA88 (12044936 decimal), bit index 4
				bool enemyDetectorRed : 1; // 0xB7CA88 (12044936 decimal), bit index 3
				bool enemyDetectorYellow : 1; // 0xB7CA89 (12044937 decimal), bit index 2
				bool flyingBit : 1; // 0xB7CA89 (12044937 decimal), bit index 1
				bool waterWalkingBit : 1; // 0xB7CA89 (12044937 decimal), bit index 0
				bool inJumpSpell : 1; // 0xB7CA89 (12044937 decimal), bit index 7
				bool inLava : 1; // 0xB7CA89 (12044937 decimal), bit index 6
			};
		};
		SKIP(222312);
		int32_t fine; // 0xBB2EF4 (12267252 decimal)
	};
	static_assert(sizeof(mm8::GameParty) == 0xBB2EF8, "Invalid \"mm8::GameParty\" structure size");
	static_assert(offsetof(mm8::GameParty, currentPlayer) == 5346128);
	static_assert(offsetof(mm8::GameParty, lastRegenerationTime) == 11669188);
	static_assert(offsetof(mm8::GameParty, monsHuntReset) == 11669196);
	static_assert(offsetof(mm8::GameParty, nextBountyHunt) == 11669196);
	static_assert(offsetof(mm8::GameParty, history) == 11670476);
	static_assert(offsetof(mm8::GameParty, specialDates) == 11670708);
	static_assert(offsetof(mm8::GameParty, pos) == 11670868);
	static_assert(offsetof(mm8::GameParty, x) == 11670868);
	static_assert(offsetof(mm8::GameParty, y) == 11670872);
	static_assert(offsetof(mm8::GameParty, z) == 11670876);
	static_assert(offsetof(mm8::GameParty, direction) == 11670880);
	static_assert(offsetof(mm8::GameParty, lookAngle) == 11670884);
	static_assert(offsetof(mm8::GameParty, lastX) == 11670888);
	static_assert(offsetof(mm8::GameParty, lastY) == 11670892);
	static_assert(offsetof(mm8::GameParty, lastZ) == 11670896);
	static_assert(offsetof(mm8::GameParty, lastDirection) == 11670900);
	static_assert(offsetof(mm8::GameParty, lastLookAngle) == 11670904);
	static_assert(offsetof(mm8::GameParty, lastEyeLevel) == 11670908);
	static_assert(offsetof(mm8::GameParty, speedX) == 11670912);
	static_assert(offsetof(mm8::GameParty, speedY) == 11670916);
	static_assert(offsetof(mm8::GameParty, speedZ) == 11670920);
	static_assert(offsetof(mm8::GameParty, stableZ) == 11670928);
	static_assert(offsetof(mm8::GameParty, lastStandFacet) == 11670932);
	static_assert(offsetof(mm8::GameParty, fallStartZ) == 11670944);
	static_assert(offsetof(mm8::GameParty, flying) == 11670948);
	static_assert(offsetof(mm8::GameParty, food) == 11670984);
	static_assert(offsetof(mm8::GameParty, gold) == 11670996);
	static_assert(offsetof(mm8::GameParty, bankGold) == 11671000);
	static_assert(offsetof(mm8::GameParty, deaths) == 11671004);
	static_assert(offsetof(mm8::GameParty, prisonTerms) == 11671012);
	static_assert(offsetof(mm8::GameParty, pritsonTerms) == 11671012);
	static_assert(offsetof(mm8::GameParty, bountiesCollected) == 11671016);
	static_assert(offsetof(mm8::GameParty, bountyHuntTarget) == 11671024);
	static_assert(offsetof(mm8::GameParty, monsHuntTarget) == 11671024);
	static_assert(offsetof(mm8::GameParty, bountyHuntKilled) == 11671034);
	static_assert(offsetof(mm8::GameParty, monsHuntKilled) == 11671034);
	static_assert(offsetof(mm8::GameParty, arcomageWins) == 11671119);
	static_assert(offsetof(mm8::GameParty, inArenaQuest) == 11671135);
	static_assert(offsetof(mm8::GameParty, arenaWinsPage) == 11671136);
	static_assert(offsetof(mm8::GameParty, arenaWinsSquire) == 11671137);
	static_assert(offsetof(mm8::GameParty, arenaWinsKnight) == 11671138);
	static_assert(offsetof(mm8::GameParty, arenaWinsLord) == 11671139);
	static_assert(offsetof(mm8::GameParty, artifactsFound) == 11671140);
	static_assert(offsetof(mm8::GameParty, spellBuffs) == 11671352);
	static_assert(offsetof(mm8::GameParty, playersArray) == 11671676);
	static_assert(offsetof(mm8::GameParty, players) == 12044876);
	static_assert(offsetof(mm8::GameParty, playersIndexes) == 12044876);
	static_assert(offsetof(mm8::GameParty, players_size) == 12044896);
	static_assert(offsetof(mm8::GameParty, stateBits) == 12044936);
	static_assert(offsetof(mm8::GameParty, fine) == 12267252);



	struct Button // size: 0xBC
	{
		int32_t left; // 0x0 (0 decimal)
		int32_t top; // 0x4 (4 decimal)
		int32_t width; // 0x8 (8 decimal)
		int32_t height; // 0xC (12 decimal)
		int32_t right; // 0x10 (16 decimal)
		int32_t bottom; // 0x14 (20 decimal)
		int32_t shape; // 0x18 (24 decimal)
		int32_t hintAction; // 0x1C (28 decimal)
		int32_t actionType; // 0x20 (32 decimal)
		int32_t actionParam; // 0x24 (36 decimal)
		int32_t actionParam2; // 0x28 (40 decimal)
		uint32_t pressed; // 4-byte boolean | 0x2C (44 decimal)
		uint32_t prevItemPtr; // 0x30 (48 decimal)
		uint32_t nextItemPtr; // 0x34 (52 decimal)
		union
		{
			mm8::Dlg* parent; // 0x38 (56 decimal)
			uint32_t parentPtr; // 0x38 (56 decimal)
		};
		std::array<int32_t, 5> sprites; // size field offset is 0x50 | 0x3C (60 decimal)
		uint32_t sprites_size; // 0x50 (80 decimal)
		uint8_t shortCut; // 0x54 (84 decimal)
		std::array<char, 103> hint; // fixed size string, requires null terminator | 0x55 (85 decimal)
	};
	static_assert(sizeof(mm8::Button) == 0xBC, "Invalid \"mm8::Button\" structure size");
	static_assert(offsetof(mm8::Button, top) == 4);
	static_assert(offsetof(mm8::Button, width) == 8);
	static_assert(offsetof(mm8::Button, height) == 12);
	static_assert(offsetof(mm8::Button, right) == 16);
	static_assert(offsetof(mm8::Button, bottom) == 20);
	static_assert(offsetof(mm8::Button, shape) == 24);
	static_assert(offsetof(mm8::Button, hintAction) == 28);
	static_assert(offsetof(mm8::Button, actionType) == 32);
	static_assert(offsetof(mm8::Button, actionParam) == 36);
	static_assert(offsetof(mm8::Button, actionParam2) == 40);
	static_assert(offsetof(mm8::Button, pressed) == 44);
	static_assert(offsetof(mm8::Button, prevItemPtr) == 48);
	static_assert(offsetof(mm8::Button, nextItemPtr) == 52);
	static_assert(offsetof(mm8::Button, parent) == 56);
	static_assert(offsetof(mm8::Button, parentPtr) == 56);
	static_assert(offsetof(mm8::Button, sprites) == 60);
	static_assert(offsetof(mm8::Button, sprites_size) == 80);
	static_assert(offsetof(mm8::Button, shortCut) == 84);
	static_assert(offsetof(mm8::Button, hint) == 85);
}

#pragma pack(pop)