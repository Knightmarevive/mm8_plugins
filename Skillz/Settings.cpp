// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"
#include "hook.hpp"

#include "structs/Player.h"
#include "structs/MergeSpecific.h"
// auto portraits = (mm8::CharacterPortrait*) *(int*)(0x6002BFCC+1);// (0x00400000 + 0x45E5E60);
mm8::CharacterPortrait* portraits = nullptr;

extern "C" __declspec(dllexport) int __cdecl initPortraits(int addr) {
    portraits = (mm8::CharacterPortrait*)addr;
    return addr;
}

#include <vector>
#include <fstream>
#include <sstream>

/*
extern "C" {
#include "../lua-5.4.6/src/lua.h" 
#include "../lua-5.4.6/src/lauxlib.h"
#include "../lua-5.4.6/src/lualib.h"
}
*/

typedef char* char_ptr;
constexpr int new_skill_count = 128;
constexpr int old_skill_count = 39;
constexpr int max_clases = 128;
constexpr int max_races = 128;


extern char_ptr *mastery_name;
extern "C" __declspec(dllexport) int32_t __cdecl PlayerID(mm8::Player * player);

char ClassSkillzMasteries[max_clases][new_skill_count] = {};
char_ptr zclassNames[max_clases] = {};
char_ptr zskillNames[new_skill_count] = {};
char_ptr zraceNames[max_clases] = {};

void smart_strcpy(char_ptr& to, char_ptr from);

char byte_4FDBE0[max_clases * new_skill_count] = {};

int getClassID(char* name) {
    if (*name == 0) return -1;

    if (*name >= '0' && *name <= '9')
        return atoi(name);

    // int answer = -1;
    for (int i = 0; i < max_clases; ++i) {
        if (zclassNames[i] == 0)
            return -1;
        if (!strcmp(name, zclassNames[i]))
            return i;
    }

    return -1; // answer;
}

int getRaceID(char* name) {
    if (*name == 0) return -1;

    if (*name >= '0' && *name <= '9')
        return atoi(name);

    // int answer = -1;
    for (int i = 0; i < max_races; ++i) {
        if (zraceNames[i] == 0)
            return -1;
        if (!strcmp(name, zraceNames[i]))
            return i;
    }

    return -1; // answer;
}

extern "C" __declspec(dllexport) char_ptr __cdecl getName(int skill_ID);
int getSkillID(char* name) {
    if (*name == 0) return -1;

    if (*name >= '0' && *name <= '9')
        return atoi(name);

    // int answer = -1;
    for (int i = 0; i < new_skill_count; ++i) {
        if (zskillNames[i] && !strcmp(name, zskillNames[i]))
            return i;
        if (getName(i) && !strcmp(name, getName(i)))
            return i;
    }

    return -1; // answer;
}

struct modifier {
    int8_t max, min, mod, unk;
    modifier() { max = 8; min = 0; mod = 0; unk = 0; };
    operator bool() { return (max != 8 || min != 0 || mod != 0); };
    int8_t operator()(int8_t arg) {
        int8_t ret = arg + mod;
        if (ret > max) ret = max;
        if (ret < min) ret = min;
        return ret;
    };
    modifier(char* arg) : modifier() {
        for (auto i = arg; *i;++i) {
            switch (*i) {
            case 'N': min = 1; break;
            case 'E': min = 2; break;
            case 'M': min = 3; break;
            case 'G': min = 4; break;
            case 'S': min = 5; break;
            case 'U': min = 6; break;
            case 'A': min = 7; break;
            case 'D': min = 8; break;


            case '0': max = 0; break;
            case 'n': max = 1; break;
            case 'e': max = 2; break;
            case 'm': max = 3; break;
            case 'g': max = 4; break;
            case 's': max = 5; break;
            case 'u': max = 6; break;
            case 'a': max = 7; break;
            case 'd': max = 8; break;

            case '-': --mod; break;
            case '+': ++mod; break;
            }
        }
    }
    void out(std::ostream& o) {
        if (*this) {
            char min_token[256] = "_NEMGSUAD";
            if (min) o.put(min_token[min]);
            char max_token[256] = "0nemgsuad";
            o.put(max_token[max]);
            if (mod > 0) for (int i = 0; i < mod; ++i) o.put('+');
            if (mod < 0) for (int i = 0; i > mod; --i) o.put('-');
        }
        else o.put('=');
    }
};

modifier RaceSkillzMasteries[max_races][new_skill_count] = {};
modifier ExtraSkillzMasteries[max_races][max_clases][new_skill_count] = {};

struct class_extra {
    int kind, step;
    char_ptr note = nullptr;
};
class_extra class_extra_table[max_clases];
bool read_class_extra_table() {
    std::ifstream is("Data\\Tables\\Class Extra.txt");
    char buf[4096]; int tmp = 0; int j = 0;
    is.getline(buf, 4096);
    while (!is.eof()) {
        is.getline(buf, 4096);
        std::stringstream row(buf);
        row >> tmp; if (tmp != j) return false;
        row >> class_extra_table[j].kind;       
        row >> class_extra_table[j].step;
        row.getline(buf, 4096);
        smart_strcpy(class_extra_table[j].note, buf);
        ++j;
    }
    if (!j) return false;

    return true;
}
int next_class(int current) {
    int kind = class_extra_table[current].kind;
    int answer = current;
    for (int i = current; kind == class_extra_table[i].kind; ++i)
        answer = i;

    return answer;
}

void write_race_skills(std::ofstream& RaceSkillsTxt, long races) {
    for (int i = 0; i < races; ++i) {
        RaceSkillsTxt.put('\t');
        RaceSkillsTxt << zraceNames[i];
    }
    RaceSkillsTxt.put('\n');

    for (int i = 0; i < new_skill_count; ++i) {
        if (!zskillNames[i] || !zskillNames[i][0]) {
            smart_strcpy(zskillNames[i], (char_ptr)"Skill");
            _itoa_s(i, zskillNames[i] + 5, 32, 10);
        }
        RaceSkillsTxt << zskillNames[i];
        for (int j = 0; j < races; ++j) {
            RaceSkillsTxt.put('\t');
            RaceSkillzMasteries[j][i].out(RaceSkillsTxt);
        }
        RaceSkillsTxt.put('\n');
    }
}

bool read_race_skills(std::ifstream& RaceSkillsTxt, long& races) {
    char buf[65536];
    if (RaceSkillsTxt.get() == '\t') {
        RaceSkillsTxt.getline(buf, sizeof(buf));
        std::stringstream header(buf);
        while (!header.eof()) {
            header.getline(buf, 512, '\t');
            // if (strstr(buf, "-")) break;
            smart_strcpy(zraceNames[races], (char_ptr)buf);
            ++races;
        }
        for (int j = 0; j < old_skill_count; ++j) {
            RaceSkillsTxt.getline(buf, sizeof(buf));
            std::stringstream row(buf);
            row.getline(buf, 512, '\t');
            for (int i = 0; i < races; ++i) {
                char tmp[8];
                row.getline(tmp, 8, '\t');
                modifier answer(tmp);

                RaceSkillzMasteries[i][j] = answer;
            }
        }

        return true;
    }
    else return false;
}

bool read_race_skills_legacy(std::ifstream& RaceSkillsTxt, long& races) {
    char buf[65536];
    if (RaceSkillsTxt.get() == '\t') {
        RaceSkillsTxt.getline(buf, sizeof(buf));
        std::stringstream header(buf);
        while (!header.eof()) {
            header.getline(buf, 512, '\t');
            if (strstr(buf, "-")) break;
            smart_strcpy(zraceNames[races], (char_ptr)buf);
            ++races;
        }
        for (int j=0; j < old_skill_count; ++j) {
            RaceSkillsTxt.getline(buf, sizeof(buf));
            std::stringstream row(buf);
            row.getline(buf, 512, '\t');
            for (int i = 0; i < races; ++i) {
                modifier answer{};
                char tmp[8];
                row.getline(tmp, 8, '\t');
                switch (*tmp)
                {
                case 'N': answer.min = 1; break;
                case 'B': answer.min = 1; break;
                case 'E': answer.min = 2; break;
                case 'M': answer.min = 3; break;
                case 'G': answer.min = 4; break;
                }

                RaceSkillzMasteries[i][j] = answer;
            }
        }
        return true;
    }
    else return false;
}

bool read_class_skills(std::ifstream& ClassSkillsTxt, long& classes, long& skills, bool legacy = false) {
    char buf[65536];
    if (ClassSkillsTxt.get() == '\t') {
        ClassSkillsTxt.getline(buf, sizeof(buf));
        std::stringstream header(buf);
        while (!header.eof()) {
            smart_strcpy(zclassNames[classes], (char_ptr)"");
            header.getline(zclassNames[classes], 512, '\t');
            ++classes;
        }
        for (; skills < new_skill_count; ++skills) {
            ClassSkillsTxt.getline(buf, sizeof(buf));
            std::stringstream row(buf);
            smart_strcpy(zskillNames[skills], (char_ptr)"");
            row.getline(zskillNames[skills], 512, '\t');
            for (int i = 0; i < classes; ++i) {
                char answer = 0; char tmp[8];
                row.getline(tmp, 8, '\t');
                if(legacy)
                switch (*tmp)
                {
                case 'B': answer = 1; break;
                case 'N': answer = 1; break;
                case 'E': answer = 2; break;
                case 'M': answer = 3; break;
                case 'G': answer = 4; break;
                }
                else switch (*tmp)
                {
                case 'B': answer = 1; break;
                case 'N': answer = 1; break;
                case 'E': answer = 2; break;
                case 'M': answer = 3; break;
                case 'G': answer = 4; break;
                case 'S': answer = 5; break;
                case 'U': answer = 6; break;
                case 'A': answer = 7; break;
                case 'D': answer = 8; break;
                }

                ClassSkillzMasteries[i][skills] = answer;
            }
        }
        return true;
    }
    else return false;
}

char_ptr zStartClassNames[max_clases];
bool read_class_start_skills(std::ifstream& ClassStartSkillsTxt, long& classes) {
    char buf[65536]; char tmp[512]; long skills = 0;
    if (ClassStartSkillsTxt.get() == '\t') {
        ClassStartSkillsTxt.getline(buf, sizeof(buf));
        std::stringstream header(buf);
        while (!header.eof()) {
            smart_strcpy(zStartClassNames[classes], (char_ptr)"");
            header.getline(zStartClassNames[classes], 512, '\t');
            // header.getline(tmp, 512, '\t');
            ++classes;
        }
        for (; skills < new_skill_count; ++skills) {
            ClassStartSkillsTxt.getline(buf, sizeof(buf));
            std::stringstream row(buf);
            // smart_strcpy(zskillNames[skills], (char_ptr)"");
            // row.getline(zskillNames[skills], 512, '\t');
            row.getline(tmp, 512, '\t');
            for (int i = 0; i < classes; ++i) {
                char answer = 0; char tmp[8];
                row.getline(tmp, 8, '\t');
                
                    switch (*tmp)
                    {
                    case '-': case '0': answer = 0; break;
                    case 'C': case '1': answer = 1; break;
                    case 'F': case '2': answer = 2; break;
                    default: answer = 0;
                    }
                
                    byte_4FDBE0[i*new_skill_count+skills] = answer;
             }
        }
        return true;
    }
    else return false;
}

void write_class_starting_skills(std::ofstream& ClassStartingSkillsTxt, long classes, long skills) {
    for (int i = 0; i < classes; ++i) {
        ClassStartingSkillsTxt.put('\t');
        ClassStartingSkillsTxt << zStartClassNames[i];// zclassNames[i];
    }
    ClassStartingSkillsTxt.put('\n');
    for (int i = 0; i < skills; ++i) {
        /*
        if (!zskillNames[i] || !zskillNames[i][0]) {
            smart_strcpy(zskillNames[i], (char_ptr)"Skill");
            _itoa_s(i, zskillNames[i] + 5, 32, 10);
        }
        */
        ClassStartingSkillsTxt << zskillNames[i];
        for (int j = 0; j < classes; ++j) {
            char XLAT[256] = "0123456789";
            ClassStartingSkillsTxt.put('\t');
            ClassStartingSkillsTxt << XLAT[byte_4FDBE0[j*new_skill_count+i]];
        }
        ClassStartingSkillsTxt.put('\n');
    }
}


void write_class_skills(std::ofstream& ClassSkillsTxt, long classes, long skills) {
    for (int i = 0; i < classes; ++i) {
        ClassSkillsTxt.put('\t');
        ClassSkillsTxt << zclassNames[i];
    }
    ClassSkillsTxt.put('\n');
    for (int i = 0; i < skills; ++i) {
        if (!zskillNames[i] || !zskillNames[i][0]) {
            smart_strcpy(zskillNames[i],(char_ptr)"Skill");
            _itoa_s(i, zskillNames[i] + 5, 32, 10);
        }
        ClassSkillsTxt << zskillNames[i];
        for (int j = 0; j < classes; ++j) {
            char XLAT[256] = "=NAEMGSUD";
            ClassSkillsTxt.put('\t');
            ClassSkillsTxt << XLAT[ClassSkillzMasteries[j][i]];
        }
        ClassSkillsTxt.put('\n');
    }
}

bool read_extra_masteries() {
    char buf[4096]; int race = -1; int clas = -1; int skill = -1;
    std::ifstream is("Data\\Tables\\Extra Skillz.txt");
    is.getline(buf, 4096);
    if (strcmp(buf, "Race\t""Class\t""Skill\t""modifier"))
        return false;

    // if (is.eof() || is.bad()) return false;

    while (!is.eof()) {
        is.getline(buf, 4096);
        std::stringstream row(buf);
        row.getline(buf, 4096, '\t');
        race = getRaceID(buf);
        row.getline(buf, 4096, '\t');
        clas = getClassID(buf);
        row.getline(buf, 4096, '\t');
        skill = getSkillID(buf);

        row.getline(buf, 4096);
        if (race >= 0 && clas >= 0 && skill >= 0)
            ExtraSkillzMasteries[race][clas][skill] = buf;
    }

    return true;
}

void write_extra_masteries_header() {
    std::ofstream os("Data\\Tables\\Extra Skillz.txt");
    os << "Race\t""Class\t""Skill\t""modifier\n\n";
}

unsigned int __fastcall player_get_skill_48EF4F(int ecx, int edx, int a2);
void settings_init() {
    hook::make_hook((void*)0x0048EF4F, player_get_skill_48EF4F, true);

    auto MMExtCore_dll = GetModuleHandleA("mmextcore.dll");
    if(MMExtCore_dll && !portraits)
        portraits = (mm8::CharacterPortrait*)*(int*)
            (long(MMExtCore_dll) + 0x2BFCC + 1);
    else if(!portraits) {
        // portraits = (mm8::CharacterPortrait*) 0x06bf4718;
    }
    long classes = 0; long skills = 0; long races = 0;
    std::ifstream ClassSkillsTxt_default("Data\\Tables\\Class Skills.txt");
    std::ifstream ClassSkillsTxt_expanded("Data\\Tables\\Class Skillz.txt");
    
    if (read_class_skills(ClassSkillsTxt_expanded, classes, skills)) {

    }
    else if (read_class_skills(ClassSkillsTxt_default , classes, skills, true)) {
        std::ofstream ClassSkillsTxt_expanded("Data\\Tables\\Class Skillz.txt");
        write_class_skills(ClassSkillsTxt_expanded, classes, skills);
    }
    else MessageBoxA(0, "class SKILLZ table is missing", "SKILLZ", 0);

    std::ifstream RaceSkillsTxt_legacy("Data\\Tables\\Race Skills.txt");
    std::ifstream RaceSkillsTxt("Data\\Tables\\Race Skillz.txt");
    if (read_race_skills(RaceSkillsTxt, races)) {

    }
    else if (read_race_skills_legacy(RaceSkillsTxt_legacy, races)) {
        std::ofstream RaceSkillsTxt("Data\\Tables\\Race Skillz.txt");
        write_race_skills(RaceSkillsTxt, races);
    }
    else MessageBoxA(0, "race SKILLZ table is missing", "SKILLZ", 0);

    if(!read_class_extra_table())
        MessageBoxA(0, "class extra table is missing", "SKILLZ", 0);

    if (!read_extra_masteries())
        write_extra_masteries_header();

    long StartingClassses = 0;
    std::ifstream ClassStartSkillsTxt("Data\\Tables\\Class Starting Skillz.txt");
    if (read_class_start_skills(ClassStartSkillsTxt, StartingClassses)) {

    }
    else {
        long StartingClassses = 0;
        std::ifstream ClassStartSkillsTxt("Data\\Tables\\Class Starting Skills.txt");
        if (read_class_start_skills(ClassStartSkillsTxt, StartingClassses)) {
            std::ofstream ClassStartSkillsTxt("Data\\Tables\\Class Starting Skillz.txt");
            write_class_starting_skills(ClassStartSkillsTxt, StartingClassses,skills);
        }
        else {
            MessageBoxA(0, " \"Class Starting Skills.txt\" is missing", "SKILLZ", 0);
        }
    }

    /*
    hook::write_to_readonly((void*)0x00601B04, &mastery_name[1], 4);
    hook::write_to_readonly((void*)0x00601B08, &mastery_name[3], 4);
    hook::write_to_readonly((void*)0x00601B0C, &mastery_name[2], 4);

    hook::write_to_readonly((void*)0x006015C8, &mastery_name[4], 4);
    */
}

void init_settings_once() {
    if (!zclassNames[0] || !zclassNames[0][0]) settings_init();
}

/*
void processLua2DArray(lua_State* L) {
    // Check if the argument is a table
    if (!lua_istable(L, -1)) {
        luaL_error(L, "Argument must be a table");
        return;
    }

    // Get the number of rows
    int rows = lua_rawlen(L, -1);

    // Assuming all rows have the same number of columns
    if (rows > 0) {
        // Get the number of columns by examining the first row
        lua_rawgeti(L, -1, 1); // Get the first row
        int columns = lua_rawlen(L, -1);
        lua_pop(L, 1); // Pop the first row

        // Process the two-dimensional array
        for (int i = 1; i <= rows; ++i) {
            lua_rawgeti(L, -1, i); // Get the i-th row

            for (int j = 1; j <= columns; ++j) {
                lua_rawgeti(L, -1, j); // Get the j-th element of the row

                // Check the type of the element (assuming integers in this example)
                if (lua_isinteger(L, -1)) {
                    int value = lua_tointeger(L, -1);
                    // Do something with the value
                    // For example, print it
                    printf("Element (%d, %d): %d\n", i, j, value);
                }

                // Pop the element from the stack
                lua_pop(L, 1);
            }

            // Pop the row from the stack
            lua_pop(L, 1);
        }
    }
}
*/

extern "C" __declspec(dllexport) int32_t __cdecl MasteryLimit_raw(int race, int clas, int skill_ID) {

    int answer = ClassSkillzMasteries[clas][skill_ID];
    if (race >= 0) {
        if (ExtraSkillzMasteries[race][clas][skill_ID]) {
            answer = ExtraSkillzMasteries[race][clas][skill_ID](answer);
        }else answer = RaceSkillzMasteries[race][skill_ID](answer);
    }
    return answer;
}

extern "C" __declspec(dllexport) int32_t __cdecl MasteryLimit_set(int race, int clas, int skill_ID, char* token) {
    if (skill_ID<0 || skill_ID>= new_skill_count) return false;
    if (race < 0 && clas < 0) return false;

    if (race < 0) {
        switch (*token) {
        case '0': case'-': ClassSkillzMasteries[clas][skill_ID] = 0; return true;
        case 'B': case'b': ClassSkillzMasteries[clas][skill_ID] = 1; return true;
        case 'E': case'e': ClassSkillzMasteries[clas][skill_ID] = 2; return true;
        case 'M': case'm': ClassSkillzMasteries[clas][skill_ID] = 3; return true;
        case 'G': case'g': ClassSkillzMasteries[clas][skill_ID] = 4; return true;
        default: return false;
        }
    }

    modifier thing = token;
    if (clas < 0) {
        RaceSkillzMasteries[race][skill_ID] = thing;
        return true;
    }

    ExtraSkillzMasteries[race][clas][skill_ID] = thing;
    return true;
}


extern "C" __declspec(dllexport) int32_t __cdecl MasteryLimit(mm8::Player * player, int skill_ID) {
    if (!zclassNames[0] || !zclassNames[0][0]) settings_init();

	if (skill_ID <0 || skill_ID >= new_skill_count) {
		return 0;
	}
	if(PlayerID(player)<0) {
		return 0;
	}

    // int answer = ClassSkillzMasteries[player->clas][skill_ID];
    int faceID = player->face; 
    int raceID = portraits[faceID].race;
    int classID = player->clas;
    return MasteryLimit_raw(raceID, classID, skill_ID);
    /*
    if (faceID >= 77) {
        return 0;
    }
    */
    /*
    answer = RaceSkillzMasteries[portraits[faceID].race][skill_ID](answer);

	return answer;
    */
}

void __cdecl MasteryLimit_fill(char* v18, int v17, mm8::Player* player, int skill_ID) {
    if (!zclassNames[0] || !zclassNames[0][0]) settings_init();

    int faceID = player->face;
    int raceID = portraits[faceID].race;
    int classID = player->clas;

    int v6 = 0;
    do {
        char v8 = MasteryLimit_raw(raceID, next_class(classID), skill_ID);
        v18[v6 + 2] = 0;
        if(v8>=v17)
            v18[v6 + 2] = 1;
        ++v6;
    } while (v6 < 2);
}


extern "C" __declspec(dllexport) int32_t __cdecl get(mm8::Player * player, int skill_ID);
int __fastcall sub_48F54A(mm8::Player* _this) {
    return _this->clas;
}
int __fastcall sub_48F622(mm8::Player* _this, int dummy, int a2)
{
    int v2; // edi
    int i; // esi
    int v5; // ebx
    WORD* j; // esi
    int v7; // [esp+14h] [ebp+8h]
    int v8; // [esp+14h] [ebp+8h]

    init_settings_once();

    v2 = 0;
    if (a2 >= 0)
    {
        if (a2 <= 1)
        {
            for (i = 0; i < new_skill_count; ++i)
            {
                if (byte_4FDBE0[new_skill_count * (sub_48F54A(_this) / 2) + i] == 2)
                {
                    if (v2 == a2)
                        return i;
                    ++v2;
                }
            }
        }
        else
        {
            if (a2 <= 3)
            {
                v8 = a2 - 2;
                v5 = 0;
                for (int j = 0/*_this + 888*/; ; ++j)
                {
                    auto jj = get(_this, j);
                    if (jj && byte_4FDBE0[new_skill_count * (sub_48F54A(_this) / 2) + v2] == 1)
                    {
                        if (v5 == v8)
                            return v2;
                        ++v5;
                    }
                    if (++v2 >= new_skill_count)
                        return new_skill_count;
                }
            }
            if (a2 <= 12)
            {
                v7 = a2 - 4;
                for (i = 0; i < new_skill_count; ++i)
                {
                    if (byte_4FDBE0[new_skill_count * (sub_48F54A(_this) / 2) + i] == 1)
                    {
                        if (v2 == v7)
                            return i;
                        ++v2;
                    }
                }
            }
        }
    }
    return new_skill_count;
}


extern "C" __declspec(dllexport) void __cdecl set(mm8::Player * player, int skill_ID, int16_t value);
extern "C" __declspec(dllexport) int32_t __cdecl get(mm8::Player * player, int skill_ID);
extern "C" __declspec(dllexport) int32_t __cdecl CleanMastery(mm8::Player * player) {
    int answer = 0;
    if (!zclassNames[0] || !zclassNames[0][0]) settings_init();

    if (PlayerID(player) < 0) {
        return 0;
    }

    for (int skill_ID = 0; skill_ID < new_skill_count;++skill_ID) {
        if (MasteryLimit(player, skill_ID) == 0
            && get(player, skill_ID) != 0
            ) {
            ++answer;
            set(player, skill_ID, 0);
        }
    }
    return answer;
}