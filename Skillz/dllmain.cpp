// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"
#include "hook.hpp"

#include "structs/Player.h"

#include <vector>

typedef char* char_ptr;
constexpr int max_masteries = 19;
constexpr int new_skill_count = 128;
constexpr int old_skill_count = 39;
constexpr int menu_skill_offset = 0x7f000000;// 4096; // 36;

constexpr int hero_count = 128;

void refresh_mastery_names();
extern "C" __declspec(dllexport) const char* __cdecl thanks() {
    static const char* authors = "Plugin Skillz.dll expanding skill limits in MM&8"
        "\n\n structures headers made by eksekk"
        "\n\n further reverse engineering by Yuji Sakai"
        "\n coding and implementation by Yuji Sakai";
    return authors;
}
struct saved_stuff{
    int16_t skills[hero_count][new_skill_count - old_skill_count];
    uint8_t extra_mastery[hero_count][new_skill_count];
};
saved_stuff saved_data;
constexpr size_t saved_block_size = sizeof(saved_data);
// long skills_long = (long) skills;
long save_long = (long) &saved_data;

int push_time(void* fun) {
    static HMODULE module = nullptr;
    if(!module)    module = GetModuleHandleA("cave_api.dll");
    if (!module)   return 0;
    static auto impl = (int(*)(void*))nullptr;
    if (!impl) impl = (int(*)(void*)) GetProcAddress(module, "push_time");
    if (impl) return impl(fun); else return 0;
}
int push_start(void* fun) {
    static HMODULE module = nullptr;
    if (!module)    module = GetModuleHandleA("cave_api.dll");
    if (!module)   return 0;
    static auto impl = (int(*)(void*))nullptr;
    if (!impl) impl = (int(*)(void*)) GetProcAddress(module, "push_start");
    if (impl) return impl(fun); else return 0;
}
int push_save(void* fun) {
    static HMODULE module = nullptr;
    if (!module)    module = GetModuleHandleA("cave_api.dll");
    if (!module)   return 0;
    static auto impl = (int(*)(void*))nullptr;
    if (!impl) impl = (int(*)(void*)) GetProcAddress(module, "push_save");
    if (impl) return impl(fun); else return 0;
}
int push_load(void* fun) {
    static HMODULE module = nullptr;
    if (!module)    module = GetModuleHandleA("cave_api.dll");
    if (!module)   return 0;
    static auto impl = (int(*)(void*))nullptr;
    if (!impl) impl = (int(*)(void*)) GetProcAddress(module, "push_load");
    if (impl) return impl(fun); else return 0;
}

extern "C" __declspec(dllexport) int32_t __cdecl MasteryLimit(mm8::Player * player, int skill_ID);

extern "C" __declspec(dllexport) int32_t __cdecl PlayerID(mm8::Player * player) {
    long begin = 0x00B2187C;
    long distance = (long) player - begin;
    if (distance < 0) return -1;
    if (distance % sizeof(mm8::Player)) return -1;
    else return distance / sizeof(mm8::Player);
}

extern "C" __declspec(dllexport) int32_t __cdecl get(mm8::Player * player, int skill_ID) {
    int player_id = PlayerID(player);
    if (player_id < 0)
        return 0;
    if (skill_ID >= old_skill_count && skill_ID < new_skill_count) {
        return saved_data.skills[player_id][skill_ID - old_skill_count];
    }
    else if (skill_ID >= 0 && skill_ID < old_skill_count) {
        return player->skills[skill_ID];
    }

    return 0;
}

// Skill Descriptions
char_ptr mastery_name[max_masteries+1];
char_ptr new_descriptions[max_masteries+2][new_skill_count];
char_ptr *dword_5E4CB0 = new_descriptions[1]; long dword_5E4CB0_long = (long) dword_5E4CB0;
char_ptr *dword_5E4C10 = new_descriptions[2]; long dword_5E4C10_long = (long) dword_5E4C10;
char_ptr *dword_5E4B70 = new_descriptions[3]; long dword_5E4B70_long = (long) dword_5E4B70;
char_ptr *dword_5E4AD0 = new_descriptions[4]; long dword_5E4AD0_long = (long) dword_5E4AD0;
char_ptr *dword_5E4A30 = new_descriptions[5]; long dword_5E4A30_long = (long) dword_5E4A30;

char_ptr new_skill_names[new_skill_count - old_skill_count];
long old_skill_names_long = 0x00BB3060; // 0x601884;// 0x0361556C; 
char_ptr* old_skill_names = (char_ptr* ) old_skill_names_long;


__declspec(naked) long hook_00419872() {
    __asm {
        push eax
        push ecx

        push edx
        push edi
        call get
        add esp, 8
        movzx edx, ax

        pop ecx
        pop eax
        push 0x0041987A
        ret
    }
}
int32_t DialogLogic_MiscSkills_4F3938[new_skill_count];
void attach_DialogLogic_MiscSkills_4F3938() {
    memset(DialogLogic_MiscSkills_4F3938, 255, sizeof(DialogLogic_MiscSkills_4F3938));

    int32_t original[12] = {37,35,27,24,34,38,31,28,25,29,30,26};
    memcpy(DialogLogic_MiscSkills_4F3938, original, 12*4);

    long DialogLogic_MiscSkills_4F3938_long = (long)DialogLogic_MiscSkills_4F3938;
    // hook::write_to_readonly( (void*)(0x00418FE5 + 3), &DialogLogic_MiscSkills_4F3938_long, 4);
    hook::write_to_readonly( (void*)(0x0041985D + 3), &DialogLogic_MiscSkills_4F3938_long, 4);
    // hook::write_to_readonly( (void*)(0x00419E74 + 4), &DialogLogic_MiscSkills_4F3938_long, 4);

    long after_MiscSkills = DialogLogic_MiscSkills_4F3938_long + 12 * 4;
    hook::write_to_readonly((void*)(0x00419AD5 + 3), &after_MiscSkills, 4);

    hook::make_hook( (void*) 0x00419872,hook_00419872);
}
extern "C" __declspec(dllexport) void __cdecl new_misc(int SkillID) {
    auto i = DialogLogic_MiscSkills_4F3938;
    while (*++i >= 0) {
        if (*i == SkillID) return;
    }   *i++ = SkillID;
    hook::write_to_readonly((void*)(0x00419AD5 + 3), &i, 4);
}

__declspec(naked) long hook_00418D82() {
    __asm {
        push eax
        push ecx

        push edx
        push edi
        call get
        add esp, 8
        movzx edx, ax

        pop ecx
        pop eax
        push 0x00418D8A
        ret
    }
}
int32_t DialogLogic_WeaponSkills_4F3914[new_skill_count];
void attach_DialogLogic_WeaponSkills_4F3914() {
    memset(DialogLogic_WeaponSkills_4F3914, 255, sizeof(DialogLogic_WeaponSkills_4F3914));

    int32_t original[9] = { 3,5,2,6,4,0,1,33,7 };
    memcpy(DialogLogic_WeaponSkills_4F3914, original, 9 * 4);

    long DialogLogic_WeaponSkills_4F3914_long = (long)DialogLogic_WeaponSkills_4F3914;
    hook::write_to_readonly((void*)(0x00418D6D + 3), &DialogLogic_WeaponSkills_4F3914_long, 4);
    // hook::write_to_readonly((void*)(0x00419732 + 3), &DialogLogic_WeaponSkills_4F3914_long, 4);
    hook::write_to_readonly((void*)(0x00419E47 + 4), &DialogLogic_WeaponSkills_4F3914_long, 4);
    // hook::write_to_readonly((void*)(0x00419F40 + 4), &DialogLogic_WeaponSkills_4F3914_long, 4);

    long after_WeaponSkills = DialogLogic_WeaponSkills_4F3914_long + 9 * 4;
    hook::write_to_readonly((void*)(0x00418FE5 + 3), &after_WeaponSkills, 4);
    hook::write_to_readonly((void*)(0x00419E74 + 4), &after_WeaponSkills, 4);

    hook::make_hook((void*)0x00418D82, hook_00418D82);
}
extern "C" __declspec(dllexport) void __cdecl new_weapon(int SkillID) {
    auto i = DialogLogic_WeaponSkills_4F3914;
    while (*++i >= 0) {
        if (*i == SkillID) return;
    }   *i++ = SkillID;
    hook::write_to_readonly((void*)(0x00418FE5 + 3), &i, 4);
    hook::write_to_readonly((void*)(0x00419E74 + 4), &i, 4);
}


__declspec(naked)long hook_004194CF() {
    __asm {
        push eax
        push ecx

        push edx
        push edi
        call get
        add esp, 8
        movzx edx, ax

        pop ecx
        pop eax
        push 0x004194D7
        ret
    }
}
int32_t DialogLogic_ArmorSkills_4F3900[new_skill_count];
void attach_DialogLogic_ArmorSkills_4F3900() {
    memset(DialogLogic_ArmorSkills_4F3900, 255, sizeof(DialogLogic_ArmorSkills_4F3900));

    int32_t original[5] = { 9, 10, 11, 8, 32 };
    memcpy(DialogLogic_ArmorSkills_4F3900, original, 5 * 4);

    long DialogLogic_ArmorSkills_4F3900_long = (long)DialogLogic_ArmorSkills_4F3900;
    hook::write_to_readonly((void*)(0x004194BA + 3), &DialogLogic_ArmorSkills_4F3900_long, 4);
    hook::write_to_readonly((void*)(0x00419F13 + 4), &DialogLogic_ArmorSkills_4F3900_long, 4);


    long after_ArmorSkills = DialogLogic_ArmorSkills_4F3900_long + 5 * 4;
    hook::write_to_readonly((void*)(0x00419F40 + 4), &after_ArmorSkills, 4);
    hook::write_to_readonly((void*)(0x00419732 + 3), &after_ArmorSkills, 4);

    hook::make_hook((void*)0x004194CF, hook_004194CF);
}
extern "C" __declspec(dllexport) void __cdecl new_armor(int SkillID) {
    auto i = DialogLogic_ArmorSkills_4F3900;
    while (*++i >= 0) {
        if (*i == SkillID) return;
    }   *i++ = SkillID;
    hook::write_to_readonly((void*)(0x00419F40 + 4), &i, 4);
    hook::write_to_readonly((void*)(0x00419732 + 3), &i, 4);
}


__declspec(naked) long hook_00419125() {
    __asm {
        push eax
        push ecx

        push edx
        push edi
        call get
        add esp, 8
        movzx edx, ax

        pop ecx
        pop eax
        push 0x0041912D
        ret
    }
}
int32_t DialogLogic_MagicSkills_4F3968[new_skill_count];
void attach_DialogLogic_MagicSkills_4F3968() {
    memset(DialogLogic_MagicSkills_4F3968, 255, sizeof(DialogLogic_MagicSkills_4F3968));

    int32_t original[12] = { 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 };
    memcpy(DialogLogic_MagicSkills_4F3968, original, 12 * 4);

    long DialogLogic_MagicSkills_4F3968_long = (long)DialogLogic_MagicSkills_4F3968;
    hook::write_to_readonly((void*)(0x00419110 + 3), &DialogLogic_MagicSkills_4F3968_long, 4);

    long after_MagicSkills = DialogLogic_MagicSkills_4F3968_long + 12 * 4;
    hook::write_to_readonly((void*)(0x00419392 + 3), &after_MagicSkills, 4);

    hook::make_hook((void*)0x00419125, hook_00419125);
}
extern "C" __declspec(dllexport) void __cdecl new_magic(int SkillID) {
    auto i = DialogLogic_MagicSkills_4F3968;
    while (*++i >= 0) {
        if (*i == SkillID) return;
    }   *i++ = SkillID;
    hook::write_to_readonly((void*)(0x00419392 + 3), &i, 4);
}



void smart_strcpy(char_ptr& to, char_ptr from) {
    static std::vector<char_ptr> done; int len = 4096;
    if (!to || std::find(done.begin(), done.end(), to) == done.end()) 
        { to = new char[len]; done.push_back(to); }
    strcpy_s(to, len, from);
}

extern "C" __declspec(dllexport) char_ptr __cdecl getName(int skill_ID) {
    static char_ptr wrong = (char_ptr) "Wrong Skill ID";
    if (skill_ID < 0 || skill_ID >= new_skill_count)
        return wrong;

    char_ptr answer;
    if (skill_ID >= old_skill_count) 
        answer = new_skill_names[skill_ID - old_skill_count];
    else answer = old_skill_names[skill_ID];

    if (!answer) {
        static char_ptr empty = (char_ptr) "Unnamed Skill";
        if (skill_ID >= old_skill_count)
            smart_strcpy(new_skill_names[skill_ID - old_skill_count], empty);
        else smart_strcpy(old_skill_names[skill_ID], empty);
    }

    return answer;
}

__declspec(naked) long hook_00418E07() {
    __asm {
        push esi
        call getName
        add esp, 4
        push eax

        push 0x00418E0E
        ret
    }
}
__declspec(naked) long hook_00418E41() {
    __asm {
        push esi
        call getName
        add esp, 4
        push eax

        push 0x00418E48
        ret
    }
}


__declspec(naked) long hook_004191B4() {
    __asm {
        push esi
        call getName
        add esp, 4
        push eax

        push 0x004191BB
        ret
    }
}
__declspec(naked) long hook_004191EE() {
    __asm {
        push esi
        call getName
        add esp, 4
        push eax

        push 0x004191F5
        ret
    }
}


__declspec(naked) long hook_00419554() {
    __asm {
        push esi
        call getName
        add esp, 4
        push eax

        push 0x0041955B
        ret
    }
}
__declspec(naked) long hook_0041958E() {
    __asm {
        push esi
        call getName
        add esp, 4
        push eax

        push 0x00419595
        ret
    }
}


__declspec(naked) long hook_004198F7() {
    __asm {
        push esi
        call getName
        add esp, 4
        push eax

        push 0x004198FE
        ret
    }
}
__declspec(naked) long hook_00419931() {
    __asm {
        push ecx
        push edx

        push esi
        call getName
        add esp, 4
        // push eax

        pop edx
        pop ecx

        push eax

        push 0x00419938
        ret
    }
}


__declspec(naked) long hook_00417708() {
    __asm {
        // push eax
        push edx

        push esi
        mov eax, 0x004171E0
        call eax
        pop esi

        push eax

        push esi
        call getName
        add esp, 4
        mov ecx, eax

        pop eax

        pop edx
        // pop eax
        push 0x00417714
        ret
    }
}

extern "C" __declspec(dllexport) void __cdecl setName(int skill_ID, char_ptr Name) {
    if (skill_ID >= old_skill_count) smart_strcpy (new_skill_names[skill_ID - old_skill_count], Name);
    else smart_strcpy( old_skill_names[skill_ID], Name); 
}

extern "C" __declspec(dllexport) char_ptr __cdecl getDesc(int skill_ID, int desc_part) {
    if (desc_part < 0 || desc_part >= max_masteries + 2) return nullptr;
    return new_descriptions[desc_part][skill_ID];
    
    switch (desc_part) {
    case 1: return dword_5E4CB0[skill_ID];
    case 2: return dword_5E4C10[skill_ID];
    case 3: return dword_5E4B70[skill_ID];
    case 4: return dword_5E4AD0[skill_ID];
    case 5: return dword_5E4A30[skill_ID];
    default: return nullptr;
    }
}

extern "C" __declspec(dllexport) void __cdecl setDesc(int skill_ID, int desc_part, char_ptr text) {
    if (desc_part < 0 || desc_part >= max_masteries + 2) return;
    smart_strcpy(new_descriptions[desc_part][skill_ID], text);
    return;

    switch (desc_part) {
    case 1:  smart_strcpy(dword_5E4CB0[skill_ID], text); break;
    case 2:  smart_strcpy(dword_5E4C10[skill_ID], text); break;
    case 3:  smart_strcpy(dword_5E4B70[skill_ID], text); break;
    case 4:  smart_strcpy(dword_5E4AD0[skill_ID], text); break;
    case 5:  smart_strcpy(dword_5E4A30[skill_ID], text); break;
    }
}

void text_refresh(char_ptr& to, long from) {
    long zero = 0;
    if (*(long*)(from)) {
        // to = (char_ptr) *(long*)(from);
        smart_strcpy(to, (char_ptr) * (long*)(from) );
        hook::write_to_readonly((long*)(from), &zero, 4);
    }
}

void text_refresh() {
    for (int i = 0; i < old_skill_count;++i) {
        text_refresh(dword_5E4CB0[i], 0x5E4CB0 + i * 4);
        text_refresh(dword_5E4C10[i], 0x5E4C10 + i * 4);
        text_refresh(dword_5E4B70[i], 0x5E4B70 + i * 4);
        text_refresh(dword_5E4AD0[i], 0x5E4AD0 + i * 4);
        text_refresh(dword_5E4A30[i], 0x5E4A30 + i * 4);
    }
}

extern "C" __declspec(dllexport) void __cdecl about() {
    MessageBoxA(0, "SKILLZ plugin by Yuji Sakai", "SKILLZ", 0);
}

void ztimer_impl() {
    static long long counter = 1;
    if ((counter & 63) == 0) {
        text_refresh();
    }

    counter++;
}

unsigned int __fastcall player_get_skill_impl(mm8::Player* player, int skill_ID) {
    int player_id = PlayerID(player);
    if (player_id < 0) return 0;
    if (skill_ID >= old_skill_count && skill_ID < new_skill_count) {
        return saved_data.skills[player_id][skill_ID - old_skill_count];
    }

    return 0;
}

__declspec(naked) int player_get_skill() {
    __asm {
        cmp esi, old_skill_count
        jge custom

        xor ebx,ebx
        cmp esi, 0x19
        push edi
        mov edi, ecx
        push 0x0048EF5D
        ret

        custom:
        push edx
        mov edx, esi
        call player_get_skill_impl
        pop edx

        pop esi
        pop ebx
        ret

    }
}

unsigned int __fastcall player_get_skill_48EF4F(int ecx, int edx, int a2) {
    // auto MMExtCore_dll = GetModuleHandleA("mmextcore.dll");
    int address = 0x48EF4F;
    // auto fun = (int(__fastcall*)(int, int, int))address;
    auto fun = (int(__thiscall*)(int, int))address;
    int answer = 0; // int skill_ID = a2;

    auto player = (mm8::Player*)(ecx);
    int player_id = PlayerID(player);
    if (player_id < 0) return 0;

    /*
    if (a2 >= old_skill_count) {

        return skills[player_id][skill_ID - old_skill_count];
    }
    */

    hook::disable((void*)address);
    answer = fun(ecx, /*edx, */ a2);
    hook::enable((void*)address);

    return answer;
}

extern "C" __declspec(dllexport) int32_t __cdecl get_buffed(mm8::Player * player, int skill_ID) {
    int16_t answer = 0;
    __asm {
        push skill_ID
        mov ecx, player
        mov eax, 0x0048EF4F
        call eax
        mov answer, ax
    }
    return answer;
}


extern "C" __declspec(dllexport) void __cdecl set(mm8::Player * player, int skill_ID, int16_t value) {
    long player_id = PlayerID(player);
    if (player_id < 0) return;

    if (skill_ID < 0 || skill_ID >= new_skill_count) return;
    if (skill_ID < old_skill_count) {
        player->skills[skill_ID] = value;
    } else {
        saved_data.skills[player_id][skill_ID - old_skill_count] = value;
    }
}

extern "C" __declspec(dllexport) void* __cdecl ptr(mm8::Player * player, int skill_ID) {
    long player_id = PlayerID(player);
    if (player_id < 0) 
        return 0;
    if (skill_ID < 0 || skill_ID >= new_skill_count)
        return 0;

    if (skill_ID < 0 || skill_ID >= new_skill_count) return nullptr;
    if (skill_ID < old_skill_count) {
        return &player->skills[skill_ID];
    }
    else {
        return &saved_data.skills[player_id][skill_ID - old_skill_count];
    }
}

auto the_ptr = ptr;
__declspec(naked) long hook_0043233F() {
    __asm {
        push edx
        push ecx

        push eax
        push ecx
        call the_ptr
        add esp, 8
        mov esi, eax

        pop ecx
        pop edx
        xor eax, eax
        push 0x00432348
        ret
    }
}

__declspec(naked) long hook_0043236B() {
    __asm {
        push ecx

        push eax
        push ecx
        call get
        add esp, 8

        pop ecx
        push 0x00432373
        ret
    }
}


__declspec(naked) long hook_004C9EB1() {
    __asm {
        push ecx

        push eax
        push edi
        call get
        add esp, 8

        pop ecx
        push 0x004C9EB9
        ret
    }
}

__declspec(naked) long hook_00417417() {
    __asm {
        push ecx

        push ebx
        push edi
        call get
        add esp, 8

        pop ecx
        push 0x0041741E
        ret
    }
}

const char* Skillz_bin = "skillz.bin";
long fun_0045EFFF = 0x0045EFFF;
long fun_004D9E20 = 0x004D9E20;
long fun_0045F5A1 = 0x0045F5A1;
long fun_004D9F10 = 0x004D9F10;
long fun_0040F5D8 = 0x0040F5D8;
long fun_004651A4 = 0x004651A4;
long fun_004DA641 = 0x004DA641;
long savegame_ret = 0;
__declspec(naked) long hook_0045D2E6_savegame() {
    __asm { 
        pusha

        lea eax, ss: [ebp - 0x44]
        push Skillz_bin
        push eax
        call fun_004D9E20
        pop  ecx
        pop  ecx
        push edi
        push save_long
        lea eax, ss: [ebp - 0x44]
        push eax
        mov  ecx, esi
        mov  eax, saved_block_size
        mov  [ebp - 0x30], eax
        call fun_0045F5A1
        test eax, eax
        jz failed

        push 1
        push dword ptr ds : [0x00601DD8]
        lea eax, ss : [ebp - 0xE4]
        push eax
        call fun_004D9F10
        add esp, 0x0C
        push 9999
        push ebx
        sub esp, 0x10
        mov ecx, esp
        mov dword ptr ss : [ebp - 0x08] , esp
        lea eax, ss : [ebp - 0x01]
        push eax
        lea eax, ss : [ebp - 0xE4]
        push eax
        call fun_0040F5D8
        lea ecx, ss : [ebp - 0x08]
        call fun_004651A4

        failed:
        cmp savegame_ret,0
        jnz nude

        popa
        lea eax, ss : [ebp - 0x44]
        push eax
        call fun_004D9E20
        push 0x0045D2EC
        ret

        nude:
        popa
        push savegame_ret
        ret
    }
}

long loadgame_ret = 0;
__declspec(naked) long hook_0045C9BB_loadgame() {
    __asm {
        pusha

        push 1
        push Skillz_bin
        mov ecx, esi
        call fun_0045EFFF
        test eax, eax
        jz failed_to_load

        push eax
        push ebx
        push saved_block_size
        push save_long
        call fun_004DA641
        add esp, 0x10
        jmp load_succeed

        failed_to_load:
        mov al, 0
        mov ecx, saved_block_size
        mov edi, save_long
        rep stosb

        load_succeed:

        cmp loadgame_ret, 0
        jnz nude

        popa
        mov ecx, esi
        call fun_0045EFFF
        push 0x0045C9C2
        ret

        nude:
        popa
        push loadgame_ret;
        ret
    }
}

__declspec(naked) long hook_0041728C(){
    __asm {
        and byte ptr ss : [ebp - 0x84] , 0x00
        xor ecx,ecx
        push eax
        push edx
        
        push ebx
        push esi
        call get
        add esp, 8
        movzx ecx, ax

        pop edx
        pop eax
        and eax, 0x3FF
        push 0x4F3BB8
        and ecx, 0x3FF

        xor eax, ecx
        push 0x004172A9
        ret
    }
}

// teachers
__declspec(naked) long hook_004B32E3() {
    __asm {
        sub edi, menu_skill_offset
        pusha 

        /*
        mov eax, 0x0048F54A
        call eax 
        imul eax, eax, 0x27
        cmp byte ptr ds : [eax + edi * 1 + 0x4FDD18] , 0x00
        */
        push edi
        push ecx
        call MasteryLimit
        add esp, 8
        cmp eax, 0
        // movzx eax, byte ptr[ecx + 352h]

        popa
        push 0x004B32F6
        ret
    }
}

__declspec(naked) long hook_004B3304() {
    __asm {
        push eax
        push ebx
        push ecx

        push edi
        call getName
        add esp, 4
        mov edx, eax

        pop ecx
        pop ebx
        pop eax
        push 0x004B330B
        ret
    }
}
__declspec(naked) long hook_004B32FA() {
    __asm {
        pusha

        push edi
        push esi
        call get
        add esp, 8
        cmp eax, 0
        //mov eax, 0

        popa
        push 0x004B3302
        ret
    }
}
__declspec(naked) long hook_004B938E() {
    __asm {
        pusha

        push esi
        push eax
        call get
        add esp, 8
        cmp ax, bx

        popa 
        push 0x004B9396
        ret
    }
}
__declspec(naked) long hook_004B9398() {
    __asm {
        push eax
        push ecx

        push esi
        call getName
        add esp, 4
        mov edx, eax

        xor ebx, ebx
        pop ecx
        pop eax
        push 0x004B939F
        ret
    }
}

__declspec(naked) long hook_004B9493() {
    __asm {
        push eax
        push ecx
        push edx

        push edi
        call getName
        add esp, 4
        mov edi, eax

        xor ebx, ebx
        pop edx
        pop ecx
        pop eax
        push 0x004B949A
        ret
    }
}

__declspec(naked) long hook_004B9483() {
    __asm {
        pusha

        push edi
        push eax
        call get
        add esp, 8
        cmp ax, bx

        popa
        push 0x004B948B
        ret
    }
}

__declspec(naked) long hook_004B946C(){
    __asm {
        sub edi, menu_skill_offset
        pusha 

        /*
        mov eax, 0x0048F54A
        call eax
        imul eax, eax, 27h
        cmp 0x4FDD18[eax + edi], bl
        */
        
        push edi
        push ecx
        call MasteryLimit
        add esp, 8
        cmp eax, 0
        //movzx eax, byte ptr[ecx + 352h]

        popa
        push 0x004B947E
        ret
    }
}

__declspec(naked) long hook_004B7BFA() {
    __asm {
        pusha

        push esi
        push eax
        call get
        add esp, 8
        cmp ax, bx

        popa
        push 0x004B7C02
        ret
    }
}

__declspec(naked) long hook_004B7C04() {
    __asm {
        push eax
        push ecx

        push esi
        call getName
        add esp, 4
        mov edx, eax

        xor ebx, ebx
        pop ecx
        pop eax
        push 0x004B7C0B
        ret
    }
}

__declspec(naked) long hook_004B7CEF() {
    __asm {
        pusha

        push edi
        push eax
        call get
        add esp, 8
        cmp ax, bx

        popa
        push 0x004B7CF7
        ret
    }
}

__declspec(naked) long hook_004B7CFF() {
    __asm {
        push eax
        push ecx
        push edx

        push edi
        call getName
        add esp, 4
        mov edi, eax

        xor ebx, ebx
        pop edx
        pop ecx
        pop eax
        push 0x004B7D06
        ret
    }
}


__declspec(naked) long hook_004B82BE() {
    __asm {
        pusha

        push esi
        push eax
        call get
        add esp, 8
        cmp ax, bx

        popa
        push 0x004B82C6
        ret
    }
}

__declspec(naked) long hook_004B82C8() {
    __asm {
        push eax
        push ecx

        push esi
        call getName
        add esp, 4
        mov edx, eax

        xor ebx, ebx
        pop ecx
        pop eax
        push 0x004B82CF
        ret
    }
}


__declspec(naked) long hook_004B83C3() {
    __asm {
        push eax
        push ecx

        push edi
        call getName
        add esp, 4
        mov edi, eax

        pop ecx
        pop eax
        push 0x004B83CA
        ret
    }
}


__declspec(naked) long hook_0048F056() {
    __asm {     
        //push ebx

        push esi
        push edi
        call get
        add esp, 8
        mov ecx, eax
        and ecx, 0x3FF
        add ecx, ebx
        cmp ecx, 0x3FF

        jl lab1
        and eax, 0xFFFFFC00
        add eax, 0x3FF
        jmp lab2
        lab1:
        test ecx,ecx
        jge lab3
        and eax, 0xFFFFFC00
        inc eax
        lab3:
        add eax,ebx
        lab2: 
        pop edi
        pop esi
        pop ebx
        ret


        //pop ebx
        push 0x0048F060
        ret
    }
}


__declspec(naked) long hook_004B83B3() {
    __asm {
        pusha

        push edi
        push eax
        call get
        add esp, 8
        cmp ax, bx

        popa
        push 0x004B83BB
        ret
    }
}


__declspec(naked) long hook_004BAF79() {
    __asm {
        push eax
        mov eax, ebp

        // sub eax, menu_skill_offset // 36
        push eax
        push edi
        call the_ptr
        add esp, 8
        mov ebp, eax

        pop eax
        push 0x004BAF80
        ret
    }
}

__declspec(naked) long hook_004B4A88() {
    __asm {
        push eax
        // mov eax, ebp

        sub eax, menu_skill_offset // 36
        push eax
        push ecx
        call the_ptr
        add esp, 8
        mov esi, eax

        pop eax
        push 0x004B4A8F
        ret
    }
}


__declspec(naked) long hook_004B4E01() {
    __asm {
        //push eax
        //push esi
        pusha

        sub esi, menu_skill_offset // 36
        push esi
        push eax
        call get
        add esp, 8
        cmp ax, bx

        popa
        //pop esi
        //pop eax
        push 0x004B4E09
        ret
    }
}

__declspec(naked) long hook_004B4F98() {
    __asm {
        //push eax
        //push edi
        pusha

        sub edi, menu_skill_offset // 36
        push edi
        push eax
        call get
        add esp, 8
        cmp ax, bx

        popa
        //pop edi
        //pop eax
        push 0x004B4FA0
        ret
    }
}

__declspec(naked) long hook_004BAEE8() {
    __asm {

        cmp     ebp, menu_skill_offset
        jge     loc_4BAF2E

        cmp     ebp, 24h
        jl      loc_4BB3F0
        cmp     ebp, 38h
        jle     loc_4BAF2E
        cmp     ebp, 3bh
        jl      loc_4BB3F0

        cmp     ebp, 4ah
        jle     loc_4BAF2E

        // cmp     ebp, 5eh
        // jne     loc_4BB3F0

        /*
        cmp     ebp, menu_skill_offset + new_skill_count // 4ah
        jl      loc_4BAF2E
        */


    loc_4BAF0D:
        push 0x004BAF0D
        ret

    loc_4BAF2E:
        push 0x4BAF2E
        ret

    loc_4BB3F0:
        push 0x4BB3F0
        ret
    }
}

/*
// do not hook own function moron :P
__declspec(naked) long hook_79582522() {
    __asm {
        cmp dword ptr ss : [ebp + 0x0C] , new_skill_count
        jge loc_79582544
        mov ecx, dword ptr ss : [ebp + 0x08]

        push edx
        push ecx
        call get

        // return
        mov esp, ebp
        pop ebp
        ret

        loc_79582544:
        push 0x79582544
        ret

    }
}
*/

__declspec(naked) long hook_004BAE92() {
    __asm {
        cmp ebp, menu_skill_offset
        jge lab_4BAEA1

        cmp ebp, 0x5f
        jg lab_4BB249
        je lab_4BAFD8

        lab_4BAEA1:
        push 0x004BAEA1
        ret

        lab_4BB249:
        push 0x004BB249
        ret

        lab_4BAFD8 :
        push 0x004BAFD8
        ret
    }
}

__declspec(naked) long hook_004B7BDD() {
    __asm {
        mov esi, dword ptr ds : [eax + 0x24]
        mov ecx, dword ptr ss : [ebp - 0x14]
        sub esi, menu_skill_offset
        pusha

        push esi
        push ecx
        call MasteryLimit
        add esp, 8
        cmp eax, 0
        //movzx eax, byte ptr[ecx + 352h]

        popa
        push 0x004B7BF5
        //push 0x004B7BE6
        ret
    }

}


__declspec(naked) long hook_004B58FF() {
    __asm {   
        mov ecx, dword ptr ss : [ebp - 0x0C]
        sub esi, menu_skill_offset
        pusha

        /*
        mov eax, 0x48F54A
        call eax
        imul    eax, 27h
        cmp     0x4FDD18[eax + esi], bl
        */

        push esi
        push ecx
        call MasteryLimit
        add esp, 8
        cmp eax, 0
        //movzx eax, byte ptr[ecx + 352h]

        popa
        push 0x004B5914
        ret
    }
}

__declspec(naked) long hook_004B5919() {
    __asm {

        push eax
        //push ecx
        push esi
        push eax
        call get
        add esp, 8
        cmp ax, bx
        //pop ecx
        pop eax
        jnz lab_4B5941

        push eax
        push esi
        call getName
        add esp, 4
        mov edx, eax
        pop eax

        lab_4B592A:
        mov ebx, 0
        push 0x004B592A
        ret

        lab_4B5941:
        push 0x004B5941
        ret
    }
}


__declspec(naked) long hook_004B59F9() {
    __asm {
        mov edi, dword ptr ds : [esi + 0x24]
        sub edi, menu_skill_offset
        pusha
        
        /*
        mov eax, 0x48F54A
        call eax
        imul eax, 27h
        cmp 0x4FDD18[eax + edi], bl
        */
        push edi
        push ecx
        call MasteryLimit
        add esp, 8
        cmp eax, 0
        //movzx eax, byte ptr[ecx + 352h]

        popa
        push 0x004B5A0E
        ret
    }

}
__declspec(naked) long hook_004B5A23() {
    __asm {
        push eax
        push ecx

        push edi
        call getName
        add esp, 4
        mov edi, eax

        pop ecx
        pop eax
        push 0x004B5A2A
        ret
    }

}

__declspec(naked) long hook_004B404B() {
    __asm {
        mov edi, [esi + 0x24]
        sub edi, menu_skill_offset
        pusha

        push edi
        push ecx
        call MasteryLimit
        add esp, 8
        cmp eax, 0
        //movzx eax, byte ptr[ecx + 352h]

        popa
        push 0x004B4060
        ret
    }
}
__declspec(naked) long hook_004B4065() {
    __asm {
        pusha // eax

        push edi
        push eax
        call get
        add esp, 8
        cmp ax, bx

        popa // eax
        push 0x004B406D
        ret
    }
}


__declspec(naked) long hook_004B4075() {
    __asm {
        push eax

        push edi
        call getName
        add esp, 4
        mov edi, eax

        mov ecx, [ebp - 0x10]
        pop eax
        push 0x004B407C
        ret
    }
}

__declspec(naked) long hook_004B3F56(){
    __asm {
        mov ecx, dword ptr ss : [ebp - 0x14]
        sub esi, menu_skill_offset
        pusha

        push esi
        push ecx
        call MasteryLimit
        add esp, 8
        cmp eax, 0
        //movzx eax, byte ptr[ecx + 352h]

        popa
        push 0x004B3F6B
        ret
    }
}

__declspec(naked) long hook_004B3F70() {
    __asm {

        push esi
        push eax
        call get
        add esp, 8
        cmp ax, bx

        push 0x004B3F78
        ret
    }
}


__declspec(naked) long hook_004B3F7A() {
    __asm {
        push esi
        call getName
        add esp, 4
        mov edx, eax

        xor ebx, ebx
        push 0x004B3F81
        ret
    }
}

__declspec(naked) long hook_004B8399() {
    __asm {
        mov edi, dword ptr ds : [esi + 0x24]
        sub edi, menu_skill_offset
        pusha

        /*
        mov eax, 0x48F54A
        call    eax
        imul    eax, 27h
        cmp     0x4FDD18[eax + edi], bl
        */

        push edi
        push ecx
        call MasteryLimit
        add esp, 8
        cmp eax, 0
        //movzx eax, byte ptr[ecx + 352h]

        popa
        push 0x004B83AE
        ret
    }
}

__declspec(naked) long hook_004B82A1() {
    __asm {
        mov esi, dword ptr ds : [eax + 0x24]
        mov ecx, dword ptr ss : [ebp - 0x18]
        sub esi, menu_skill_offset
        pusha

        /*
        mov eax, 0x48F54A
        call    eax
        imul    eax, 27h
        cmp     0x4FDD18[eax + esi], bl
        */
        
        push esi
        push ecx
        call MasteryLimit
        add esp, 8
        cmp eax, 0
        // movzx eax, byte ptr[ecx + 352h]
        
        popa
        push 0x004B82B9
        ret
    }

}

__declspec(naked) long hook_004B7CD5() {
    __asm {
        mov edi, dword ptr ds : [esi + 0x24]
        sub edi, menu_skill_offset
        pusha

        /*
        mov eax, 0x48F54A
        call    eax
        imul    eax, 27h
        cmp     0x4FDD18[eax + edi], bl
        */
        push edi
        push ecx
        call MasteryLimit
        add esp, 8
        cmp eax, 0
        // movzx eax, byte ptr[ecx + 352h]

        popa
        push 0x004B7CEA
        ret
    }
}

__declspec(naked) long hook_004B9374() {
    __asm {
        mov ecx, dword ptr ss : [ebp - 0x10]
        sub esi, menu_skill_offset
        pusha

        /*
        mov eax, 0x48F54A
        call eax
        imul eax, eax, 27h
        cmp 0x4FDD18[eax + esi], bl
        */

        push esi
        push ecx
        call MasteryLimit
        add esp, 8
        cmp eax, 0
        // movzx eax, byte ptr[ecx + 352h]

        popa
        push 0x004B9389
        ret
    }
}

__declspec(naked) long hook_004B4BA2() {
    __asm {
        mov ecx, dword ptr ss : [ebp - 0x14]
        sub esi, menu_skill_offset
        pusha

        push esi
        push ecx
        call MasteryLimit
        add esp, 8
        cmp eax, 0
        // movzx eax, byte ptr[ecx + 352h]

        popa
        push 0x004B4BB7
        ret
    }
}


__declspec(naked) long hook_004B4BBC() {
    __asm {
        pusha

        push esi
        push eax
        call get
        add esp, 8
        cmp ax, bx

        popa
        push 0x004B4BC4
        ret
    }
}
__declspec(naked) long hook_004B4BC6() {
    __asm {
        push esi
        call getName
        add esp, 4
        mov edx, eax

        push 0x004B4BCD
        ret
    }
}



__declspec(naked) long hook_004B4C99() {
    __asm {
        mov edi, [esi + 24h]
        sub edi, menu_skill_offset
        pusha

        push edi
        push ecx
        call MasteryLimit
        add esp, 8
        cmp eax, 0
        // movzx eax, byte ptr[ecx + 352h]

        popa
        push 0x004B4CAE
        ret
    }
}



__declspec(naked) long hook_004B4CC3() {
    __asm {
        

        push edi
        call getName
        add esp, 4
        mov edi, eax
        mov eax, [ebp - 0x08]
        mov ecx, [ebp - 0x10]

        push 0x004B4CCA
        ret
    }
}

__declspec(naked) long hook_0048E2CE() {
    __asm {
        push eax
        push edi
        call get
        add esp, 8
        cmp ax, bx

        push 0x0048E2D6
        ret
    }
}
__declspec(naked) long hook_004B693D() {
    __asm {
        sub esi, menu_skill_offset
        /*
        mov eax, 0x48F54A
        call eax
        imul eax, 27h
        cmp 0x4FDD18[eax + esi], bl
        */
        pusha

        push esi
        push ecx
        call MasteryLimit
        add esp, 8
        cmp eax, 0
        
        popa
        // mov ecx, [ebp - 0x18]
        // movzx eax, byte ptr[ecx + 352h]


        push 0x004B694F
        ret
    }
}

__declspec(naked) long hook_004B6A38() {
    __asm {
        sub edi, menu_skill_offset

        push edi
        push ecx
        call MasteryLimit
        add esp, 8
        cmp eax, 0

        //movzx eax, byte ptr[ecx + 352h]
        mov ecx, [ebp - 0x18]

        push 0x004B6A4A
        ret
    }
}

__declspec(naked) long hook_004B6A4C() {
    __asm {
        mov     eax, [ebp - 0x18]

        push edi
        push eax
        call get
        add esp, 8
        cmp ax, bx

        push 0x004B6A57
        ret
    }
}

__declspec(naked) long hook_004B6A5F() {
    __asm {

        push edi
        call getName
        add esp, 4
        mov edi, eax
        mov eax, [ebp - 0x0C]
        mov ecx, [ebp - 0x04]

        push 0x004B6A66
        ret
    }
}

__declspec(naked) long hook_() {
    __asm {

    }
}

long fun_0041C473 = 0x0041C473;
int code_004B23B6(int v44) {
    int answer = 0;
    __asm {
        mov ecx, dword ptr ds : [0x00519324]
        push 0x02
        push 0x00
        push 0x01
        push v44
        call fun_0041C473
        mov eax, dword ptr ds : [0x00519324]
        mov eax, dword ptr ds : [eax + 0x28]
        mov dword ptr ds : [0x00FFD450] , eax
        mov answer, eax
    }
    return answer;
}

auto newHouseBlock = (int (__fastcall*)(int, int)) (0x004B1F3C) ;
auto dword_FFD424 = (char_ptr*)0xFFD424; auto dword_FFD448 = (int*) 0xFFD448;
int code_004B238F(/*char_ptr skill_name,*/ int skill_type, int& v2, int& v44, int& v45) {
    // dword_FFD424[v2] = 0;
    getName(skill_type); // skill_name;
    int v37 = v44++; 
    *dword_FFD448 = v2 + 1;
    newHouseBlock(v37,/*36*/ menu_skill_offset + skill_type);
    if (v44 >= v45) return false;
    v2 = *dword_FFD448; return true;
}

const int Weapon_Shop = 1; const int Armor_Shop = 2;
int Skill_Shop[36][new_skill_count];

int __fastcall createSkillLearningTopics(int HouseType) {
    int answer = 0; int address = 0x004B1F73;
    if (HouseType <= 15 || HouseType == 21|| HouseType == 23 || HouseType == 30) {
        int v44 = 0; int v2 = 0; int v45 = 1;
        auto i = Skill_Shop[HouseType];// DialogLogic_WeaponSkills_4F3914;
        for(;*i>=0;++i)
            code_004B238F(*i, v2, v44, v45);
        return code_004B23B6(v44);
    }
    else {
        hook::disable((void*)address);
        __asm {
            mov ecx, HouseType
            call address
            mov answer, eax
        }
        hook::enable((void*)address);
    }

    return answer;
}

void preset_shops() {
    memset(Skill_Shop, 255, sizeof(Skill_Shop));
    int Weaponz[] = { 1,5,3,6,0, 2,4};
    memcpy(Skill_Shop[1], Weaponz, sizeof(Weaponz));
    int Armorz[] = { 9,10,11, 8 };
    memcpy(Skill_Shop[2], Armorz, sizeof(Armorz));
    int Magicianz[] = {24, 26};
    memcpy(Skill_Shop[3], Magicianz, sizeof(Magicianz));
    int Alchemyz[] = {37, 34};
    memcpy(Skill_Shop[4], Alchemyz, sizeof(Alchemyz));

    Skill_Shop[5][0] = 12;
    Skill_Shop[6][0] = 13;
    Skill_Shop[7][0] = 14;
    Skill_Shop[8][0] = 15;
    Skill_Shop[9][0] = 16;
    Skill_Shop[10][0] = 17;
    Skill_Shop[11][0] = 18;
    Skill_Shop[12][0] = 19;
    Skill_Shop[13][0] = 20;

    /*
    for (int i = 5; i < 14; i++) {
        Skill_Shop[i][1] = 38;
        Skill_Shop[i][2] = 28;
    }
    */

    int Elementz[] = { 12,13,14,15, /*38, 28*/};
    memcpy(Skill_Shop[14], Elementz, sizeof(Elementz));
    int Selfz[] = { 16, 17, 18, /* 38, 28 */};
    memcpy(Skill_Shop[15], Selfz, sizeof(Selfz));

    int Tavernz[] = { 36, 31, 29 };
    memcpy(Skill_Shop[21], Tavernz, sizeof(Tavernz));

    int Templez[] = { 33, 32, 30, 25, 28 };
    memcpy(Skill_Shop[23], Templez, sizeof(Templez));

    int Instructionz[] = {35,27, 38};
    memcpy(Skill_Shop[30], Instructionz, sizeof(Instructionz));
}

extern "C" __declspec(dllexport) int32_t __cdecl learn_at(int skill_ID, int HouseType) {
    if (skill_ID < 0 || HouseType < 0) return false;

    // if (HouseType <= 15 || HouseType == 21 || HouseType == 23 || HouseType == 30)
    if (HouseType < 36 && Skill_Shop[HouseType][0] >= 0)
    {
        auto i = Skill_Shop[HouseType];
        for (; *i >= 0; ++i)
            if(*i==skill_ID)
                return false;

        *i = skill_ID;
        return true;
    }

    return false;
}


__declspec(naked) long hook_0043343A() {
    __asm {
        push eax

        push 1
        push ebp
        push eax
        call set
        add esp, 0xc

        pop eax
        push 0x00433444
        ret
    }
}


__declspec(naked) long hook_004BAF64() {
    __asm {
        //pusha
        push eax
        push ebx
        push ecx

        /*
        mov eax, 0x0048F54A
        call eax
        imul eax, eax, 0x27
        cmp  0x4FDCF4[eax + ebp], bl
        */
        sub ebp, menu_skill_offset
        push ebp
        push ecx
        call MasteryLimit
        add esp, 8
        cmp eax, 0
        //movzx eax, byte ptr[ecx + 352h]
        
        pop ecx
        pop ebx
        pop eax
        //popa
        push 0x004BAF73
        // push 0x004BAF73
        ret
    }
}


__declspec(naked) long hook_004BB6F7() {
    __asm {
        mov eax, dword ptr ds : [0x00FFD408]

        cmp eax, menu_skill_offset
        jge teacher

        cmp eax, 0x38
        push 0x004BB6FF
        ret

        teacher:
        push 0x004BBCBE
        ret
    }
}

__declspec(naked) long hook_004B33DF() {
    __asm {
        sub ebx, menu_skill_offset
        pusha

        push ebx
        push ecx
        call MasteryLimit
        add esp, 8
        cmp eax, 0

        popa
        push 0x004B33F2
        ret
    }
}

__declspec(naked) long hook_004B33F6() {
    __asm {
        pusha

        push ebx
        push esi
        call get
        add esp, 8
        cmp eax, 0

        popa
        push 0x004B33FE
        ret
    }
}

__declspec(naked) long hook_004B3406() {
    __asm {
        push ebx
        call getName
        add esp, 4
        mov ebx, eax

        xor eax, eax
        mov edx, [ebp-0x10]
        mov ecx, [ebp-0x08]

        push 0x004B340D
        ret
    }
}

long /* mm8::Player* */ ebx_004B0E5D = 0;// nullptr;
__declspec(naked) long hook_004B0E5D() {
    __asm {
        mov[ebp - 0x0C], eax
        mov ebx_004B0E5D, ebx

        push ecx
        push ebx

        push ecx
        push ebx

        push ecx
        push ebx
        call get
        add esp, 8
        mov edi, eax

        /*
        pop ebx
        pop ecx

        push ecx
        push ebx
        */

        call MasteryLimit
        add esp, 8

        pop ebx
        pop ecx
        push 0x004B0E73
        ret
    }
}

void __cdecl MasteryLimit_fill(char* v18, int v17, mm8::Player* player, int skill_ID);
__declspec(naked) long hook_004B0E8E() {
    __asm {
        mov eax, edi
        xor esi, esi

        push ecx
        push ebx
        push[ebp - 8]
        push[ebp - 4]
        call MasteryLimit_fill
        add esp, 0x10

        push 0x004B0EBB
        ret
        
        /*
        next:
        push ecx
        push ebx
        call MasteryLimit
        add esp, 8
        // mov ebx, eax

        lea edx, ss: [ebp + esi * 1 - 0x02]
        and byte ptr ds : [edx] , 0x00
        cmp eax, dword ptr ss : [ebp - 0x08]
        jl lab_004B0EB2
        mov byte ptr ds : [edx] , 0x01
        lab_004B0EB2:
        inc esi

        cmp esi, 2
        jl next

        push 0x004B0EBB
        ret
        */
    }
}

__declspec(naked) long hook_0041719A() {
    __asm {
        mov eax, [ebp - 0x04]

        push edx
        push edi
        call MasteryLimit
        add esp, 8
        cmp eax, [ebp + 8]

        push 0x004171AB
        ret
    }
}


#include "structs/MergeSpecific.h"
extern mm8::CharacterPortrait* portraits;
//mm8::Player* tmp_00417295 = nullptr;
__declspec(naked) long hook_00417295() {
    __asm {
        push eax
        //mov tmp_00417295, esi
    
        push ebx
        push esi
        call get
        add esp, 8
        mov ecx, eax

        pop eax
        push 0x0041729C
        ret
    }
}

mm8::Player* tmp_00417265 = nullptr;
__declspec(naked) long hook_00417265() {
    __asm {
        imul    esi, 0x1D28
        add     esi, 0x00B2187C

        mov     tmp_00417265, esi

        push    0x00417271
        ret
    }
}


int next_class(int current);
void init_settings_once();
extern "C" __declspec(dllexport) int32_t __cdecl MasteryLimit_raw(int race, int clas, int skill_ID);
auto sub_40F247 = (int(__fastcall*) (int,int,int)) 0x40F247;
int __fastcall sub_41714E(int a1, int a2, int a3) {
    int v4, v5, v7, v8, v9;
    init_settings_once();

    v9 = sub_40F247(255, 255, 255);
    v8 = sub_40F247(255, 0, 0);
    v7 = sub_40F247(255, 255, 0);
    v4 = 0;
    int race = tmp_00417265 ? portraits[tmp_00417265->face].race : -1;

    
    if(true)//if(!(a1%2))
    {
        if (MasteryLimit_raw(race, a1, a2) >= a3) {
            return v9;
        }
        v4 = v7;
        if (MasteryLimit_raw(race, next_class(a1) , a2) >= a3) {
            return v4;
        }
        return v8;
    }
    else
    {
        if (MasteryLimit_raw(race, a1, a2) >= a3) {
            return v9;
        }
        else {
            return v8;
        }}
    return v4;
}

long player_get_skill_48EF4F_long = 0x48EF4F;
__declspec(naked) void hook_0041745C() {
    __asm {
        call player_get_skill_48EF4F_long
        push eax

        push ebx
        push edi
        call get
        add esp,8
        mov ecx, eax

        // and eax, 3FFh
        // and ecx, 3FFh

        pop eax

        and eax, 3FFh
        and ecx, 3FFh

        push 0x0041746B
        ret
    }
}

auto getSkillMastery_old = (int (__fastcall*) (int16_t) ) 0x00455B09;
auto concatenateStrings = (char* (*) (char*, const char*, ...))0x004D9E30;
auto processFormattedString_4D9F10 = (int (*)(char * , const char*, ...)) 0x4D9F10;
auto color_41714E = (int(__fastcall*) (int, int, int))0x41714E;
auto sub_48F54A = (int(__thiscall*)(mm8::Player*)) 0x48F54A;
auto byte_4F3BB0 = (char*) 0x4F3BB0;
auto sub_449C7B = (int(__fastcall*) (char*, char*)) 0x449C7B;
char HintBuffer[65536] = {}; long HintBuffer_long = (long) HintBuffer;

extern "C" int __cdecl getSkillMastery_new(mm8::Player* p, int skil) {
    int mastery = getSkillMastery_old(get(p, skil));
    if (mastery < 4) return mastery;
    mastery += 0xF & saved_data.extra_mastery[PlayerID(p)][skil];
    return mastery;
}

extern "C" char_ptr  __cdecl process_skill_description(char_ptr target, mm8::Player* esi, int a2, bool bonus) {
    int skill_id = a2 & 0xff;
    int mastery = getSkillMastery_old(get(esi, skill_id));
    char_ptr mastery_str = mastery_name[mastery];
    // char_ptr skill_str = getName(skill_ID);
    auto dword_5DB938 = *(char**)0x5DB938;
    refresh_mastery_names();

    processFormattedString_4D9F10(target, "%s \n", getDesc(skill_id,1));
    for (int i = 2; i < max_masteries +2; ++i) {
        char_ptr skill_str = getDesc(skill_id, i);
        if (!skill_str || !*skill_str) break;
        char_ptr mastery_str = mastery_name[i - 1];
        if (!mastery_str || !*mastery_str) break;
        int v64 = sub_449C7B(dword_5DB938, mastery_str);
        char v61[1280] = ""; char v62[1280] = "";
        int v20 = sub_48F54A(esi);    
        int v21 = color_41714E(v20, a2, i - 1);
        processFormattedString_4D9F10(v62, byte_4F3BB0, v21);
        concatenateStrings(v61, v62);
        // concatenateStrings(v61, "%s\t%03d:\t%03d%s\t000\n", v33, v41, v49, v57);
        // processFormattedString_4D9F10(v62, "%s\t%03d:\t%03d%s\t000\n", mastery_str, v64 + 22, v64 + 22, skill_str);
        processFormattedString_4D9F10(v62, "%s:\t%03d%s\t000\n", mastery_str, 72, skill_str);

        concatenateStrings(v61, v62);

        concatenateStrings(target, v61);
    }

    if (bonus) {
        char tmp[1280] = ""; int value = get_buffed(esi,skill_id) - get(esi, skill_id);
        if(value>0) processFormattedString_4D9F10(tmp,"\n\n Bonus: +%d", value);
        else processFormattedString_4D9F10(tmp, "\n\n Bonus: %d", value);
        concatenateStrings(target, tmp);
    }

    return target;
}

__declspec(naked) int  hook_004172C1() {
    __asm {

        push 1
        push ebx
        push esi
        push HintBuffer_long // 0x517FF0
        call process_skill_description
        add esp, 0x10

        pop edi
        mov eax, HintBuffer_long // 0x517FF0
        pop ebx
        leave
        retn

        /*
        mov esi, eax
        push 0x00417650
        ret
        */
    }

}

__declspec(naked) int hook_004174DD() {
    __asm {
        
        push 0
        push ebx
        push esi
        push HintBuffer_long // 0x517FF0
        call process_skill_description
        add esp, 0x10

        pop edi
        mov eax, HintBuffer_long // 0x517FF0
        pop ebx
        leave
        retn

        /*
        mov esi, eax
        push 0x00417650
        ret
        */
    }

}


__declspec(naked) int hook_0043349C() {
    __asm {
        push 0
        push [ebp-8]
        push edi
        push HintBuffer_long // 0x517FF0
        call process_skill_description
        add esp, 0x10

        push eax    
        push 0x004334A2
        ret
    }
}

__declspec(naked) int hook_004198EE() {
    __asm {

        mov esi, [ebp - 20h]

        push esi
        push [ebp - 72]
        call getSkillMastery_new
        add esp, 8
        push [eax*4+mastery_name]

        push 0x004198F7
        ret
    }
}


__declspec(naked) int hook_00418DFE() {
    __asm {

        mov esi, [ebp - 20h]

        push esi
        push[ebp - 72]
        call getSkillMastery_new
        add esp, 8
        push[eax * 4 + mastery_name]

        push 0x00418E07
        ret
    }
}


__declspec(naked) int hook_004191AB() {
    __asm {

        mov esi, [ebp - 20h]

        push esi
        push[ebp - 72]
        call getSkillMastery_new
        add esp, 8
        push[eax * 4 + mastery_name]

        push 0x004191B4
        ret
    }
}


__declspec(naked) int hook_0041954B() {
    __asm {

        mov esi, [ebp - 20h]

        push esi
        push[ebp - 72]
        call getSkillMastery_new
        add esp, 8
        push[eax * 4 + mastery_name]

        push 0x00419554
        ret
    }
}

void refresh_mastery_names() {

    smart_strcpy(mastery_name[0], (char_ptr)"");
    smart_strcpy(mastery_name[1], (char_ptr)"Novice");
    // smart_strcpy(mastery_name[2], (char_ptr)"Advanced");
    smart_strcpy(mastery_name[2], (char_ptr)"Expert");
    smart_strcpy(mastery_name[3], (char_ptr)"Master");
    smart_strcpy(mastery_name[4], (char_ptr)"Grand");
    smart_strcpy(mastery_name[5], (char_ptr)"Supreme");
    smart_strcpy(mastery_name[6], (char_ptr)"Ultimate");
    smart_strcpy(mastery_name[7], (char_ptr)"Ascended");
    smart_strcpy(mastery_name[8], (char_ptr)"Deity");

    hook::write_to_readonly((void*)0x00601B04, &mastery_name[1], 4);
    hook::write_to_readonly((void*)0x00601B08, &mastery_name[3], 4);
    hook::write_to_readonly((void*)0x00601B0C, &mastery_name[2], 4);

    hook::write_to_readonly((void*)0x006015C8, &mastery_name[4], 4);
}


int __fastcall sub_48F622(mm8::Player* _this, int dummy, int a2);
__declspec(naked) int hook_00433428() {
    __asm {
        call sub_48F622
        cmp eax, new_skill_count
        push 0x00433430
        ret
    }
}


__declspec(naked) int hook_00433A54() {
    __asm {
        pusha

        push 0
        push ebp
        push eax
        call set
        add esp, 0xC

        popa
        push 0x00433A5C
        ret
    }

}


__declspec(naked) int hook_004940E8() {
    __asm {
        pusha

        push edi
        push esi
        call get
        add esp, 8
        cmp eax, 0

        popa
        push 0x004940F1
        ret
    }

}

extern "C" void __cdecl startup_impl(void) {
    static bool once = false;
    if (once) return;

    hook::make_hook((void*)0x004940E8, hook_004940E8);
    hook::make_hook((void*)0x00433A54, hook_00433A54);
    hook::make_hook((void*)0x00433428, hook_00433428);

    refresh_mastery_names();
    hook::make_hook((void*)0x00418DFE, hook_00418DFE);
    hook::make_hook((void*)0x004191AB, hook_004191AB);
    hook::make_hook((void*)0x0041954B, hook_0041954B);
    hook::make_hook((void*)0x004198EE, hook_004198EE);
    hook::make_hook((void*)0x004172C1, hook_004172C1);
    hook::make_hook((void*)0x004174DD, hook_004174DD);
    hook::make_hook((void*)0x0043349C, hook_0043349C);

    // MessageBoxA(0, "SKILLZ", "SKILLZ", 0);

    hook::make_hook((void*)0x0041745C, hook_0041745C);

    void* dword_5E4CB0_ptr = &dword_5E4CB0_long;
    // memcpy(dword_5E4CB0_ptr, (void*) 0x5E4CB0, 4*old_skill_count);
    hook::write_to_readonly((void*)(0x00416B39 + 2), dword_5E4CB0_ptr, 4);
    hook::write_to_readonly((void*)(0x004174B8 + 2), dword_5E4CB0_ptr, 4);
    hook::write_to_readonly((void*)(0x0041763A + 2), dword_5E4CB0_ptr, 4);
    hook::write_to_readonly((void*)(0x00433AD2 + 2), dword_5E4CB0_ptr, 4);
    hook::write_to_readonly((void*)(0x004556BA + 2), dword_5E4CB0_ptr, 4);

    void* dword_5E4C10_ptr = &dword_5E4C10_long;
    hook::write_to_readonly((void*)(0x004174A4 + 2), dword_5E4C10_ptr, 4);
    hook::write_to_readonly((void*)(0x0041762C + 2), dword_5E4C10_ptr, 4);
    hook::write_to_readonly((void*)(0x004556AD + 2), dword_5E4C10_ptr, 4);

    void* dword_5E4B70_ptr = &dword_5E4B70_long;
    hook::write_to_readonly((void*)(0x00417496 + 2), dword_5E4B70_ptr, 4);
    hook::write_to_readonly((void*)(0x0041761E + 2), dword_5E4B70_ptr, 4);
    hook::write_to_readonly((void*)(0x004556A0 + 2), dword_5E4B70_ptr, 4);

    void* dword_5E4AD0_ptr = &dword_5E4AD0_long;
    hook::write_to_readonly((void*)(0x00417488 + 2), dword_5E4AD0_ptr, 4);
    hook::write_to_readonly((void*)(0x00417610 + 2), dword_5E4AD0_ptr, 4);
    hook::write_to_readonly((void*)(0x00455693 + 2), dword_5E4AD0_ptr, 4);

    void* dword_5E4A30_ptr = &dword_5E4A30_long;
    hook::write_to_readonly((void*)(0x0041747A + 2), dword_5E4A30_ptr, 4);
    hook::write_to_readonly((void*)(0x004175F7 + 2), dword_5E4A30_ptr, 4);
    hook::write_to_readonly((void*)(0x00455686 + 2), dword_5E4A30_ptr, 4);

    hook::make_hook((void*)0x0048EF55, player_get_skill, false);
    // hook::make_hook((void*)0x0048EF4F, player_get_skill_48EF4F, true);

    attach_DialogLogic_MiscSkills_4F3938();
    attach_DialogLogic_WeaponSkills_4F3914();
    attach_DialogLogic_ArmorSkills_4F3900();
    attach_DialogLogic_MagicSkills_4F3968();

    hook::make_hook((void*)0x00418E07, hook_00418E07);
    hook::make_hook((void*)0x00418E41, hook_00418E41);
    hook::make_hook((void*)0x004191B4, hook_004191B4);
    hook::make_hook((void*)0x004191EE, hook_004191EE);
    hook::make_hook((void*)0x00419554, hook_00419554);
    hook::make_hook((void*)0x0041958E, hook_0041958E);
    hook::make_hook((void*)0x004198F7, hook_004198F7);
    hook::make_hook((void*)0x00419931, hook_00419931);
    hook::make_hook((void*)0x00417708, hook_00417708);

    hook::make_hook((void*)0x0043233F, hook_0043233F);
    hook::make_hook((void*)0x0043236B, hook_0043236B);
    hook::make_hook((void*)0x004C9EB1, hook_004C9EB1);
    hook::make_hook((void*)0x00417417, hook_00417417);
    
    loadgame_ret = push_load(hook_0045C9BB_loadgame);
    if(!loadgame_ret)
    {
        hook::make_hook((void*)0x0045C9BB, hook_0045C9BB_loadgame);
    }

    savegame_ret = push_save(hook_0045D2E6_savegame);
    if (!savegame_ret) {
        hook::make_hook((void*)0x0045D2E6, hook_0045D2E6_savegame);
    }

    hook::make_hook((void*)0x0041728C, hook_0041728C);

    // teachers
    hook::make_hook((void*)0x004B3304, hook_004B3304);
    hook::make_hook((void*)0x004B32E3, hook_004B32E3);
    hook::make_hook((void*)0x004B32FA, hook_004B32FA);
    hook::make_hook((void*)0x004B938E, hook_004B938E);
    hook::make_hook((void*)0x004B9398, hook_004B9398);
    hook::make_hook((void*)0x004B9493, hook_004B9493);
    hook::make_hook((void*)0x004B9483, hook_004B9483);
    hook::make_hook((void*)0x004B946C, hook_004B946C);
    hook::make_hook((void*)0x004B7BFA, hook_004B7BFA);
    hook::make_hook((void*)0x004B7C04, hook_004B7C04);
    hook::make_hook((void*)0x004B7CEF, hook_004B7CEF);
    hook::make_hook((void*)0x004B7CFF, hook_004B7CFF);
    hook::make_hook((void*)0x004B82BE, hook_004B82BE);
    hook::make_hook((void*)0x004B82C8, hook_004B82C8);
    hook::make_hook((void*)0x004B83C3, hook_004B83C3);
    hook::make_hook((void*)0x004B83B3, hook_004B83B3);
    hook::make_hook((void*)0x0048F056, hook_0048F056, true);
    hook::make_hook((void*)0x004BAF79, hook_004BAF79);
    hook::make_hook((void*)0x004B4A88, hook_004B4A88);
    hook::make_hook((void*)0x004B4E01, hook_004B4E01);
    hook::make_hook((void*)0x004B4F98, hook_004B4F98);
    hook::make_hook((void*)0x004BAEE8, hook_004BAEE8);
    // hook::make_hook((void*)0x79582522, hook_79582522);
    hook::make_hook((void*)0x004BAE92, hook_004BAE92);
    hook::make_hook((void*)0x004B8399, hook_004B8399);
    hook::make_hook((void*)0x004B7BDD, hook_004B7BDD);
    hook::make_hook((void*)0x004B58FF, hook_004B58FF);
    hook::make_hook((void*)0x004B5919, hook_004B5919);
    hook::make_hook((void*)0x004B59F9, hook_004B59F9);
    hook::make_hook((void*)0x004B5A23, hook_004B5A23);
    hook::make_hook((void*)0x004B404B, hook_004B404B);
    hook::make_hook((void*)0x004B4065, hook_004B4065);
    hook::make_hook((void*)0x004B4075, hook_004B4075);
    hook::make_hook((void*)0x004B3F56, hook_004B3F56);
    hook::make_hook((void*)0x004B3F7A, hook_004B3F7A);
    hook::make_hook((void*)0x004B3F70, hook_004B3F70);
    hook::make_hook((void*)0x004B82A1, hook_004B82A1);
    hook::make_hook((void*)0x004B7CD5, hook_004B7CD5);
    hook::make_hook((void*)0x004B9374, hook_004B9374);
    hook::make_hook((void*)0x004B4BA2, hook_004B4BA2);
    hook::make_hook((void*)0x004B4BBC, hook_004B4BBC);
    hook::make_hook((void*)0x004B4BC6, hook_004B4BC6);
    hook::make_hook((void*)0x004B4C99, hook_004B4C99);
    hook::make_hook((void*)0x004B4CC3, hook_004B4CC3);
    hook::make_hook((void*)0x0048E2CE, hook_0048E2CE);
    hook::make_hook((void*)0x004B1F73, createSkillLearningTopics);
    preset_shops();
    hook::make_hook((void*)0x0043343A, hook_0043343A);
    hook::make_hook((void*)0x004BAF64, hook_004BAF64);
    hook::make_hook((void*)0x004B693D, hook_004B693D);
    hook::make_hook((void*)0x004B6A38, hook_004B6A38);
    hook::make_hook((void*)0x004B6A4C, hook_004B6A4C);
    hook::make_hook((void*)0x004B6A5F, hook_004B6A5F);
    hook::make_hook((void*)0x004BB6F7, hook_004BB6F7, false);

    hook::make_hook((void*)0x004B33DF, hook_004B33DF);
    hook::make_hook((void*)0x004B33F6, hook_004B33F6);
    hook::make_hook((void*)0x004B3406, hook_004B3406);
    hook::make_hook((void*)0x004B0E5D, hook_004B0E5D);

    hook::make_hook((void*)0x004B0E8E, hook_004B0E8E, true);
    hook::make_hook((void*)0x0041719A, hook_0041719A, false);
    hook::make_hook((void*)0x0041714E, sub_41714E); // color thingy
    hook::make_hook((void*)0x00417295, hook_00417295, false);
    hook::make_hook((void*)0x00417265, hook_00417265);

    once = true;
}

long startup_ret = 0;
__declspec(naked) void startup(void) {
    __asm {
        cmp startup_ret, 0
        jnz nude

        call dword ptr ds : [0x004E8148]
        xor edx, edx
        push eax
        pusha
        call startup_impl
        popa
        pop eax
        push 0x004DC859
        ret

        nude:
        push eax
        pusha
        call startup_impl
        popa
        pop eax
        push startup_ret
        ret
    }
}

long ztime_ret = 0;
long sub_445F25 = 0x445F25;
__declspec(naked) void ztimer(void) {
    __asm {
        pusha
        call ztimer_impl
        cmp ztime_ret, 0
        jnz nude
        popa
        xor ecx, ecx
        call sub_445F25
        push 0x004613FB
        ret

        nude:
        popa
        push ztime_ret
        ret
    }
}

extern "C" __declspec(dllexport) int32_t __cdecl get2(mm8::Player * player, int skill_ID) {
    int player_id = PlayerID(player);
    if (player_id < 0)
        return 0;
    return (getSkillMastery_new(player, skill_ID) << 16) + (0x3ff & get(player, skill_ID));
}


extern "C" __declspec(dllexport) void __cdecl set2(mm8::Player * player, int skill_ID, int32_t value) {
    long player_id = PlayerID(player);
    if (player_id < 0) return;
    int16_t raw_value = value & 0x3ff;    int16_t raw_mastery = value >> 16;
    int16_t skill_flags[5] = { 0, 0, 1024, 2048, 4096};
    saved_data.extra_mastery[player_id][skill_ID] = raw_mastery < 4 ? 0 : raw_mastery - 4;
    set(player, skill_ID, raw_value + (raw_mastery > 4 ? skill_flags[4] : skill_flags[raw_mastery]));
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    static bool once = false;
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        
        if (once) return TRUE;

        startup_ret = push_start(startup);
        if(!startup_ret)
        {
            MessageBoxA(0, "cave_api is missing", "SKILLZ", 0);
            hook::make_hook((void*)0x004DC851, startup);
        }

        ztime_ret = push_time(ztimer);
        if (!ztime_ret) {
            MessageBoxA(0, "cave_api is missing", "SKILLZ", 0);
            hook::make_hook((void*)0x004613F4,ztimer);
        }

        once = true;
        break;
    }
    return TRUE;
}

