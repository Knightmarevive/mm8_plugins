// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"
#include "../skillz/hook.hpp"

struct code_cave {
    static constexpr int hook_size = 6;
    static constexpr int code_size = hook_size * 256;
    char code[code_size]; char* ptr;
    code_cave() = delete;
    code_cave(void* fun) {
        memset(code, 0x90, code_size); ptr = code;
        hook::make_hook((void*)((int)code + code_size - hook_size), fun);
    }
    int push(void* fun) {
        if (*ptr != (char) 0x90) 
            return 0;
        hook::make_hook(ptr, fun);
        ptr += hook_size;
        return (int) ptr;
    }
};

long sub_445F25 = 0x445F25;
__declspec(naked) void time_ret() {
    __asm {
        xor ecx, ecx
        call sub_445F25
        push 0x004613FB
        ret
    }
}
code_cave time_cave(time_ret);

__declspec(naked) void startup_ret(void) {
    __asm {
        call dword ptr ds : [0x004E8148]
        xor edx, edx

        push 0x004DC859
        ret
    }
}
code_cave start_cave(startup_ret);

long fun_004D9E20 = 0x004D9E20;
__declspec(naked) void save_ret(void) {
    __asm {

        lea eax, ss : [ebp - 0x44]
        push eax
        call fun_004D9E20
        push 0x0045D2EC
        ret
    }
}
code_cave save_cave(save_ret);


long fun_0045EFFF = 0x0045EFFF;
__declspec(naked) void load_ret(void) {
    __asm {
        mov ecx, esi
        call fun_0045EFFF
        push 0x0045C9C2
        ret
    }
}
code_cave load_cave(load_ret);

__declspec(naked) void buff_ret(void) {
    __asm {
        pop edi
        pop esi
        pop ebx
        ret 4
    }
}
code_cave buff_cave(buff_ret);

// long equip_ptr = 0;
__declspec(naked) void equip_ret(void) {
    __asm {
        cmp     esi, 4
        jnz     loc_4674B8
        test    edx, edx

        push    0x00467491
        ret

        loc_4674B8:
        push    0x004674B8
        ret
    }
}
code_cave equip_cave(equip_ret);


__declspec(naked) void character_creation_ret(void) {
    __asm {
        mov     ecx, [ebp - 0Ch]
        mov     eax, [ebp - 14h]
        push    0x004942BE
        ret
    }

}
code_cave  character_creation_cave(character_creation_ret);

void __cdecl DoLater_impl() {
    ///equip_ptr = *(int*)(0x00467479 + 3);
    //hook::make_hook((void*)0x00467479, equip_cave.code);
}

int DoLater_ret = 0;
__declspec(naked) void DoLater(void) {
    __asm {
        pusha
        call DoLater_impl
        popa
        push DoLater_ret
        ret
    }
}


extern "C" __declspec(dllexport) int __cdecl push_character_creation(void* fun) {
    return character_creation_cave.push(fun);
    // return true;
}

extern "C" __declspec(dllexport) int __cdecl push_equip(void* fun) {
    return equip_cave.push(fun);
    // return true;
}

extern "C" __declspec(dllexport) int __cdecl push_buff(void* fun) {
    return buff_cave.push(fun);
    // return true;
}

extern "C" __declspec(dllexport) int __cdecl push_time(void* fun) {
    return time_cave.push(fun);
    // return true;
}


extern "C" __declspec(dllexport) int __cdecl push_start(void* fun) {
    return start_cave.push(fun);
    // return true;
}

extern "C" __declspec(dllexport) int __cdecl push_save(void* fun) {
    return save_cave.push(fun);
    // return true;
}

extern "C" __declspec(dllexport) int __cdecl push_load(void* fun) {
    return load_cave.push(fun);
    // return true;
}

bool once = false;
BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        if (once) return TRUE;
        hook::make_hook((void*)0x004942B8, character_creation_cave.code);
        hook::make_hook((void*)0x0046748A, equip_cave.code);

        hook::make_hook((void*)0x0048F07E, buff_cave.code);
        hook::make_hook((void*)0x004613F4, time_cave.code);
        hook::make_hook((void*)0x004DC851, start_cave.code);
        hook::make_hook((void*)0x0045D2E6, save_cave.code);
        hook::make_hook((void*)0x0045C9BB, load_cave.code);

        // DoLater_ret = start_cave.push(DoLater);

        once = true;
        break;
    }
    return TRUE;
}

