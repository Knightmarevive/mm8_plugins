// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"
#include "../Skillz/hook.hpp"
#include "../Skillz/structs/Player.h"

constexpr int frames = 5;
constexpr int hero_count = 64;

int push_time(void* fun) {
    static HMODULE module = nullptr;
    if (!module)    module = GetModuleHandleA("cave_api.dll");
    if (!module)   return 0;
    static auto impl = (int(*)(void*))nullptr;
    if (!impl) impl = (int(*)(void*)) GetProcAddress(module, "push_time");
    if (impl) return impl(fun); else return 0;
}
int push_start(void* fun) {
    static HMODULE module = nullptr;
    if (!module)    module = GetModuleHandleA("cave_api.dll");
    if (!module)   return 0;
    static auto impl = (int(*)(void*))nullptr;
    if (!impl) impl = (int(*)(void*)) GetProcAddress(module, "push_start");
    if (impl) return impl(fun); else return 0;
}
int push_save(void* fun) {
    static HMODULE module = nullptr;
    if (!module)    module = GetModuleHandleA("cave_api.dll");
    if (!module)   return 0;
    static auto impl = (int(*)(void*))nullptr;
    if (!impl) impl = (int(*)(void*)) GetProcAddress(module, "push_save");
    if (impl) return impl(fun); else return 0;
}
int push_load(void* fun) {
    static HMODULE module = nullptr;
    if (!module)    module = GetModuleHandleA("cave_api.dll");
    if (!module)   return 0;
    static auto impl = (int(*)(void*))nullptr;
    if (!impl) impl = (int(*)(void*)) GetProcAddress(module, "push_load");
    if (impl) return impl(fun); else return 0;
}


extern "C" __declspec(dllexport) int32_t __cdecl PlayerID(mm8::Player * player) {
    long begin = 0x00B2187C;
    long distance = (long)player - begin;
    if (distance < 0) return -1;
    if (distance % sizeof(mm8::Player)) return -1;
    else return distance / sizeof(mm8::Player);
}

int count(mm8::Player* p) {
    int answer = 0;
    for (auto& i : p->items)
        if (i.number)
            ++answer;
    return answer;
}


bool operator !=(mm8::Item& a, mm8::Item& b) {
    return std::memcmp(&a, &b, sizeof(mm8::Item));
}

bool operator ==(mm8::Item& a, mm8::Item& b) {
    return !std::memcmp(&a, &b, sizeof(mm8::Item));
}

void clean(mm8::Player* p) {
    
        for (auto& item : p->items) {
            bool found = false;
            for (auto& worn : p->equippedItems) {
                if (item == p->items[worn-1])
                    found = true;
            }
            if (found) continue;
            item.bodyLocation = 0;
        }
}

struct eq_frame {
    std::array<mm8::Item, 16> Equipment;
    eq_frame() {
        memset(&Equipment, 0, sizeof(Equipment));
    }
    int count() {
        int answer = 0;
        for (auto &i : Equipment) 
            if (i.number)
                ++answer;
        return answer;
    }

    eq_frame(mm8::Player* p) {
        memset(&Equipment, 0, sizeof(Equipment));
        for (auto &i: p->items) {
            if (i.number && i.bodyLocation) {
                Equipment[i.bodyLocation - 1] = i;
                i = mm8::Item();
            }
        }
        for (auto& i : p->equippedItems) i = 0;
    }

    bool write(mm8::Player* p) {
        for (auto& i : Equipment) {
            if (i.bodyLocation && i.number != 0) {
                bool done = false;
                for (int j = 0; j< 138; ++j) {
                    // if (j < 0) break; // return false;
                    if (p->items[j].number != 0 && p->items[j].bodyLocation == i.bodyLocation) {
                        p->items[j] = i;
                        p->equippedItems[i.bodyLocation - 1] = j + 1;
                        done = true; break;
                    }
                }
                if (done) continue;

                for (int j = 0; j < 138; ++j) {
                    if (p->items[j].number == 0) {
                        p->items[j] = i;
                        p->equippedItems[i.bodyLocation - 1] = j + 1;
                        done = true; break;
                    }
                }
            }
        }
        return true;
    }
};

struct wardrobe {
    eq_frame equipment[frames];
    int current = 0;
    bool swap(mm8::Player* p, int target) {
        if (PlayerID(p) < 0) return false;
        if (PlayerID(p) >= hero_count) return false;
        if (target < 0) return false;
        if (target >= frames) return false;
        if (target == current) return false;
        clean(p);

        auto new_frame = eq_frame(p);

        if (equipment[target].count() + count(p) > 138) {
            new_frame.write(p);
            return false;
        }

        equipment[current] = new_frame;
        equipment[target].write(p);
        current = target;
        return true;
    }
    void next(mm8::Player* p) {
        swap(p, (current + 1) % frames);
    }
    void prev(mm8::Player* p) {
        swap(p, (current - 1) % frames);
    }
};

struct inv_frame {
    std::array<mm8::Item, 138> items; // MMExt: 1..138, here 0..137 
    std::array<int32_t, 126> inventory;
    std::array<int32_t, 16> equippedItems;
    inv_frame() {
        memset(&items, 0, sizeof(items));
        memset(&inventory, 0, sizeof(inventory));
        memset(&equippedItems, 0, sizeof(equippedItems));
    };
    int count() {
        int answer = 0;
        for (auto& i : items)
            if (i.number && i.bodyLocation == 0)
                ++answer;
        return answer;
    }
    inv_frame(mm8::Player *p) :inv_frame() {
        memcpy_s(&items, sizeof(items), &(p->items),sizeof(p->items));
        memcpy_s(&inventory, sizeof(inventory), &(p->inventory), sizeof(p->inventory));
        memcpy_s(&equippedItems, sizeof(equippedItems), &(p->equippedItems), sizeof(p->equippedItems));
    };
    void write(mm8::Player* p) {
        hook::write_to_readonly(&(p->items), &items, sizeof(items));
        hook::write_to_readonly(&(p->inventory), &inventory, sizeof(inventory));
        hook::write_to_readonly(&(p->equippedItems), &equippedItems, sizeof(equippedItems));
    }
};

struct luggage {
    inv_frame inventory[frames];
    int current = 0;
    bool swap(mm8::Player* p, int target) {
        if (PlayerID(p) < 0) return false;
        if (PlayerID(p) >= hero_count) return false;
        if (target < 0) return false;
        if (target >= frames) return false;
        if (target == current) return false;
        clean(p);

        auto tmp = eq_frame(p);
        if (tmp.count() + inventory[target].count() > 138) {
            tmp.write(p); return false;
        }

        inventory[current] = inv_frame(p);
        inventory[target].write(p);
        tmp.write(p);
        current = target;

        return true;
    }
    void next(mm8::Player* p) {
        swap(p, (current + 1) % frames);
    }
    void prev(mm8::Player* p) {
        swap(p, (current - 1) % frames);
    }
};

luggage saved_luggage[hero_count];
long saved_luggage_long = (long)saved_luggage;
const char* Luggage_bin = "luggage_maw.bin";
constexpr size_t saved_luggage_block_size = sizeof(saved_luggage);


wardrobe saved_wardrobe[hero_count];
long saved_wardrobe_long = (long)saved_wardrobe;
const char* Wardrobe_bin = "wardrobe_maw.bin";
constexpr size_t saved_wardrobe_block_size = sizeof(saved_wardrobe);

long fun_0045EFFF = 0x0045EFFF;
long fun_004D9E20 = 0x004D9E20;
long fun_0045F5A1 = 0x0045F5A1;
long fun_004D9F10 = 0x004D9F10;
long fun_0040F5D8 = 0x0040F5D8;
long fun_004651A4 = 0x004651A4;
long fun_004DA641 = 0x004DA641;
long luggage_savegame_ret = 0;
__declspec(naked) long hook_luggage_save() {
    __asm {
        pusha

        lea eax, ss: [ebp - 0x44]
        push Luggage_bin
        push eax
        call fun_004D9E20
        pop  ecx
        pop  ecx
        push edi
        push saved_luggage_long
        lea eax, ss: [ebp - 0x44]
        push eax
        mov  ecx, esi
        mov  eax, saved_luggage_block_size
        mov[ebp - 0x30], eax
        call fun_0045F5A1
        test eax, eax
        jz failed


        push 1
        push dword ptr ds : [0x00601DD8]
        lea eax, ss : [ebp - 0xE4]
        push eax
        call fun_004D9F10
        add esp, 0x0C
        push saved_luggage_block_size
        push ebx
        sub esp, 0x10
        mov ecx, esp
        mov dword ptr ss : [ebp - 0x08] , esp
        lea eax, ss : [ebp - 0x01]
        push eax
        lea eax, ss : [ebp - 0xE4]
        push eax
        call fun_0040F5D8
        lea ecx, ss : [ebp - 0x08]
        call fun_004651A4


        failed:
        cmp luggage_savegame_ret, 0
        jnz nude

        popa
        lea eax, ss : [ebp - 0x44]
        push eax
        call fun_004D9E20

        push 0x0045D358
        ret

        nude:
        popa
        push luggage_savegame_ret
        ret
    }
}

long luggage_loadgame_ret = 0;
__declspec(naked) long hook_luggage_load() {
    __asm {

        pusha

        push 1
        push Luggage_bin
        mov ecx, esi
        call fun_0045EFFF
        test eax, eax
        jz failed_to_load

        push eax
        push ebx
        push saved_luggage_block_size
        push saved_luggage_long
        call fun_004DA641
        add esp, 0x10
        jmp load_succeed

        failed_to_load :
        mov al, 0
        mov ecx, saved_luggage_block_size
        mov edi, saved_luggage_long
        rep stosb

        load_succeed :
        cmp luggage_loadgame_ret, 0
        jnz nude

        popa

        mov ecx, esi
        call fun_0045EFFF

        push 0x0045CA2A
        ret

        nude :
        popa
        push luggage_loadgame_ret
        ret
    }
}


long wardrobe_savegame_ret = 0;
__declspec(naked) long hook_wardrobe_save() {
    __asm {
        pusha

        lea eax, ss: [ebp - 0x44]
        push Wardrobe_bin
        push eax
        call fun_004D9E20
        pop  ecx
        pop  ecx
        push edi
        push saved_wardrobe_long
        lea eax, ss: [ebp - 0x44]
        push eax
        mov  ecx, esi
        mov  eax, saved_wardrobe_block_size
        mov[ebp - 0x30], eax
        call fun_0045F5A1
        test eax, eax
        jz failed


        push 1
        push dword ptr ds : [0x00601DD8]
        lea eax, ss : [ebp - 0xE4]
        push eax
        call fun_004D9F10
        add esp, 0x0C
        push saved_wardrobe_block_size
        push ebx
        sub esp, 0x10
        mov ecx, esp
        mov dword ptr ss : [ebp - 0x08] , esp
        lea eax, ss : [ebp - 0x01]
        push eax
        lea eax, ss : [ebp - 0xE4]
        push eax
        call fun_0040F5D8
        lea ecx, ss : [ebp - 0x08]
        call fun_004651A4


        failed :
        cmp wardrobe_savegame_ret, 0
        jnz nude

            popa
            lea eax, ss : [ebp - 0x44]
            push eax
            call fun_004D9E20

            push 0x0045D358
            ret

            nude :
        popa
            push wardrobe_savegame_ret
            ret
    }
}

long wardrobe_loadgame_ret = 0;
__declspec(naked) long hook_wardrobe_load() {
    __asm {

        pusha

        push 1
        push Wardrobe_bin
        mov ecx, esi
        call fun_0045EFFF
        test eax, eax
        jz failed_to_load

        push eax
        push ebx
        push saved_wardrobe_block_size
        push saved_wardrobe_long
        call fun_004DA641
        add esp, 0x10
        jmp load_succeed

        failed_to_load :
        mov al, 0
            mov ecx, saved_wardrobe_block_size
            mov edi, saved_wardrobe_long
            rep stosb

            load_succeed :
        cmp wardrobe_loadgame_ret, 0
            jnz nude

            popa

            mov ecx, esi
            call fun_0045EFFF

            push 0x0045CA2A
            ret

            nude :
        popa
            push wardrobe_loadgame_ret;
        ret
    }
}



void init_luggage() {
    static bool once = false;
    if (once) return;

    luggage_savegame_ret = push_save(hook_luggage_save);
    if (!luggage_savegame_ret) {
        MessageBoxA(0, "cave_api is missing", "Luggage", 0);
        hook::make_hook((void*)0x0045D352, hook_luggage_save);
    }

    luggage_loadgame_ret = push_load(hook_luggage_load);
    if (!luggage_loadgame_ret) {
        MessageBoxA(0, "cave_api is missing", "Luggage", 0);
        hook::make_hook((void*)0x0045CA23, hook_luggage_load);
    }

    wardrobe_loadgame_ret = push_load(hook_wardrobe_load);
    wardrobe_savegame_ret = push_save(hook_wardrobe_save);

    once = true;
}


extern "C" __declspec(dllexport) void* __cdecl LuggageSlot(mm8::Player * p, int slot) {
    int index = PlayerID(p);
    if (index < 0) return nullptr;
    if (slot < 0 || slot >= frames) return nullptr;
    clean(p);

    if (saved_luggage[index].current == slot) {

        auto tmp = eq_frame(p);
        saved_luggage[index].inventory[slot] = inv_frame(p);
        tmp.write(p);
    }
    auto ret = &saved_luggage[index].inventory[slot];

    return ret;
}

extern "C" __declspec(dllexport) int __cdecl activate(mm8::Player* p, int target) {
    int index = PlayerID(p);
    if (index < 0) return false;
    // clean(p);
    return saved_luggage[index].swap(p, target);
}
extern "C" __declspec(dllexport) void __cdecl next(mm8::Player * p) {
    int index = PlayerID(p);
    if (index < 0) return;
    saved_luggage[index].next(p);
}
extern "C" __declspec(dllexport) void __cdecl prev(mm8::Player * p) {
    int index = PlayerID(p);
    if (index < 0) return;
    saved_luggage[index].prev(p);
}

extern "C" __declspec(dllexport) int __cdecl current(mm8::Player * p) {
    int index = PlayerID(p);
    if (index < 0) return 0;

    return saved_luggage[index].current;
}


extern "C" __declspec(dllexport) int __cdecl activate2(mm8::Player * p, int target) {
    int index = PlayerID(p);
    if (index < 0) return false;
    // clean(p);
    return saved_wardrobe[index].swap(p, target);
    
}
extern "C" __declspec(dllexport) int __cdecl current2(mm8::Player * p) {
    int index = PlayerID(p);
    if (index < 0) return 0;

    return saved_wardrobe[index].current;
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        init_luggage();
        break;
    }
    return TRUE;
}

