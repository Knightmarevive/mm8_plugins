// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"
#include "../Skillz/hook.hpp"

bool once = true; 
char empty[] = "";
int empty_ptr = (int)empty;

char new_004F3BF8[] = "%s\n";
char new_recovery_time_cap = 1;

void patch_ini(void) {
    FILE* fdesc = nullptr; 
    char* buf = nullptr;
    fopen_s(&fdesc, "mm8.ini", "r");
    if (fdesc) {
        fseek(fdesc, 0, SEEK_END);
        int fdesc_size = ftell(fdesc);
        rewind(fdesc);
        //----------
        buf = (char*)malloc(fdesc_size + 1);
        fread(buf, 1, fdesc_size, fdesc);
        buf[fdesc_size] = 0;
        fclose(fdesc);

        
        if (strstr(buf,"FixMonstersBlockingShots=0")) {
            const char* pattern = "FixMonstersBlockingShots=";
            char* ptr = strstr(buf, pattern);
            if (ptr) ptr[strlen(pattern)] = '1';

            fdesc = nullptr;
            fopen_s(&fdesc, "mm8.ini", "w");
            fprintf(fdesc, buf);
            fclose(fdesc);

        }

        free(buf);
    }

}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        if (once) {
            hook::write_to_readonly((void*)0x004F3BF8, new_004F3BF8, sizeof(new_004F3BF8));
            hook::write_to_readonly((void*)(0x0042DA51 + 1), &new_recovery_time_cap, 1);
            patch_ini();
        }
        once = false;
        break;
    }
    return TRUE;
}

